package requestlab.com.latribune.accengage;

import android.app.Application;
import android.content.res.Configuration;

import com.ad4screen.sdk.A4S;

/**
 * Created by lchazal on 23/05/16.
 */
public class A4SApplication extends Application {

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        if (A4S.isInA4SProcess(this)) {
            return;
        }
        super.onConfigurationChanged(newConfig);
    }

    @Override
    public void onCreate() {
        if (A4S.isInA4SProcess(this)) {
            return;
        }
        super.onCreate();
    }

    @Override
    public void onLowMemory() {
        if (A4S.isInA4SProcess(this)) {
            return;
        }
        super.onLowMemory();
    }

    @Override
    public void onTerminate() {
        if (A4S.isInA4SProcess(this)) {
            return;
        }
        super.onTerminate();
    }
}
