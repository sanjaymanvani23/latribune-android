package requestlab.com.latribune.accengage;

import android.content.Intent;


import androidx.appcompat.app.AppCompatActivity;

import com.ad4screen.sdk.A4S;

/**
 * Created by lchazal on 23/05/16.
 */
public class A4SAppCompatActivity extends AppCompatActivity {

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);

        getA4S().setIntent(intent);
    }

    @Override
    protected void onResume() {
        super.onResume();

        getA4S().startActivity(this);
    }

    @Override
    protected void onPause() {
        super.onPause();

        getA4S().stopActivity(this);
    }

    public A4S getA4S() {
        return A4S.get(this);
    }
}