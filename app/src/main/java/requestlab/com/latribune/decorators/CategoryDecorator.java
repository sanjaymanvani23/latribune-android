package requestlab.com.latribune.decorators;

import android.content.Context;
import android.graphics.Rect;

import android.util.Log;
import android.util.Pair;
import android.view.View;

import androidx.recyclerview.widget.RecyclerView;

import requestlab.com.latribune.utils.StylesManager;

/**
 * Created by lchazal on 12/05/16.
 */
public class CategoryDecorator extends RecyclerView.ItemDecoration {

    protected int[]   styleSet;
    protected int     defaultMargin;

    protected int     topMultiplier = 1;
    protected int     bottomMultiplier = 1;

    public CategoryDecorator(int[] styleSet, int defaultMargin) {
        this.styleSet = styleSet;
        this.defaultMargin = defaultMargin;
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
        topMultiplier = 1;
        bottomMultiplier = 1;

        int position = parent.getChildAdapterPosition(view);
        if (position == 0) topMultiplier = 2;
        int type = styleSet.length <= position ? styleSet[styleSet.length - 1] : styleSet[position];

        switch (type) {
            case StylesManager.ART_SMALL_CARD:
            case StylesManager.AD_NATIVE:
                getSmallArticleDimensions(position);
                break;
            case StylesManager.INFO:
            case StylesManager.SUBSCRIBE:
            case StylesManager.STOCK:
            case StylesManager.STOCK_NOTITLE:
                topMultiplier = 0;
                bottomMultiplier = 0;
            default:
                break;
        }

        outRect.set(0, defaultMargin * topMultiplier, 0, defaultMargin * bottomMultiplier); //left top right bottom
    }

    protected void  getSmallArticleDimensions(int position) {
        int nextType = styleSet.length <= position + 1 ? styleSet[styleSet.length - 1] : styleSet[position + 1];

        topMultiplier = 1;
        switch (nextType) {
            case StylesManager.ART_SMALL_CARD:
            case StylesManager.AD_NATIVE:
                bottomMultiplier = 0;
                break;
            default:
                break;
        }
    }
}
