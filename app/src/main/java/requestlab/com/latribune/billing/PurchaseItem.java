package requestlab.com.latribune.billing;



public enum PurchaseItem {
    BASE64_KEY,
    IAB_SUBSCRIPTION,
    IAB_PRODUCT_CONNECTION,
    IAB_TEST_CONSUMABLE_PRODUCT;


    public String toString() {
        switch (this) {
            case BASE64_KEY:
                return "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAmS3dRKvib6U7+Ia8TyLCOCjrHngSI1zQeRk6nU/lLmLcl7MyewHnt0fp30yYQ7MCHmSKXgoleAHxH0ub12pqNWV7biTB3lJpRWLDsx8Q4Lu4+8CL496cE8usCSaizch6DR5+n+vnPoBB/ufAHCdFOHmAqg0P31qZ2QIaly8KBo8+UNqwHzgmIWBjS70sUE0XLb1Vk5PUdUcS4N6giHCu26kDNKnRzlR2OLC7L50H3OrNzyYjeqA8xkm/ejuGq1wnsRPyCSMaeRbldyXVVvD9UWnG4h2UAiyCLYcDOXKzHQ0HIS/DLVSm+kPu2z1x/qG+mbYepgrzY3R9AIOFh5ejRQIDAQAB";
            case IAB_SUBSCRIPTION:
                return "monthly";
                case IAB_PRODUCT_CONNECTION:
                return "connection_consume";
            case IAB_TEST_CONSUMABLE_PRODUCT:
                return "test_consumable";
        }
        return null;
    }

}