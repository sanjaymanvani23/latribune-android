/*
 * Copyright 2017 Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package requestlab.com.latribune.billing;

import android.app.Activity;
import android.content.Context;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.android.billingclient.api.AcknowledgePurchaseParams;
import com.android.billingclient.api.AcknowledgePurchaseResponseListener;
import com.android.billingclient.api.BillingClient;
import com.android.billingclient.api.BillingClient.FeatureType;
import com.android.billingclient.api.BillingClient.SkuType;
import com.android.billingclient.api.BillingClientStateListener;
import com.android.billingclient.api.BillingFlowParams;
import com.android.billingclient.api.BillingResult;
import com.android.billingclient.api.ConsumeParams;
import com.android.billingclient.api.ConsumeResponseListener;
import com.android.billingclient.api.Purchase;
import com.android.billingclient.api.Purchase.PurchasesResult;
import com.android.billingclient.api.PurchasesUpdatedListener;
import com.android.billingclient.api.SkuDetails;
import com.android.billingclient.api.SkuDetailsParams;
import com.android.billingclient.api.SkuDetailsResponseListener;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Handles all the interactions with Play Store (via Billing library), maintains connection to
 * it through BillingClient and caches temporary states/data if needed
 */
public class BillingManager implements PurchasesUpdatedListener {
    // Default value of mBillingClientResponseCode until BillingManager was not yeat initialized
    public static final int BILLING_MANAGER_NOT_INITIALIZED = -1;

    private static final String TAG = "BillingManager";
    /* BASE_64_ENCODED_PUBLIC_KEY should be YOUR APPLICATION'S PUBLIC KEY
     * (that you got from the Google Play developer console). This is not your
     * developer public key, it's the *app-specific* public key.
     *
     * Instead of just storing the entire literal string here embedded in the
     * program,  construct the key at runtime from pieces or
     * use bit manipulation (for example, XOR with some other string) to hide
     * the actual key.  The key itself is not secret information, but we don't
     * want to make it easy for an attacker to replace the public key with one
     * of their own and then fake messages from the server.
     */
    private static String BASE_64_ENCODED_PUBLIC_KEY = "CONSTRUCT_YOUR";
    //    private static final String BASE_64_ENCODED_PUBLIC_KEY = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAiUqv79XqqBCC/jbRUTZ0W+4vulPFUn7FhEkMW7aqarUR+PhFnkaoweYCq1ytJPnO7czn9VnlUcHBk9JOgRea1v0O03tRBa1OhDdloM1xfoKjN0z6u9VGrWGxSbU2Q/zbH2aAsp3Kvv7vZGOQOhoBUv2jVQgJD/53k+9ofD08mtoPdNvCBl7c/Glk59bmDGjk2CvKDQ/XGMQ4ooJKjK4GiJDX/h9MdRasaMqTEqacfpBgQr6bmeAlB4pVEuRZFZfcbwni2qikziDD8udlLjePF7MJsLDXoPWVl5lPivsHZf2cXMeMkk5yBF2mlfqG1GXXEe2PsviRjzcbbEIGXm6JtwIDAQAB";
    private final BillingUpdatesListener mBillingUpdatesListener;
    private final Activity mActivity;
    private final List<Purchase> mPurchases = new ArrayList<>();
    /**
     * A reference to BillingClient
     **/
    private BillingClient mBillingClient;
    /**
     * True if billing service is connected now.
     */
    private boolean mIsServiceConnected;
    private Set<String> mTokensToBeConsumed;
    private int mBillingClientResponseCode = BILLING_MANAGER_NOT_INITIALIZED;


    public BillingManager(Activity activity, final BillingUpdatesListener updatesListener, String base64PublicKey) {
        Log.e(TAG, "Creating Billing client.");
        mActivity = activity;
        mBillingUpdatesListener = updatesListener;
        BASE_64_ENCODED_PUBLIC_KEY = base64PublicKey;
        mBillingClient = BillingClient.newBuilder(mActivity)
                .enablePendingPurchases()
                .setListener(this)
                .build();

        Log.e(TAG, "Starting setup.");

        // Start setup. This is asynchronous and the specified listener will be called
        // once setup completes.
        // It also starts to report all the new purchases through onPurchasesUpdated() callback.
        startServiceConnection(new Runnable() {
            @Override
            public void run() {
                // Notifying the listener that billing client is ready
                mBillingUpdatesListener.onBillingClientSetupFinished();
                // IAB is fully set up. Now, let's get an inventory of stuff we own.
                Log.e(TAG, "Setup successful. Querying inventory.");
                queryPurchases();
            }
        });
    }

    public Context getContext() {
        return mActivity;
    }

    public void startServiceConnection(final Runnable executeOnSuccess) {
        mBillingClient.startConnection(new BillingClientStateListener() {

            @Override
            public void onBillingSetupFinished(@NonNull BillingResult billingResult) {
                Log.e(TAG, "Setup finished. Response code: " + billingResult.getResponseCode());

                if (billingResult.getResponseCode() == BillingClient.BillingResponseCode.OK) {
                    mIsServiceConnected = true;
                    if (executeOnSuccess != null) {
                        executeOnSuccess.run();
                    }
                }
                mBillingClientResponseCode = billingResult.getResponseCode();
            }

            @Override
            public void onBillingServiceDisconnected() {
                mIsServiceConnected = false;
                Log.e(TAG, "onBillingServiceDisconnected");
            }
        });
    }

    /**
     * Query purchases across various use cases and deliver the result in a formalized way through
     * a listener
     */
    public void queryPurchases() {
        Runnable queryToExecute = new Runnable() {
            @Override
            public void run() {
                long time = System.currentTimeMillis();
                PurchasesResult purchasesResult = mBillingClient.queryPurchases(SkuType.INAPP);
                Log.e(TAG, "Querying purchases elapsed time: " + (System.currentTimeMillis() - time) + "ms");
                // If there are subscriptions supported, we add subscription rows as well
                // commented subscription code
                if (areSubscriptionsSupported()) {
                    PurchasesResult subscriptionResult = mBillingClient.queryPurchases(SkuType.SUBS);
                    Log.e(TAG, "Querying purchases and subscriptions elapsed time: " + (System.currentTimeMillis() - time) + "ms");
                    Log.e(TAG, "Querying subscriptions response code: " + subscriptionResult.getResponseCode()
                            + " Purchase List Size: " + subscriptionResult.getPurchasesList().size());

                    if (subscriptionResult.getResponseCode() == BillingClient.BillingResponseCode.OK) {
                        purchasesResult.getPurchasesList().addAll(subscriptionResult.getPurchasesList());
                    } else {
                        Log.e(TAG, "Got an error response trying to query subscription purchases");
                    }
                } else if (purchasesResult.getResponseCode() == BillingClient.BillingResponseCode.OK) {
                    Log.e(TAG, "Skipped subscription purchases query since they are not supported");
                } else {
                    Log.e(TAG, "queryPurchases() got an error response code: " + purchasesResult.getResponseCode());
                }
                onQueryPurchasesFinished(purchasesResult);
            }
        };

        executeServiceRequest(queryToExecute);
    }

    /**
     * Handle a result from querying of purchases and report an updated list to the listener
     */
    private void onQueryPurchasesFinished(PurchasesResult result) {
        // Have we been disposed of in the meantime? If so, or bad result code, then quit
        if (mBillingClient == null || result.getResponseCode() != BillingClient.BillingResponseCode.OK) {
            Log.e(TAG, "Billing client was null or result code (" + result.getResponseCode() + ") was bad - quitting");
            return;
        }

        Log.e(TAG, "Query inventory was successful.");

        // Update the UI and purchases inventory with new list of purchases
        mPurchases.clear();
        // onPurchasesUpdated(BillingResponse.OK, result.getPurchasesList());
        mBillingUpdatesListener.onPurchasesUpdated(result.getPurchasesList());
    }

    private void executeServiceRequest(Runnable runnable) {
        if (mIsServiceConnected) {
            runnable.run();
        } else {
            // If billing service was disconnected, we try to reconnect 1 time.
            // (feel free to introduce your retry policy here).
            startServiceConnection(runnable);
        }
    }

    /**
     * Start a purchase or subscription replace flow
     */
    public void initiatePurchaseFlow(final SkuDetails mSkuDetails /* ,final ArrayList<String> oldSkus, final @SkuType String billingType */) {

        Runnable purchaseFlowRequest = new Runnable() {
            @Override
            public void run() {
//                Log.e(TAG, "Launching in-app purchase flow. Replace old SKU? " + (oldSkus != null));
                BillingFlowParams purchaseParams = BillingFlowParams.newBuilder()
                        .setSkuDetails(mSkuDetails)
                        /* .setType(billingType)
                         .setold(oldSkus)*/
                        .build();
                BillingResult mBillingResult = mBillingClient.launchBillingFlow(mActivity, purchaseParams);
                Log.e(TAG, "initiatePurchaseFlow result: " + mBillingResult.getResponseCode() + " MSG: " + mBillingResult.getDebugMessage());
            }
        };

        executeServiceRequest(purchaseFlowRequest);
    }

    /**
     * Handle a callback that purchases were updated from the Billing library
     */
    @Override
    public void onPurchasesUpdated(@NonNull BillingResult billingResult, @Nullable List<Purchase> purchases) {
        if (billingResult.getResponseCode() == BillingClient.BillingResponseCode.OK) {
            for (Purchase purchase : purchases) {
                handlePurchase(purchase);
            }
            // mBillingUpdatesListener.onPurchasesUpdated(mPurchases);
//            mBillingUpdatesListener.onPurchasesFinished(mPurchases); // working commented on 30 9 2020
        } else if (billingResult.getResponseCode() == BillingClient.BillingResponseCode.USER_CANCELED) {
            Log.e(TAG, "onPurchasesUpdated() - user cancelled the purchase flow - skipping");
        } else {
            Log.e(TAG, "onPurchasesUpdated() got unknown resultCode: " + billingResult.getResponseCode());
        }
    }

    /**
     * Start a purchase flow
     */
//    public void initiatePurchaseFlow(final SkuDetails mSkuDetails, final @SkuType String billingType) {
//        initiatePurchaseFlow(mSkuDetails, null, billingType);
//    }


    /**
     * Handles the purchase
     * <p>Note: Notice that for each purchase, we check if signature is valid on the client.
     * It's recommended to move this check into your backend.
     * See {@link Security#verifyPurchase(String, String, String)}
     * </p>
     *
     * @param purchase Purchase to be handled
     */
    private void handlePurchase(Purchase purchase) {
        /*
        Got a verified purchase:Purchase.Json:
        Subscription
        {
            "orderId":"GPA.3359-8484-4414-14044", "packageName":"com.recipeapp.swipeeatmeet",
            "productId":"standard_subscription", "purchaseTime":1601447139623,
            "purchaseState":0, "purchaseToken":"ibaeikennbmopgkdjgaakkic.AO-J1OwNkPtjdNlT67HwSZENhSIkv8rq3cDv9h6_YO1lBb4c3nqrWs3n44uRKxmMzRLxP2vcVx-IsN04oAH6oyNKpVxlqy9uTtj-Z9PUz_xOLBRyrd9tR63QjriOzalI9ei5MkY3phKQsUOl9ce1CtFblEJ9fr5lbw",
            "autoRenewing":true, "acknowledged":false
        }

        Consumable product Json
        {
            "orderId":"GPA.3316-8196-5340-49057", "packageName": "com.recipeapp.swipeeatmeet",
            "productId":"connection", "purchaseTime": 1600185076761,
            "purchaseState":0, "purchaseToken": "bbiomkgkbjmgpopekmmgmdnd.AO-J1OypEhHWOh83cfvlySkLpHFBwZCMn8QChhsXE1cFdOwISS9Nw6Mpy6_2TR1wDK9GWR55jvaHjoEevXl4Bh6sduMC3zbgXsuAVcph4Kt7OFH1r4QY1hcmnR_AM8jtfG77Xg_wx8hp"
        }

        */

        if (!verifyValidSignature(purchase.getOriginalJson(), purchase.getSignature())) {
            Log.e(TAG, "Got a purchase: " + purchase + "; but signature is bad. Skipping...");
            return;
        }

        Log.e(TAG, "Got a verified purchase: " + purchase);

        mPurchases.add(purchase);

        // This is the Test purchase like Test ads IDs will not affect to Any project IAB
        if (purchase.getSku().equals(PurchaseItem.IAB_TEST_CONSUMABLE_PRODUCT.toString())) {
            consumeAsync(purchase.getPurchaseToken());
        }

        if (purchase.getSku().equals(PurchaseItem.IAB_PRODUCT_CONNECTION.toString())) {

            Log.e(TAG, "consumeAsync will be called: ");
            consumeAsync(purchase.getPurchaseToken());
        } else {

            Log.e(TAG, "handleNonConsumableProductAcknowledgePurchase will be called: ");
            handleNonConsumableProductAcknowledgePurchase(purchase);
        }
    }

    public void querySkuDetailsAsync(@SkuType final String itemType, final List<String> skuList, final SkuDetailsResponseListener listener) {
        // Creating a runnable from the request to use it inside our connection retry policy below
        Runnable queryRequest = new Runnable() {
            @Override
            public void run() {
                // Query the purchase async
                SkuDetailsParams.Builder params = SkuDetailsParams
                        .newBuilder()
                        .setSkusList(skuList)
                        .setType(itemType);
                mBillingClient.querySkuDetailsAsync(params.build(), new SkuDetailsResponseListener() {
                    @Override
                    public void onSkuDetailsResponse(@NonNull BillingResult billingResult, @Nullable List<SkuDetails> list) {
                        if (list != null && billingResult.getResponseCode() == BillingClient.BillingResponseCode.OK) {
                            listener.onSkuDetailsResponse(billingResult, list);
                        } else {
                            Log.e(TAG, "querySkuDetailsAsync() got an error response code: " + billingResult.getResponseCode());
                        }
                    }

                });
            }
        };

        executeServiceRequest(queryRequest);
    }


    /**
     * Checks if subscriptions are supported for current client
     * <p>Note: This method does not automatically retry for RESULT_SERVICE_DISCONNECTED.
     * It is only used in unit tests and after queryPurchases execution, which already has
     * a retry-mechanism implemented.
     * </p>
     */
    public boolean areSubscriptionsSupported() {
        int responseCode = mBillingClient.isFeatureSupported(FeatureType.SUBSCRIPTIONS).getResponseCode();
        if (responseCode != BillingClient.BillingResponseCode.OK) {
            Log.e(TAG, "areSubscriptionsSupported() got an error response: " + responseCode);
        }
        return responseCode == BillingClient.BillingResponseCode.OK;
    }

    /**
     * Verifies that the purchase was signed correctly for this developer's public key.
     * <p>Note: It's strongly recommended to perform such check on your backend since hackers can
     * replace this method with "constant true" if they decompile/rebuild your app.
     * </p>
     */
    private boolean verifyValidSignature(String signedData, String signature) {
        // Some sanity checks to see if the developer (that's you!) really followed the
        // instructions to run this sample (don't put these checks on your app!)
        if (BASE_64_ENCODED_PUBLIC_KEY.contains("CONSTRUCT_YOUR")) {
            throw new RuntimeException("Please update your app's public key at: "
                    + "BASE_64_ENCODED_PUBLIC_KEY");
        }

        try {
            return Security.verifyPurchase(BASE_64_ENCODED_PUBLIC_KEY, signedData, signature);
        } catch (IOException e) {
            Log.e(TAG, "Got an exception trying to validate a purchase: " + e);
            return false;
        }
    }


    public void consumeAsync(final String purchaseToken) {
        // If we've already scheduled to consume this token - no action is needed (this could happen
        // if you received the token when querying purchases inside onReceive() and later from
        // onActivityResult()
        if (mTokensToBeConsumed == null) {
            mTokensToBeConsumed = new HashSet<>();
        } else if (mTokensToBeConsumed.contains(purchaseToken)) {
            Log.e(TAG, "Token was already scheduled to be consumed - skipping...");
            return;
        }
        mTokensToBeConsumed.add(purchaseToken);

        handleConsumableProduct(purchaseToken);

       /* // Generating Consume Response listener
        final ConsumeResponseListener onConsumeListener = new ConsumeResponseListener() {
            @Override
            public void onConsumeResponse(@NonNull BillingResult billingResult, @NonNull String s) {
                // If billing service was disconnected, we try to reconnect 1 time
                // (feel free to introduce your retry policy here).
                Log.e(TAG, "onConsumeResponse: Consumption finished S: " + s);
                mBillingUpdatesListener.onConsumeFinished(purchaseToken, billingResult.getResponseCode());

            }
        };

        // Creating a runnable from the request to use it inside our connection retry policy below
        Runnable consumeRequest = new Runnable() {
            @Override
            public void run() {
                // Consume the purchase async
                ConsumeParams consumeParams = ConsumeParams
                        .newBuilder()
                        .setPurchaseToken(purchaseToken)
                        .build();

                mBillingClient.consumeAsync(consumeParams, onConsumeListener);
//                mBillingClient.consumeAsync(purchaseToken, onConsumeListener);
            }
        };

        executeServiceRequest(consumeRequest);*/
    }

    // new code
    private void handleConsumableProduct(String purchaseToken) {

       /* ConsumeParams consumeParams = ConsumeParams.newBuilder()
                .setPurchaseToken(purchaseToken)
                .build();

        mBillingClient.consumeAsync(consumeParams, new ConsumeResponseListener() {
            @Override
            public void onConsumeResponse(@NonNull BillingResult billingResult, @NonNull String s) {
                if (billingResult.getResponseCode() == BillingClient.BillingResponseCode.OK) {
                    // Handle the success of the consume operation.
                }
            }
        });
*/
        // Generating Consume Response listener
        final ConsumeResponseListener onConsumeListener = new ConsumeResponseListener() {
            @Override
            public void onConsumeResponse(@NonNull BillingResult billingResult, @NonNull String s) {
                // If billing service was disconnected, we try to reconnect 1 time
                // (feel free to introduce your retry policy here).
                Log.e(TAG, "onConsumeResponse: Consumption finished S: " + s);
                mBillingUpdatesListener.onConsumeFinished(purchaseToken, billingResult.getResponseCode());

            }
        };

        // Creating a runnable from the request to use it inside our connection retry policy below
        Runnable consumeRequest = new Runnable() {
            @Override
            public void run() {
                // Consume the purchase async
                ConsumeParams consumeParams = ConsumeParams
                        .newBuilder()
                        .setPurchaseToken(purchaseToken)
                        .build();

                mBillingClient.consumeAsync(consumeParams, onConsumeListener);
//                mBillingClient.consumeAsync(purchaseToken, onConsumeListener);
            }
        };

        executeServiceRequest(consumeRequest);

    }

    /**
     * Handle a callback after successfuly purchases to acknowledge purchase success
     */
    private void handleNonConsumableProductAcknowledgePurchase(Purchase purchase) {
        if (!purchase.isAcknowledged()) {
            AcknowledgePurchaseParams acknowledgePurchaseParams = AcknowledgePurchaseParams
                    .newBuilder()
                    .setPurchaseToken(purchase.getPurchaseToken())
                    .build();

            mBillingClient.acknowledgePurchase(acknowledgePurchaseParams, new AcknowledgePurchaseResponseListener() {
                @Override
                public void onAcknowledgePurchaseResponse(@NonNull BillingResult billingResult) {
                    Log.e(TAG, "onAcknowledgePurchaseResponse called ");

                    if (billingResult.getResponseCode() == BillingClient.BillingResponseCode.OK) {
                        Log.e(TAG, "onAcknowledgePurchaseResponse PURCHASED PRODUCT HAS BEEN ACKNOWLEDGED SUCCESSFULLY ");
                        mBillingUpdatesListener.onPurchasesFinished(mPurchases); // working commented on 30 9 2020
                    }
                }
            });

        }


    }


    /**
     * Listener to the updates that happen when purchases list was updated or consumption of the
     * item was finished
     */
    public interface BillingUpdatesListener {

        void onBillingClientSetupFinished();

        void onPurchasesUpdated(List<Purchase> purchases);

        void onPurchasesFinished(List<Purchase> purchases);

        void onConsumeFinished(String token, @BillingClient.BillingResponseCode int result);
    }

    /**
     * Listener for the Billing client state to become connected
     */
    public interface ServiceConnectedListener {
        void onServiceConnected(@BillingClient.BillingResponseCode int resultCode);
    }


    /**
     * Returns the value Billing client response code or BILLING_MANAGER_NOT_INITIALIZED if the
     * clien connection response was not received yet.
     */
    public int getBillingClientResponseCode() {
        return mBillingClientResponseCode;
    }

    /**
     * Clear the resources
     */
    public void destroy() {
        Log.e(TAG, "Destroying the manager.");

        if (mBillingClient != null && mBillingClient.isReady()) {
            mBillingClient.endConnection();
            mBillingClient = null;
        }
    }
}

