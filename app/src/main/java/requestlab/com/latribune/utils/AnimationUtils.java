package requestlab.com.latribune.utils;

import android.app.Activity;

import requestlab.com.latribune.R;

/**
 * Created by lchazal on 17/03/16.
 */
public class AnimationUtils {

    public static void animateIn(Activity activity) {
        activity.overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
    }

    public static void animateOut(Activity activity) {
        activity.overridePendingTransition(R.anim.in_from_left, R.anim.out_to_right);
    }
}
