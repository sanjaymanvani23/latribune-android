package requestlab.com.latribune.utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;


import androidx.browser.customtabs.CustomTabsIntent;
import androidx.core.content.ContextCompat;

import requestlab.com.latribune.R;
import requestlab.com.latribune.chrome.CustomTabActivityHelper;

/**
 * Created by lchazal on 03/05/16.
 */
public class NetworkUtils {

    public static void sendEmail(Context context,
                                 String destination,
                                 String subject,
                                 String content) {
        String uriText = "mailto:"
                + destination
                + "?subject="
                + Uri.encode(subject)
                + "&body="
                + Uri.encode(content);
        Uri uri = Uri.parse(uriText);

        Intent sendIntent = new Intent(Intent.ACTION_SENDTO);
        sendIntent.setData(uri);
        context.startActivity(Intent.createChooser(sendIntent, "Envoyer via"));
    }

    /**
     * Open an Uri into Chrome's custom tab service
     *
     * @param activity current Activity context
     * @param uri Uri containing the target page to load
     */
    public static void openUrlInCustomTab(Activity activity, Uri uri) {
        CustomTabsIntent.Builder intentBuilder = new CustomTabsIntent.Builder();
        intentBuilder.setToolbarColor(ContextCompat.getColor(activity, R.color.colorPrimary));
        intentBuilder.setStartAnimations(activity, R.anim.in_from_right, R.anim.out_to_left);
        intentBuilder.setExitAnimations(activity, R.anim.in_from_left, R.anim.out_to_right);
        intentBuilder.setShowTitle(true);

        CustomTabsIntent customTabsIntent = intentBuilder.build();
        CustomTabActivityHelper.openCustomTab(activity, customTabsIntent, uri,
                new CustomTabActivityHelper.CustomTabFallback() {
                    @Override
                    public void openUri(Activity activity, Uri uri) {
                        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                        activity.startActivity(intent);
                    }
                });
    }

    public static void  openAppOrUrl(Context context, String packageName) {
        Intent intent = context.getPackageManager().getLaunchIntentForPackage(packageName);
        if (intent != null) {
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(intent);
        } else {
            openUrlInCustomTab((Activity) context, Uri.parse("https://play.google.com/store/apps/details?id=" + packageName));
        }
    }
}
