package requestlab.com.latribune.utils;

import com.crashlytics.android.Crashlytics;

import java.io.IOException;

import retrofit2.Response;

/**
 * Created by lchazal on 22/07/16.
 */
public class ErrorLogger {

    public static void  logBadArticleToCrashlytics(int articleId, String callerTag) {
        logBadArticleToCrashlytics(articleId, callerTag, "");
    }

    public static void  logBadArticleToCrashlytics(int articleId, String callerTag, String extra) {
        try {
            throw new CrashlyticsBadArticleIdException(articleId, callerTag, extra);
        } catch (CrashlyticsBadArticleIdException e) {
            Crashlytics.logException(e);
        }
    }

    public static void  logNetworkErrorToCrashlytics(Response response) {
        String s = "";
        try {
            s = response.errorBody().string();
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            throw new CrashlyticsNetworkReportException(s);
        } catch (CrashlyticsNetworkReportException e) {
            Crashlytics.logException(e);
        }
    }

    public static class     CrashlyticsBadArticleIdException extends Exception {
        public CrashlyticsBadArticleIdException(int articleId, String callerName, String extra) {
            super("Wrong articleId : " + articleId + " from : " + callerName + ". " + extra);
        }
    }

    public static class     CrashlyticsNetworkReportException extends Exception {
        public CrashlyticsNetworkReportException(String message) {
            super(message);
        }
    }
}
