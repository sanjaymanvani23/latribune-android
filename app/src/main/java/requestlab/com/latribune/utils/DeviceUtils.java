package requestlab.com.latribune.utils;

import android.content.Context;
import android.graphics.Point;
import android.view.Display;
import android.view.WindowManager;

/**
 * Created by lchazal on 01/04/16.
 */
public class DeviceUtils {

    public static int screenHeight = -1;
    public static int screenWidth = -1;

    public static int getDeviceScreenHeight(Context context) {
        if (screenHeight == -1) {
            calculateScreenDimensions(context);
        }
        return screenHeight;
    }

    public static int getDeviceScreenWidth(Context context) {
        if (screenWidth == -1) {
           calculateScreenDimensions(context);
        }
        return screenWidth;
    }

    private static void calculateScreenDimensions(Context context) {
        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        screenHeight = size.y;
        screenWidth = size.x;
    }
}
