package requestlab.com.latribune.utils;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;

import com.mngads.MNGAdsFactory;
import com.mngads.MNGNativeObject;
import com.mngads.listener.MNGBannerListener;
import com.mngads.listener.MNGInterstitialListener;
import com.mngads.listener.MNGNativeListener;
import com.mngads.util.MNGFrame;

import requestlab.com.latribune.R;

/**
 * Created by lchazal on 19/05/16.
 *
 */
public class AdManager {

    public static final int MNG_AD_APP_KEY = 8177862;

    public enum AdType {
        BANNER("banner"),
        INTERSTITIAL("interstitial"),
        INTERSTITIAL_OVERLAY("interstitialOverlay"),
        NATIVE("nativead"),
        SQUARE("square");

        private final String placementId;

        AdType(String placementId) {
            this.placementId = placementId;
        }

        public String getPlacementId() {
            return placementId;
        }
    }

    private static ExposedMNGAdsFactory    currentFactory;

    public static void  killCurrentFactory() {
        if (currentFactory != null) {
            currentFactory.releaseMemory();
            currentFactory = null;
        }
    }

    public static void  displayInterstitial() {
        if (currentFactory != null && currentFactory.isInterstitialReady()) currentFactory.displayInterstitial();
    }

    public static AsyncTask<Void, Void, Void>   buildMNGAdLoadingTask(final Context context,
                                                                      final IAdLoadedListener listener,
                                                                      final AdType type) {

        return new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                ((Activity) context).runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        boolean canHandleRequest = loadAd(context, listener, type);
                        if (canHandleRequest) Log.d("MNGADS", "AdsFactory Created");
                        else Log.d("MNGADS", "AdsFactory couldn't handle request");
                    }
                });
                return null;
            }
        };
    }

    public interface IAdLoadedListener {
        void onAdLoaded(View view, int i);
        void onNativeAdLoaded(MNGNativeObject object);
        void onAdFailed(Exception e);
        void onAdChanged(MNGFrame mngFrame);
    }

    private static boolean loadAd(final Context context,
                               final IAdLoadedListener listener,
                               AdType type) {

//        currentFactory = new ExposedMNGAdsFactory(context);
//
//        switch (type) {
//            case BANNER:
//                createBanner(currentFactory, listener);
//                setPlacementId(currentFactory, type);
//                return currentFactory.createBanner(new MNGFrame(DeviceUtils.getDeviceScreenWidth(context),
//                        (int) context.getResources().getDimension(R.dimen.mngads_banner_height)));
//            case SQUARE:
//                createBanner(currentFactory, listener);
//                setPlacementId(currentFactory, type);
//                return currentFactory.createBanner(new MNGFrame(DeviceUtils.getDeviceScreenWidth(context),
//                        (int) context.getResources().getDimension(R.dimen.mngads_square_height)));
//            case INTERSTITIAL_OVERLAY:
//                createInterstitial(currentFactory, listener);
//                setPlacementId(currentFactory, type);
//                return currentFactory.createInterstitial();
//            case INTERSTITIAL:
//                createInterstitial(currentFactory, listener);
//                setPlacementId(currentFactory, type);
//                return currentFactory.createInterstitial(false);
//            case NATIVE:
//                createNative(currentFactory, listener);
//                setPlacementId(currentFactory, type);
//                return currentFactory.createNative();
//        }
        return false;
    }

    private static void setPlacementId(MNGAdsFactory mngAdsFactory, AdType type) {
        Log.d("MNG_ADS", "Setting placement ID : " + "/" + MNG_AD_APP_KEY + "/" + type.getPlacementId());
        mngAdsFactory.setPlacementId("/" + MNG_AD_APP_KEY + "/" + type.getPlacementId());
    }

    private static void createBanner(MNGAdsFactory mngAdsFactory,
                                     final IAdLoadedListener listener) {
        mngAdsFactory.setBannerListener(new MNGBannerListener() {
            @Override
            public void bannerDidLoad(View view, int i) {
                if (listener != null) listener.onAdLoaded(view, i);
            }

            @Override
            public void bannerDidFail(Exception e) {
                if (listener != null) listener.onAdFailed(e);
            }

            @Override
            public void bannerResize(MNGFrame mngFrame) {
                if (listener != null) listener.onAdChanged(mngFrame);
            }
        });
    }

    private static void createInterstitial(final ExposedMNGAdsFactory mngAdsFactory,
                                           final IAdLoadedListener listener) {
        mngAdsFactory.setInterstitialListener(new MNGInterstitialListener() {
            @Override
            public void interstitialDidLoad() {
                Log.d("AdManager", "interstitialDidLoad");
                if (listener != null) listener.onAdLoaded(null, 0);
            }

            @Override
            public void interstitialDidFail(Exception e) {
                Log.d("AdManager", "interstitialDidFail");
                if (listener != null) listener.onAdFailed(e);
            }

            @Override
            public void interstitialDisappear() {
                Log.d("AdManager", "interstitialDisappear");
                if (listener != null) listener.onAdChanged(null);
            }
        });
    }

    private static void createNative(final ExposedMNGAdsFactory mngAdsFactory,
                                           final IAdLoadedListener listener) {
        mngAdsFactory.setNativeListener(new MNGNativeListener() {
            @Override
            public void nativeObjectDidLoad(MNGNativeObject mngNativeObject) {
                if (listener != null) listener.onNativeAdLoaded(mngNativeObject);
            }

            @Override
            public void nativeObjectDidFail(Exception e) {
                if (listener != null) listener.onAdFailed(e);
            }
        });
    }

    private static class ExposedMNGAdsFactory extends MNGAdsFactory {

        public ExposedMNGAdsFactory(Context context) {
            super(context);
        }

        public boolean createInterstitial(boolean autoLoad) {
            loadInterstitial(autoLoad);
            return autoLoad;
        }

        public boolean displayInterstitial() {
            return super.displayInterstitial();
        }

        public boolean isInterstitialReady() {
            return super.isInterstitialReady();
        }
    }
}
