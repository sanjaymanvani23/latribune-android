package requestlab.com.latribune.utils;

import android.content.Context;
import android.util.Log;

import java.util.List;

import requestlab.com.latribune.api.TribuneAPI;
import requestlab.com.latribune.api.listeners.APICallbackListener;
import requestlab.com.latribune.cache.TribuneCache;
import requestlab.com.latribune.data.Category;

/**
 * Created by lchazal on 20/05/16.
 */
public class DataManager {
    private static DataManager ourInstance = new DataManager();

    public static DataManager getInstance() {
        return ourInstance;
    }

    private Context applicationContext;

    private DataManager() {
    }

    public void init(Context context) {
        applicationContext = context;
    }

    interface DataLoadedListener {
        void    onDataLoaded(boolean isSuccess);
    }

    private boolean categoryDataLoaded = false;
    private int     categoryDataLoadRetryCount = 0;

    public boolean isCategoryDataLoaded() {
        return categoryDataLoaded;
    }

    public void loadCategoryData() {
        loadCategoryData(null, 3);
    }

    public void loadCategoryData(int retryCount) {
        loadCategoryData(null, retryCount);
    }

    public void loadCategoryData(final DataLoadedListener listener) {
        loadCategoryData(listener, 3);
    }

    public void loadCategoryData(final DataLoadedListener listener, final int retryCount) {
        TribuneAPI.getInstance().getCategories(new APICallbackListener<Category>() {
            @Override
            public void onResponseSuccess(Category response) {
                Log.e("JACKSON", "loadCategoryData success");
                Log.e("JACKSON", "Category name : " + response.getName());
                Log.e("JACKSON", "id : " + response.getId());
                Log.e("JACKSON", "position : " + response.getPosition());
                Log.e("JACKSON", "name : " + response.getName());
                List<Category> cf = response.getCategories();
                if (cf == null) {
                    Log.e("JACKSON", "category list null");
                    return;
                } else {
                    Log.e("JACKSON", "category size : " + cf.size());
                }

                for (Category c :
                        response.getCategories()) {
                    Category saved = TribuneCache.getInstance().getObject(Category.class, c.getId());
                    if (saved != null) c.setHidden(saved.isHidden());
                }
                TribuneCache.getInstance().addOrUpdateObjectList(response.getCategories());
                categoryDataLoaded = true;
                if (listener != null) listener.onDataLoaded(true);
            }

            @Override
            public void onResponseFailure(String message) {
                if (retryCount > 0) loadCategoryData(listener, retryCount - 1);
                else if (listener != null) listener.onDataLoaded(false);
            }

            @Override
            public void onFailure(String message) {
                if (retryCount > 0) loadCategoryData(listener, retryCount - 1);
                else if (listener != null) listener.onDataLoaded(false);
            }
        });
    }
}
