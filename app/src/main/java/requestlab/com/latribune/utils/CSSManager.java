package requestlab.com.latribune.utils;

import android.content.Context;
import android.content.res.Resources;
import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;

import requestlab.com.latribune.R;

/**
 * Created by lchazal on 29/04/16.
 */
public class CSSManager {
    private static CSSManager ourInstance = new CSSManager();

    public static CSSManager getInstance() {
        return ourInstance;
    }

    private CSSManager() {
    }

    private String cssString;
    private String formatedCssString;
    private boolean validCSS = false;

    public void init(Context context) {
        readCssFromAssets(context);
    }

    private void readCssFromAssets(Context context) {
        try {
            StringBuilder buf = new StringBuilder();
            InputStream json = context.getAssets().open("style.css");
            BufferedReader in=
                    new BufferedReader(new InputStreamReader(json, "UTF-8"));
            String str;

            while ((str=in.readLine()) != null) {
                buf.append(str);
            }

            in.close();

            cssString = buf.toString();
            validCSS = true;
            formatCssString(context);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            cssString = "Error Reading CSS file";
            validCSS = false;
        } catch (IOException e) {
            e.printStackTrace();
            cssString = "Error Opening CSS file";
            validCSS = false;
        }

    }

    public void formatCssString(Context context) {
        if (!validCSS) return;

        Resources res = context.getResources();
        int pSize = FontPreferencesManager.getInstance().getpSize();
        int hSize = FontPreferencesManager.getInstance().getHeaderSize();
        int shSize = FontPreferencesManager.getInstance().getSubHeaderSize();

        String buf = cssString.replace(res.getString(R.string.texte_size_css_p_media_small), String.valueOf(pSize));
        buf = buf.replace(res.getString(R.string.texte_size_css_p_media_medium), String.valueOf(pSize * 2));
        buf = buf.replace(res.getString(R.string.texte_size_css_p_media_large), String.valueOf(pSize * 3));
        buf = buf.replace(res.getString(R.string.texte_size_css_h1_media_small), String.valueOf(hSize));
        buf = buf.replace(res.getString(R.string.texte_size_css_h1_media_medium), String.valueOf(hSize * 2));
        buf = buf.replace(res.getString(R.string.texte_size_css_h1_media_large), String.valueOf(hSize * 3));
        buf = buf.replace(res.getString(R.string.texte_size_css_h2_media_small), String.valueOf(shSize));
        buf = buf.replace(res.getString(R.string.texte_size_css_h2_media_medium), String.valueOf(shSize * 2));
        buf = buf.replace(res.getString(R.string.texte_size_css_h2_media_large), String.valueOf(shSize * 3));

        formatedCssString = buf;
        Log.d("FORMAT", "Formatted CSS string : " + formatedCssString);
    }

    public String formatHTMLString(String htmlString) {
        if (validCSS) {
             return "<html><head><style type=\"text/css\">" + formatedCssString + "</style></head><body>" + htmlString +"</body></html>";
        }
        Log.e("FORMAT", "Returned non formatted string : " + cssString);
        return htmlString;
    }
}
