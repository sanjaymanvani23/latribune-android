package requestlab.com.latribune.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;
import com.squareup.picasso.Transformation;

import requestlab.com.latribune.data.Media;

/**
 * Created by lchazal on 30/09/15.
 */
public class ImageManager {

    private Picasso mPicasso;

    private static ImageManager ourInstance = new ImageManager();

    public static ImageManager getInstance() {
        return ourInstance;
    }

    private ImageManager() {
    }

    public void init(Context context) {
        mPicasso = new Picasso.Builder(context)
//                .downloader(new OkHttpDownloader(getPicassoDownloaderClient()))
                .build();
    }

    public static void      preloadImageFromURL(Context context, String url) {
        Picasso.with(context).load(url).fetch();
    }

//    public static Bitmap    scaleBitmap(Bitmap bitmap, int width, int height) {
//        return  Bitmap.createScaledBitmap(bitmap, width, height, false);
//    }
//
//    public static Bitmap    cropBitmap(Bitmap bitmap) {
//        Bitmap croppedBitmap;
//        if (bitmap.getWidth() >= bitmap.getHeight()) {
//            croppedBitmap = Bitmap.createBitmap(bitmap, bitmap.getWidth()/2 - bitmap.getHeight()/2,
//                    0, bitmap.getHeight(), bitmap.getHeight());
//        }else{
//            croppedBitmap = Bitmap.createBitmap(bitmap, 0,
//                    bitmap.getHeight()/2 - bitmap.getWidth()/2,
//                    bitmap.getWidth(), bitmap.getWidth());
//        }
//        return croppedBitmap;
//    }

//    public static RoundImage getRoundImage(Bitmap rawBitmap, int width, int height) {
//        Bitmap bitmap = ImageManager.cropBitmap(rawBitmap);
//        bitmap = ImageManager.scaleBitmap(bitmap, width, height);
//
//        return new RoundImage(bitmap);
//    }

    public static void  loadImageIntoTarget(Context context, String url, Target target, int width, int height) {
        Picasso.with(context).load(url).resize(width, height).into(target);
    }

    public void  fitImageIntoView(final String url, ImageView image, int placeholderId) {
        mPicasso
                .load(url)
                .placeholder(placeholderId)
                .error(placeholderId)
                .fit()
                .centerCrop()
                .into(image);
    }

    public void  fitImageIntoView(final String url, ImageView image) {
        fitImageIntoView(url, image, android.R.color.transparent);
    }

    public void  loadImageIntoView(String url, ImageView image) {
        mPicasso
                .load(url)
                .into(image);
    }

    public void  loadImageIntoView(String url, ImageView image, int errorImageId) {
        if (errorImageId <= 0) loadImageIntoView(url, image);

        mPicasso
                .load(url)
                .error(errorImageId)
                .into(image);
    }

    public void loadImageCroppedBottom(String url, ImageView image, int placeholderId) {
        mPicasso
                .load(url)
                .placeholder(placeholderId)
                .error(placeholderId)
                .fit()
                .into(image);
    }

    public void  loadRoundImageIntoView(String url, ImageView image) {
        mPicasso
                .load(url)
                .fit()
                .transform(new CircleTransform())
                .into(image);
    }

    private static class CircleTransform implements Transformation {
        @Override
        public Bitmap transform(Bitmap source) {
            int size = Math.min(source.getWidth(), source.getHeight());

            int x = (source.getWidth() - size) / 2;
            int y = (source.getHeight() - size) / 2;

            Bitmap squaredBitmap = Bitmap.createBitmap(source, x, y, size, size);
            if (squaredBitmap != source) {
                source.recycle();
            }

            Bitmap bitmap = Bitmap.createBitmap(size, size, squaredBitmap.getConfig());

            Canvas canvas = new Canvas(bitmap);
            Paint paint = new Paint();
            BitmapShader shader = new BitmapShader(squaredBitmap,
                    BitmapShader.TileMode.CLAMP, BitmapShader.TileMode.CLAMP);
            paint.setShader(shader);
            paint.setAntiAlias(true);

            float r = size / 2f;
            canvas.drawCircle(r, r, r, paint);

            squaredBitmap.recycle();
            return bitmap;
        }

        @Override
        public String key() {
            return "circle";
        }
    }

    public static String  getBestUrl(Media m, boolean isLarge) {
        if (m == null) return null;
        String url;
        if (m.getFormats() != null) {
            if (!isLarge) {
                url = m.getFormats().getSmall();
                if (url != null && !url.isEmpty()) return url;
            }
            url = m.getFormats().getLarge();
            if (url != null && !url.isEmpty()) return url;
            url = m.getFormats().getOriginal();
            if (url != null && !url.isEmpty()) return url;
            return m.getUrl();
        } else return m.getUrl();
    }

//    public static OkHttpClient getPicassoDownloaderClient() {
//        OkHttpClient picassoClient = new OkHttpClient();
//
//        picassoClient.networkInterceptors().add(new Interceptor() {
//            @Override
//            public Response intercept(Chain chain) throws IOException {
//                Request newRequest = chain.request().newBuilder()
//                        .addHeader("Authorization", INSTANCE.BEARER_TOKEN + " " + Session.getInstance().getAccessToken())
//                        .build();
//                return chain.proceed(newRequest);
//            }
//        });
//        return picassoClient;
//    }
}