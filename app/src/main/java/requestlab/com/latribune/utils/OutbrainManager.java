package requestlab.com.latribune.utils;

import android.content.Context;
import android.util.Log;

import com.outbrain.OBSDK.Entities.OBRecommendation;
import com.outbrain.OBSDK.FetchRecommendations.OBRequest;
import com.outbrain.OBSDK.FetchRecommendations.RecommendationsListener;
import com.outbrain.OBSDK.Outbrain;
import com.outbrain.OBSDK.OutbrainException;

import requestlab.com.latribune.BuildConfig;

/**
 * Created by lchazal on 31/05/16.
 */
public class OutbrainManager {
    private static OutbrainManager ourInstance = new OutbrainManager();

    public static OutbrainManager getInstance() {
        return ourInstance;
    }

    private OutbrainManager() {
    }

    private boolean initialized = false;

    public void initialize(Context context) {
        try {
            Outbrain.register(context, BuildConfig.OUTBRAIN_KEY);
        } catch (OutbrainException ex) {
            Log.e("OUTBRAIN", ex.getMessage());
            initialized = false;
            return;
        }
        initialized = true;
        if (BuildConfig.DEBUG) Outbrain.setTestMode(true);
    }

    public void getRecommendations(String url, RecommendationsListener listener) {
        if (!initialized) return;

        OBRequest request = new OBRequest(url, BuildConfig.OUTBRAIN_ID);
        Outbrain.fetchRecommendations(request, listener);
    }

    public String getRecommendationUrl(OBRecommendation rec) {
        if (!initialized) return "";

        return Outbrain.getOriginalContentURLAndRegisterClick(rec);
    }
}
