package requestlab.com.latribune.utils;

import android.content.Intent;

/**
 * Created by Léo on 16/03/2016.
 *
 * A helper creating share intents used by the app to send URLs
 */
public class ShareManager {

    public static Intent createShareIntent(String message) {
        Intent intent = new Intent();

        intent.setAction(Intent.ACTION_SEND);
        intent.putExtra(Intent.EXTRA_TEXT, message);
        intent.setType("text/plain");
        return intent;
    }
}
