package requestlab.com.latribune.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import requestlab.com.latribune.R;

/**
 * Created by lchazal on 29/04/16.
 */
public class FontPreferencesManager {

    private static FontPreferencesManager ourInstance = new FontPreferencesManager();

    public static FontPreferencesManager getInstance() {
        return ourInstance;
    }

    private FontPreferencesManager() {
    }

    public void fetchValues(Context context) {
        SharedPreferences settings = context.getSharedPreferences(SHARED_PREF_TAG_FONTS_FILE, Context.MODE_PRIVATE);
        pSize = settings.getInt(SHARED_PREF_TAG_P, context.getResources().getInteger(R.integer.css_p_default_size));
        headerSize = settings.getInt(SHARED_PREF_TAG_H1, context.getResources().getInteger(R.integer.css_h1_default_size));
        subHeaderSize = settings.getInt(SHARED_PREF_TAG_H2, context.getResources().getInteger(R.integer.css_h2_default_size));

        Log.d("FONT_MANAGER", "Fetched values : " + pSize + " " + headerSize + " " + subHeaderSize);
    }

    public void saveValues(Context context) {
        SharedPreferences settings = context.getSharedPreferences(SHARED_PREF_TAG_FONTS_FILE, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = settings.edit();
        editor.putInt(SHARED_PREF_TAG_P, pSize);
        editor.putInt(SHARED_PREF_TAG_H1, headerSize);
        editor.putInt(SHARED_PREF_TAG_H2, subHeaderSize);
        editor.apply();

        Log.d("FONT_MANAGER", "Saved values : " + pSize + " " + headerSize + " " + subHeaderSize);
    }

    private static final String SHARED_PREF_TAG_FONTS_FILE = "sharedpreftagfonts";
    private static final String SHARED_PREF_TAG_P = "sharedpreftagp";
    private static final String SHARED_PREF_TAG_H1 = "sharedpreftagh1";
    private static final String SHARED_PREF_TAG_H2 = "sharedpreftagh2";

    private int pSize;
    private int headerSize;
    private int subHeaderSize;

    public int getpSize() {
        return pSize;
    }

    public void setpSize(int pSize) {
        this.pSize = pSize;
    }

    public int getHeaderSize() {
        return headerSize;
    }

    public void setHeaderSize(int headerSize) {
        this.headerSize = headerSize;
    }

    public int getSubHeaderSize() {
        return subHeaderSize;
    }

    public void setSubHeaderSize(int subHeaderSize) {
        this.subHeaderSize = subHeaderSize;
    }
}
