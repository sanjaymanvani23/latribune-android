package requestlab.com.latribune.utils;

import android.content.Context;
import android.graphics.Typeface;
import android.text.Html;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.AbsoluteSizeSpan;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.ViewTreeObserver;
import android.widget.TextView;

import requestlab.com.latribune.views.fontviews.CustomTypefaceSpan;

/**
 * Created by lchazal on 30/03/16.
 */
public class StringUtils {


    public enum Fonts {
        ROBOTO_REGULAR,
        ROBOTO_SLAB_REGULAR,
        OPEN_SANS_CONDENSED_BOLD
    }

    public static Spannable getSpannableString(Context context, String firstWord, String secondWord, Fonts firstTypeFace, Fonts secondTypeFace) {
        return getSpannableString(context, firstWord, secondWord, firstTypeFace, secondTypeFace, 0, 0);
    }

    public static Spannable getSpannableString(Context context, String firstWord, String secondWord, Fonts firstTypeFace, Fonts secondTypeFace, int firstColor, int secondColor) {
        Spannable spannable = new SpannableString(firstWord + secondWord);

        spannable.setSpan(new CustomTypefaceSpan("sans-serif", getTypefaceFromFont(context, firstTypeFace)), 0, firstWord.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        if (firstColor != 0) spannable.setSpan(new ForegroundColorSpan(firstColor), 0, firstWord.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        spannable.setSpan(new CustomTypefaceSpan("sans-serif", getTypefaceFromFont(context, secondTypeFace)), firstWord.length(), firstWord.length() + secondWord.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        if (secondColor != 0) spannable.setSpan(new ForegroundColorSpan(secondColor), firstWord.length(), firstWord.length() + secondWord.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        return spannable;
    }

    public static Spannable getSpannableString(Context context, String word, Fonts typeFace, int color, int size) {
        Spannable spannable = new SpannableString(word);

        spannable.setSpan(new CustomTypefaceSpan("sans-serif", getTypefaceFromFont(context, typeFace)), 0, word.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        spannable.setSpan(new ForegroundColorSpan(color), 0, word.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        spannable.setSpan(new AbsoluteSizeSpan(size, true), 0, word.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        return spannable;
    }

    private static Typeface getTypefaceFromFont(Context context, Fonts font) {
        switch (font) {
            case ROBOTO_SLAB_REGULAR:
                return Typeface.createFromAsset(context.getAssets(), "RobotoSlab-Regular.ttf");
            case OPEN_SANS_CONDENSED_BOLD:
                return Typeface.createFromAsset(context.getAssets(), "OpenSans-CondBold.ttf");
            case ROBOTO_REGULAR:
            default:
                return Typeface.DEFAULT;
        }
    }

    public static void setTextViewEllipsized(final Context context, final TextView tv, final int maxLine, final String expandText) {

        if (tv.getTag() == null) {
            tv.setTag(tv.getText());
        }
        ViewTreeObserver vto = tv.getViewTreeObserver();
        vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {

            @SuppressWarnings("deprecation")
            @Override
            public void onGlobalLayout() {

                ViewTreeObserver obs = tv.getViewTreeObserver();
                obs.removeOnGlobalLayoutListener(this);
                if (maxLine <= 0) {
                    int lineEndIndex = tv.getLayout().getLineEnd(0);
                    String text = tv.getText().subSequence(0, lineEndIndex - expandText.length() + 1) + " " + expandText;
                    tv.setText(text);
                } else if (tv.getLineCount() >= maxLine) {
                    int lineEndIndex = tv.getLayout().getLineEnd(maxLine - 1);

                    String text = tv.getText().subSequence(0, lineEndIndex - expandText.length() + 1).toString();
                    String cut = text.substring(0, text.lastIndexOf(' '));



                    String finalText = cut + " ... <b>" + expandText + "</b>";
                    tv.setText(Html.fromHtml(finalText));
                }
            }
        });

    }
}
