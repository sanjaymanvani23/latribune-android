package requestlab.com.latribune.utils;


import android.util.Log;

import androidx.annotation.NonNull;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * Created by lchazal on 27/01/16.
 *
 * Helper for date formatting related methods
 */
public class DateConverter {

    public static final String TIMESTAMP_FORMAT = "yyyy-MM-dd HH:mm:ss";

    /**
     * Current date as string, without seconds and separated by spaces
     *
     * @return formatted string
     */
    public static String getCurrentDateAsString() {
        return getCurrentDateStringWithFormat("dd MMMM yyyy hh:mm");
    }

    /**
     * Date as string, without seconds and separated by spaces
     *
     * @return formatted string
     */
    public static String getFullDateAsString(Date date) {
        return getDateStringWithFormat(date, "dd MMMM yyyy hh:mm");
    }

    /**
     * Date as string, without year
     *
     * @return formatted string
     */
    public static String getShortDateAsString(Date date) {
        return getDateStringWithFormat(date, "dd MMMM hh:mm");
    }

    /**
     * Date as string "dd MMMM yyyy"
     *
     * @return formatted string
     */
    public static String getBirthdayAsString(Date date) {
        return getDateStringWithFormat(date, "dd MMMM yyyy");
    }

    /**
     * Current date as string, separated by underscore
     *
     * @return formatted string
     */
    public static String getCurrentDateAsFileName() {
        return getCurrentDateStringWithFormat("yyyy_MM_dd_hh_mm_ss");
    }

    /**
     * Return a timestamp string with specified format
     *
     * @param format formatting pattern
     * @return formatted String
     */
    private static String   getCurrentDateStringWithFormat(String format) {
        Date now = Calendar.getInstance().getTime();
        SimpleDateFormat formatter = new SimpleDateFormat(format, Locale.getDefault());

        return formatter.format(now);
    }

    /**
     * Return a timestamp string with specified format
     *
     * @param format formatting pattern
     * @return formatted String
     */
    private static String   getDateStringWithFormat(Date date, String format) {
        SimpleDateFormat formatter = new SimpleDateFormat(format, Locale.getDefault());

        return formatter.format(date);
    }

    public static boolean isSameDay(@NonNull Date date1, @NonNull Date date2) {
        Calendar cal1 = Calendar.getInstance();
        cal1.setTime(date1);
        Calendar cal2 = Calendar.getInstance();
        cal2.setTime(date2);
        return isSameDay(cal1, cal2);
    }

    public static boolean isSameDay(@NonNull Calendar cal1, @NonNull Calendar cal2) {
        return (cal1.get(Calendar.ERA) == cal2.get(Calendar.ERA) &&
            cal1.get(Calendar.YEAR) == cal2.get(Calendar.YEAR) &&
            cal1.get(Calendar.DAY_OF_YEAR) == cal2.get(Calendar.DAY_OF_YEAR));
    }

    public static Calendar parseTimestamp(String timestamp) {
        SimpleDateFormat dateFormat = new SimpleDateFormat(TIMESTAMP_FORMAT, Locale.getDefault());

        Calendar calendar = Calendar.getInstance();

        try {
            calendar.setTime(dateFormat.parse(timestamp));
        } catch (ParseException e) {
            Log.d("DateHelper", "Failed to convert string " + timestamp);
            return null;
        }
        return calendar;
    }

    public static boolean isSameDate(Date dateOne, Date dateTwo) {
        if (dateOne == null || dateTwo == null) return false;
        return dateOne.compareTo(dateTwo) == 0;
    }
}
