package requestlab.com.latribune.utils;

/**
 * Created by lchazal on 01/04/16.
 */
public class StylesManager {

    public static final int ART_SMALL = 0;
    public static final int ART_SMALL_CARD = 1;
    public static final int ART_LARGE = 3;
    public static final int ART_LARGE_CARD = 4;
    public static final int INFO = 5;
    public static final int STOCK = 6;
    public static final int STOCK_NOTITLE = 2;
    public static final int SUBSCRIBE = 7;
    public static final int AD_NATIVE = 8;
    public static final int AD_BANNER = 9;
    public static final int AD_SQUARE = 10;
    public static final int COMMENT = 11;
    public static final int ART_LIVE = 12;

    private static final int[] STYLE_FRONT = {
            ART_LARGE,
            ART_SMALL_CARD,
            ART_SMALL_CARD,
            ART_SMALL_CARD,
            ART_SMALL_CARD,
            INFO,
            AD_NATIVE,
            ART_SMALL_CARD,
            ART_SMALL_CARD,
            AD_BANNER,
            ART_LARGE_CARD,
            ART_SMALL_CARD,
            ART_SMALL_CARD,
            ART_SMALL_CARD,
            STOCK,
            ART_SMALL_CARD,
            ART_SMALL_CARD,
            ART_SMALL_CARD,
            ART_LARGE_CARD,
            ART_SMALL_CARD,
            ART_SMALL_CARD,
            ART_SMALL_CARD,
            SUBSCRIBE,
            ART_SMALL_CARD,
            ART_SMALL_CARD,
            ART_SMALL_CARD,
            ART_LARGE_CARD,
            ART_SMALL_CARD
    };

    private static final int[] STYLE_STOCK = {
            STOCK_NOTITLE,
            ART_SMALL_CARD,
            ART_SMALL_CARD,
            ART_SMALL_CARD,
            ART_SMALL_CARD,
            ART_SMALL_CARD,
            AD_NATIVE,
            ART_LARGE_CARD,
            ART_SMALL_CARD,
            AD_BANNER,
            ART_SMALL_CARD,
            ART_SMALL_CARD,
            ART_SMALL_CARD,
            ART_LARGE_CARD,
            ART_SMALL_CARD,
            ART_SMALL_CARD,
            ART_SMALL_CARD,
            ART_SMALL_CARD,
            ART_SMALL_CARD,
            ART_LARGE_CARD,
            ART_SMALL_CARD,
            ART_SMALL_CARD,
            ART_SMALL_CARD,
            ART_SMALL_CARD,
            ART_SMALL_CARD,
            ART_LARGE_CARD,
            ART_SMALL_CARD,
            ART_SMALL_CARD,
            ART_SMALL_CARD,
            ART_SMALL_CARD,
            ART_SMALL_CARD,
            ART_LARGE_CARD,
            ART_SMALL_CARD
    };

    private static final int[] STYLE_LIVE = {
            ART_LIVE
    };

    private static final int[] STYLE_PLACEHOLDER = {
            ART_LARGE,
            ART_SMALL_CARD,
            ART_SMALL_CARD,
            ART_SMALL_CARD,
            ART_SMALL_CARD,
            ART_LARGE_CARD,
            AD_NATIVE,
            ART_SMALL_CARD,
            ART_SMALL_CARD,
            AD_BANNER,
            ART_SMALL_CARD,
            ART_LARGE_CARD,
            ART_SMALL_CARD,
            ART_SMALL_CARD,
            ART_SMALL_CARD,
            ART_LARGE_CARD,
            ART_SMALL_CARD,
            ART_SMALL_CARD,
            ART_SMALL_CARD,
            ART_LARGE_CARD,
            ART_SMALL_CARD,
            ART_SMALL_CARD,
            ART_SMALL_CARD,
            ART_LARGE_CARD,
            ART_SMALL_CARD
    };

    public static int[] getStyleArray(Style style) {
        switch (style) {
            case LIVE:
                return STYLE_LIVE;
            case FRONT:
                return STYLE_FRONT;
            case STOCK:
                return STYLE_STOCK;
            case BUSINESS:
            case FINANCES:
            case TECH_MEDIAS:
            case YOUR_FINANCES:
            case OPINIONS:
            case REGIONS:
            case CAREERS:
            case SUBSCRIBERS:
            case HOBBIES:
            case EXTRA:
            default:
                return STYLE_PLACEHOLDER;
        }
    }

    public enum Style {
        FRONT,
        LIVE,
        BUSINESS,
        STOCK,
        FINANCES,
        TECH_MEDIAS,
        YOUR_FINANCES,
        OPINIONS,
        REGIONS,
        CAREERS,
        SUBSCRIBERS,
        HOBBIES,
        EXTRA,
        DEFAULT
    }
}
