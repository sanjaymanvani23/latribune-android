package requestlab.com.latribune.utils;

import android.annotation.SuppressLint;
import android.content.res.ColorStateList;
import android.os.Build;

import android.widget.Button;

import androidx.appcompat.widget.AppCompatButton;
import androidx.core.view.ViewCompat;

/**
 * Created by lchazal on 04/05/16.
 */
public class CompatibilityUtils {

    @SuppressLint("RestrictedApi")
    public static void setButtonTint(Button button, int colorValue) {

        ColorStateList tint = new ColorStateList(new int[][]{new int[0]}, new int[]{colorValue});

        if (Build.VERSION.SDK_INT == Build.VERSION_CODES.LOLLIPOP && button instanceof AppCompatButton) {
            ((AppCompatButton) button).setSupportBackgroundTintList(tint);
        } else {
            ViewCompat.setBackgroundTintList(button, tint);
        }
    }
}
