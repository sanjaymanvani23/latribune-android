package requestlab.com.latribune.utils;

/**
 * Simple 3 values data storage class
 *
 * Created by lchazal on 24/05/16.
 */
public class Triple<A, B, C> {

    private A   first;
    private B   second;
    private C   third;

    public Triple(A first, B second, C third) {
        this.first = first;
        this.second = second;
        this.third = third;
    }

    public A    first() {
        return first;
    }

    public B    second() {
        return second;
    }

    public C    third() {
        return third;
    }
}
