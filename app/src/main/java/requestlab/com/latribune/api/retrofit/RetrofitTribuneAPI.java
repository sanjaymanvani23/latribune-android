package requestlab.com.latribune.api.retrofit;


import android.util.Log;

import androidx.collection.ArrayMap;

import com.crashlytics.android.Crashlytics;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import okhttp3.logging.HttpLoggingInterceptor;
import requestlab.com.latribune.BuildConfig;
import requestlab.com.latribune.api.ITribuneAPI;
import requestlab.com.latribune.api.listeners.APICallbackListener;
import requestlab.com.latribune.data.Article;
import requestlab.com.latribune.data.Category;
import requestlab.com.latribune.data.Comment;
import requestlab.com.latribune.data.Converter.JacksonConverterFactory;
import requestlab.com.latribune.data.Depeche;
import requestlab.com.latribune.data.Hebdo;
import requestlab.com.latribune.data.Infos;
import requestlab.com.latribune.data.Live;
import requestlab.com.latribune.data.Stock;
import requestlab.com.latribune.data.Update;
import requestlab.com.latribune.utils.DateConverter;
import requestlab.com.latribune.utils.Encoder;
import requestlab.com.latribune.utils.ErrorLogger;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Created by lchazal on 01/03/16.
 */
public class RetrofitTribuneAPI implements ITribuneAPI {

    @Override
    public Call<Category> getCategories(APICallbackListener<Category> listener) {
        String time = getTimeKey();

        Call<Category> call = buildService(Services.CategoryAllService.class)
                .load(ITribuneAPI.API_KEY,
                        time,
                        getEncodedKey(time, Services.CATEGORY_ALL_URL));

        call.enqueue(createAPICallback(listener));

        return call;
    }

    @Override
    public Call<Article> getArticle(int id, APICallbackListener<Article> listener) {
        String time = getTimeKey();

        Call<Article> call = buildService(Services.ArticleSingleService.class)
                .load(id,
                        ITribuneAPI.API_KEY,
                        time,
                        getEncodedKey(time, Services.ARTICLE_SINGLE_URL + id));

        call.enqueue(createAPICallback(listener));

        return call;
    }

    @Override
    public Call<Depeche> getDepeche(int id, APICallbackListener<Depeche> listener) {
        String time = getTimeKey();

        Call<Depeche> call = buildService(Services.DepecheService.class)
                .load(id,
                        ITribuneAPI.API_KEY,
                        time,
                        getEncodedKey(time, Services.DEPECHE_SINGLE_URL + id));

        call.enqueue(createAPICallback(listener));

        return call;
    }

    @Override
    public Call<List<Article>> getCategory(int categoryId, APICallbackListener<List<Article>> listener) {
        String time = getTimeKey();

        Call<List<Article>> call = buildService(Services.CategorySingleService.class)
                .load(categoryId,
                        ITribuneAPI.API_KEY,
                        time,
                        getEncodedKey(time, "1" + Services.CATEGORY_SINGLE_URL + categoryId + "/homepage"),
                        "1");

        call.enqueue(createAPICallback(listener));

        return call;
    }

    @Override
    public Call<Update> getLastCategoryUpdate(int categoryId, APICallbackListener<Update> listener) {
        String time = getTimeKey();

        Call<Update> call = buildService(Services.UpdateCategoryService.class)
                .load(
                        categoryId,
                        ITribuneAPI.API_KEY,
                        time,
                        getEncodedKey(time, Services.CATEGORY_SINGLE_URL + categoryId + "/homepage/lastupdate"));

        call.enqueue(createAPICallback(listener));

        return call;
    }

    @Override
    public Call<Update> getLastLiveUpdate(APICallbackListener<Update> listener) {
        String time = getTimeKey();


        Call<Update> call = buildService(Services.UpdateLiveService.class)
                .load(
                        ITribuneAPI.API_KEY,
                        time,
                        getEncodedKey(time, Services.LIVE_ALL_URL_UPDATE + "/lastupdate"));

        call.enqueue(createAPICallback(listener));

        return call;
    }

    @Override
    public Call<List<Live>> getLive(APICallbackListener<List<Live>> listener) {
        String time = getTimeKey();

        Log.e("tribune",ITribuneAPI.API_KEY);
        Log.e("tribune",time);
        Log.e("tribune",getEncodedKey(time, Services.LIVE_ALL_URL));


        Call<List<Live>> call = buildService(Services.LiveAllService.class)
                .load(ITribuneAPI.API_KEY,
                        time,
                        getEncodedKey(time, Services.LIVE_ALL_URL));

        call.enqueue(createAPICallback(listener));

        return call;
    }

    @Override
    public Call<List<Stock>> getStock(APICallbackListener<List<Stock>> listener) {
        String time = getTimeKey();

        Call<List<Stock>> call = buildService(Services.StockAllService.class)
                .load(ITribuneAPI.API_KEY,
                        time,
                        getEncodedKey(time, Services.STOCK_ALL_URL));

        call.enqueue(createAPICallback(listener));

        return call;
    }

    @Override
    public Call<List<Article>> getMostRead(APICallbackListener<List<Article>> listener) {
        String time = getTimeKey();

        Call<List<Article>> call = buildService(Services.ArticleMostReadService.class)
                .load(ITribuneAPI.API_KEY,
                        time,
                        getEncodedKey(time, Services.ARTICLE_TOP_URL));

        call.enqueue(createAPICallback(listener));

        return call;
    }

    @Override
    public Call<List<Article>> getRendezVous(APICallbackListener<List<Article>> listener) {
        String time = getTimeKey();

        Call<List<Article>> call = buildService(Services.RendezVousService.class)
                .load(ITribuneAPI.API_KEY,
                        time,
                        getEncodedKey(time, Services.SPECIAL_ALL_URL));

        call.enqueue(createAPICallback(listener));

        return call;
    }

    @Override
    public Call<Hebdo> getDailyEdition(APICallbackListener<Hebdo> listener) {
        String time = getTimeKey();

        Call<Hebdo> call = buildService(Services.EditionDailyService.class)
                .load(ITribuneAPI.API_KEY,
                        time,
                        getEncodedKey(time, Services.EDITION_DAILY_URL));

        call.enqueue(createAPICallback(listener));

        return call;
    }

    @Override
    public Call<Hebdo> getWeeklyEdition(APICallbackListener<Hebdo> listener) {
        String time = getTimeKey();

        Call<Hebdo> call = buildService(Services.EditionWeeklyService.class)
                .load(ITribuneAPI.API_KEY,
                        time,
                        getEncodedKey(time, Services.EDITION_WEEKLY_URL));

        call.enqueue(createAPICallback(listener));

        return call;
    }

    @Override
    public Call<Infos> getInfos(APICallbackListener<Infos> listener) {
        String time = getTimeKey();

        Call<Infos> call = buildService(Services.InfosService.class)
                .load(ITribuneAPI.API_KEY,
                        time,
                        getEncodedKey(time, Services.INFOS_URL));

        call.enqueue(createAPICallback(listener));

        return call;
    }

    @Override
    public Call<List<Comment>> getCommentList(int articleId, APICallbackListener<List<Comment>> listener) {
        String time = getTimeKey();

        Call<List<Comment>> call = buildService(Services.CommentListService.class)
                .load(articleId,
                        ITribuneAPI.API_KEY,
                        time,
                        getEncodedKey(time, Services.ARTICLE_SINGLE_URL + articleId + "/comments")
                        );

        call.enqueue(createAPICallback(listener));

        return call;
    }

    @Override
    public Call<Void> sendComment(APICallbackListener<Void> listener, int articleId, String content, String email, String pseudo) {
        String time = getTimeKey();

        Map<String, Object> jsonParams = new ArrayMap<>();

        jsonParams.put("api_key", ITribuneAPI.API_KEY);
        jsonParams.put("api_time", time);
        jsonParams.put("api_signature", getEncodedKey(time, Services.ARTICLE_SINGLE_URL + articleId + "/comment", content + email + pseudo));
        jsonParams.put("content", content);
        jsonParams.put("email", email);
        jsonParams.put("pseudo", pseudo);

        RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"),(new JSONObject(jsonParams)).toString());

        Call<Void> call = buildService(Services.PostCommentService.class)
                .load(articleId,
                        body
                );
        call.enqueue(createAPICallback(listener));

        return call;
    }

    private String getTimeKey() {
        return String.valueOf(System.currentTimeMillis() / 1000);
    }

    private String getEncodedKey(String timeKey, String url) {
        return Encoder.sha256(ITribuneAPI.API_KEY + ITribuneAPI.API_SECRET + timeKey + url);
    }

    private String getEncodedKey(String timeKey, String url, String extra) {
        return Encoder.sha256(ITribuneAPI.API_KEY + ITribuneAPI.API_SECRET + timeKey + extra + url);
    }

    /**
     * Create a service from Services interfaces
     *
     * @param serviceClass interface class defining the  service
     * @return created Service
     */
    private <S> S buildService(Class<S> serviceClass, String endPoint) {

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
//        interceptor.setLevel(HttpLoggingInterceptor.Level.HEADERS);
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();

        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        mapper.configure(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);
        mapper.configure(DeserializationFeature.ACCEPT_EMPTY_STRING_AS_NULL_OBJECT, true);
        mapper.configure(DeserializationFeature.ACCEPT_EMPTY_ARRAY_AS_NULL_OBJECT, true);

        SimpleDateFormat dateFormat = new SimpleDateFormat(DateConverter.TIMESTAMP_FORMAT, Locale.getDefault());
        mapper.setDateFormat(dateFormat);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(endPoint)
                .client(client)
//                .addConverterFactory(TribuneRealmCache.getRealmGSONConverter())
                .addConverterFactory(JacksonConverterFactory.create(mapper))
                .build();

        return retrofit.create(serviceClass);
    }

    private <S> S buildService(Class<S> serviceClass) {
        return buildService(serviceClass, ITribuneAPI.API_SCHEME + BuildConfig.BASE_URL);
    }

    /**
     * Create a Retrofit callback from provided listener
     *
     * @param listener callback listener
     * @return created callback
     */
    private <S> Callback<S> createAPICallback(final APICallbackListener<S> listener) {

        return new Callback<S>() {
            @Override
            public void onResponse(Call<S> call, Response<S> response) {
                if (response.isSuccessful()) {
                    Log.e("RETROFIT", "Retrofit API response success");
                    listener.onResponseSuccess(response.body());
                } else {
                    Log.e("RETROFIT", "Retrofit API response failure");
                    ErrorLogger.logNetworkErrorToCrashlytics(response);
                    listener.onResponseFailure(response.message());
                }
            }

            @Override
            public void onFailure(Call<S> call, Throwable t) {
                Log.e("RETROFIT", "Retrofit API call failed : " + t.getMessage());
                Crashlytics.logException(t);
                listener.onFailure(t.getMessage());
            }
        };
    }


    public class    CrashlyticsNetworkReportException extends Exception {
        public CrashlyticsNetworkReportException(String message) {
            super(message);
        }
    }
}
