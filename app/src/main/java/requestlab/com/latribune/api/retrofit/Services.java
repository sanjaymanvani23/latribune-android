package requestlab.com.latribune.api.retrofit;

import java.util.List;

import okhttp3.RequestBody;
import requestlab.com.latribune.data.Article;
import requestlab.com.latribune.data.Category;
import requestlab.com.latribune.data.Comment;
import requestlab.com.latribune.data.Depeche;
import requestlab.com.latribune.data.Hebdo;
import requestlab.com.latribune.data.Infos;
import requestlab.com.latribune.data.Live;
import requestlab.com.latribune.data.Stock;
import requestlab.com.latribune.data.Update;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by lchazal on 02/03/16.
 *
 * Retrofit2 services
 */
public class Services {

    static final String CATEGORY_ALL_URL =      "/categories/tree";
    static final String CATEGORY_SINGLE_URL =   "/category/";
    static final String ARTICLE_SINGLE_URL =    "/article/";
    static final String DEPECHE_SINGLE_URL =    "/depeche/";
    static final String ARTICLE_TOP_URL =       "/article/mostread";
    static final String LIVE_ALL_URL =          "/live/version2";
    static final String LIVE_ALL_URL_UPDATE =     "/live";
    static final String STOCK_ALL_URL =         "/bourse/bar";
    static final String SPECIAL_ALL_URL =       "/rendezvous";
    static final String EDITION_DAILY_URL =     "/edition/daily";
    static final String EDITION_WEEKLY_URL =    "/edition/weekly";
    static final String INFOS_URL =             "/relevant_information";

    public interface CategoryAllService {
        @GET(CATEGORY_ALL_URL)
        Call<Category> load(
                @Query("api_key") String apiKey,
                @Query("api_time") String apiTime,
                @Query("api_signature") String apiSignature);
    }

    public interface CategorySingleService {
        @GET(CATEGORY_SINGLE_URL + "{categoryId}" + "/homepage")
        Call<List<Article>> load(
                @Path("categoryId") int categoryId,
                @Query("api_key") String apiKey,
                @Query("api_time") String apiTime,
                @Query("api_signature") String apiSignature,
                @Query("diaporama") String diaporama);
    }

    public interface ArticleSingleService {
        @GET(ARTICLE_SINGLE_URL + "{articleid}")
        Call<Article> load(
                @Path("articleid") int articleId,
                @Query("api_key") String apiKey,
                @Query("api_time") String apiTime,
                @Query("api_signature") String apiSignature);
    }

    public interface DepecheService {
        @GET(DEPECHE_SINGLE_URL + "{articleid}")
        Call<Depeche> load(
                @Path("articleid") int articleId,
                @Query("api_key") String apiKey,
                @Query("api_time") String apiTime,
                @Query("api_signature") String apiSignature);
    }

    public interface ArticleMostReadService {
        @GET(ARTICLE_TOP_URL)
        Call<List<Article>> load(
                @Query("api_key") String apiKey,
                @Query("api_time") String apiTime,
                @Query("api_signature") String apiSignature);
    }

    public interface LiveAllService {
        @GET(LIVE_ALL_URL)
        Call<List<Live>> load(
                @Query("api_key") String apiKey,
                @Query("api_time") String apiTime,
                @Query("api_signature") String apiSignature);
    }

    public interface StockAllService {
        @GET(STOCK_ALL_URL)
        Call<List<Stock>> load(
                @Query("api_key") String apiKey,
                @Query("api_time") String apiTime,
                @Query("api_signature") String apiSignature);
    }

    public interface RendezVousService {
        @GET(SPECIAL_ALL_URL)
        Call<List<Article>> load(
                @Query("api_key") String apiKey,
                @Query("api_time") String apiTime,
                @Query("api_signature") String apiSignature);
    }

    public interface EditionDailyService {
        @GET(EDITION_DAILY_URL)
        Call<Hebdo> load(
                @Query("api_key") String apiKey,
                @Query("api_time") String apiTime,
                @Query("api_signature") String apiSignature);
    }

    public interface EditionWeeklyService {
        @GET(EDITION_WEEKLY_URL)
        Call<Hebdo> load(
                @Query("api_key") String apiKey,
                @Query("api_time") String apiTime,
                @Query("api_signature") String apiSignature);
    }

    public interface InfosService {
        @GET(INFOS_URL)
        Call<Infos> load(
                @Query("api_key") String apiKey,
                @Query("api_time") String apiTime,
                @Query("api_signature") String apiSignature);
    }

    public interface UpdateCategoryService {
        @GET(CATEGORY_SINGLE_URL + "{categoryId}" + "/homepage/lastupdate")
        Call<Update> load(
                @Path("categoryId") int categoryId,
                @Query("api_key") String apiKey,
                @Query("api_time") String apiTime,
                @Query("api_signature") String apiSignature);
    }

    public interface UpdateLiveService {
        @GET(LIVE_ALL_URL_UPDATE + "/lastupdate")
        Call<Update> load(
                @Query("api_key") String apiKey,
                @Query("api_time") String apiTime,
                @Query("api_signature") String apiSignature);
    }

    public interface PostCommentService {
        @POST(ARTICLE_SINGLE_URL + "{articleId}" + "/comment")
        Call<Void> load(
                @Path("articleId") int articleId,
                @Body RequestBody body);
    }

    public interface CommentListService {
        @GET(ARTICLE_SINGLE_URL + "{articleId}" + "/comments")
        Call<List<Comment>> load(
                @Path("articleId") int articleId,
                @Query("api_key") String apiKey,
                @Query("api_time") String apiTime,
                @Query("api_signature") String apiSignature);
    }
}
