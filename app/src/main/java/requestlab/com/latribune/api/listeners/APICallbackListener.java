package requestlab.com.latribune.api.listeners;

/**
 * Created by lchazal on 01/03/16.
 */
public interface APICallbackListener<T> {

    void onResponseSuccess(T response);

    void onResponseFailure(String message);

    void onFailure(String message);
}
