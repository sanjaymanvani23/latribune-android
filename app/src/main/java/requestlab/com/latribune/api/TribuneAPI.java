package requestlab.com.latribune.api;

/**
 * Created by lchazal on 01/03/16.
 *
 * API Handler, relay calls to current API implementation, created by TribuneAPIFactory
 */
public class TribuneAPI {
    private static ITribuneAPI ourInstance = TribuneAPIFactory.getAPIImplementation();

    public static ITribuneAPI getInstance() {
        return ourInstance;
    }
}
