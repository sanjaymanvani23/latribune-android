package requestlab.com.latribune.api;

import requestlab.com.latribune.api.retrofit.RetrofitTribuneAPI;

/**
 * Created by lchazal on 01/03/16.
 *
 * Build an instance of the current Officin API implementation.
 */
public class TribuneAPIFactory {

    public static ITribuneAPI   getAPIImplementation() {
        return new RetrofitTribuneAPI();
    }
}
