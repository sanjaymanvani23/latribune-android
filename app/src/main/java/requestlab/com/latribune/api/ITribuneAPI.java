package requestlab.com.latribune.api;

import java.util.List;

import requestlab.com.latribune.api.listeners.APICallbackListener;
import requestlab.com.latribune.data.Article;
import requestlab.com.latribune.data.Category;
import requestlab.com.latribune.data.Comment;
import requestlab.com.latribune.data.Depeche;
import requestlab.com.latribune.data.Hebdo;
import requestlab.com.latribune.data.Infos;
import requestlab.com.latribune.data.Live;
import requestlab.com.latribune.data.Stock;
import requestlab.com.latribune.data.Update;
import retrofit2.Call;

/**
 * Created by lchazal on 01/03/16.
 *
 * API method calls interface
 */
public interface ITribuneAPI {

    String API_SCHEME = "https://";
    String API_KEY = "application_live";
    String API_SECRET = "d70c9dbb3af800622287f0458d22a68f";

    Call<Category>  getCategories(APICallbackListener<Category> listener);
    Call<Article>   getArticle(int id, APICallbackListener<Article> listener);
    Call<Depeche>   getDepeche(int id, APICallbackListener<Depeche> listener);

    Call<List<Article>> getCategory(int categoryId, APICallbackListener<List<Article>> listener);
    Call<List<Live>>    getLive(APICallbackListener<List<Live>> listener);
    Call<List<Stock>>   getStock(APICallbackListener<List<Stock>> listener);
    Call<List<Article>> getMostRead(APICallbackListener<List<Article>> listener);
    Call<List<Article>> getRendezVous(APICallbackListener<List<Article>> listener);
    Call<Hebdo>         getDailyEdition(APICallbackListener<Hebdo> listener);
    Call<Hebdo>         getWeeklyEdition(APICallbackListener<Hebdo> listener);

    Call<Infos> getInfos(APICallbackListener<Infos> listener);

    Call<Update>    getLastCategoryUpdate(int categoryId, APICallbackListener<Update> listener);
    Call<Update>    getLastLiveUpdate(APICallbackListener<Update> listener);

    Call<List<Comment>> getCommentList(int articleId, APICallbackListener<List<Comment>> listener);

    Call<Void>      sendComment(APICallbackListener<Void> listener, int articleId, String content, String email, String pseudo);
}
