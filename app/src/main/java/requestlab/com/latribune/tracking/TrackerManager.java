package requestlab.com.latribune.tracking;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;

import com.ad4screen.sdk.A4S;
import com.adjust.sdk.Adjust;
import com.adjust.sdk.AdjustConfig;
import com.atinternet.tracker.SetConfigCallback;
import com.atinternet.tracker.Tracker;

import com.weborama.tracking.WeboramaTrackingPlugin;

import java.util.HashMap;

import requestlab.com.latribune.BuildConfig;
import requestlab.com.latribune.C.Constants;
import requestlab.com.latribune.R;
import requestlab.com.latribune.utils.Triple;

/**
 * Created by lchazal on 21/04/16.
 */
public class TrackerManager {
    private static TrackerManager ourInstance = new TrackerManager();

    public static TrackerManager getInstance() {
        return ourInstance;
    }

    private TrackerManager() {

    }

    public static final String  TRACKING_CATEGORY_STOCK_MODEL       = "BOURSE";
    public static final String  TRACKING_CATEGORY_ECO_MODEL         = "ÉCONOMIE";
    public static final String  TRACKING_CATEGORY_FINANCES_MODEL    = "VOS FINANCES";
    public static final String  TRACKING_CATEGORY_CAREERS_MODEL     = "CARRIÈRES";
    public static final String  TRACKING_CATEGORY_STOCK             = "RUBRIQUE BOURSE";
    public static final String  TRACKING_CATEGORY_ECO               = "ECO";
    public static final String  TRACKING_CATEGORY_FINANCES          = "FINANCES PERSO";
    public static final String  TRACKING_CATEGORY_CAREERS           = "CARRIERE";

    private Tracker ATITracker; //Xiti
    private WeboramaTrackingPlugin audiTracker;



    public void init(Context context) {
        initAdjust(context);
        initATInternet(context);
        initAudipresse(context);
//        initGAnalytics(context);
    }

    private void initAdjust(Context context) {
        String environment = BuildConfig.DEBUG ? AdjustConfig.ENVIRONMENT_SANDBOX : AdjustConfig.ENVIRONMENT_PRODUCTION;
        AdjustConfig config = new AdjustConfig(context, BuildConfig.ADJUST_APP_TOKEN, environment);
        Adjust.onCreate(config);
    }

    private void initATInternet(Context context) {
        ATITracker = new Tracker(context);

        HashMap config = new HashMap<String, Object>() {{
            put("site", 529603);
            put("log", "logc202");
            put("identifier", "logs1202");
        }};
        ATITracker.setConfig(config, false, new SetConfigCallback() {
            @Override
            public void setConfigEnd() {
                Log.d("TRACKING", "Configuration is now set");
            }
        });
    }

    private void initAudipresse(Context context) {
        audiTracker = new WeboramaTrackingPlugin(context,
                1151,
                201,
                "www.etude-premium.fr");
        audiTracker.customPath = "/proxy/track.php";
        audiTracker.trackerPing();
    }

    private void initGAnalytics(Context context) {
//        GoogleAnalytics analytics = GoogleAnalytics.getInstance(context);
//        googleTracker = analytics.newTracker(R.xml.global_tracker);
        //TODO update with TRACKER ID and stuff
    }

    // MENU BURGER
    public void tagScreen(final String chapter) {
        String chapterFormated = chapter.replace("& ", "");
        Log.d("TRACKING", chapterFormated.toUpperCase());
        ATITracker.Screens().add(chapterFormated.toUpperCase()).sendView();
        sendGoogleAnalyticsScreen(chapterFormated.toUpperCase());
    }

    //COMMENTAIRES:LISTE
    //COMMENTAIRES:POST
    //5 INFOS A RETENIR:Edito
    //5 INFOS A RETENIR:Accueil
    //PARAMETRES:Catégories
    //PARAMETRES:Mentions légales
    //PARAMETRES:Suggestions
    //PARAMETRES:Problème technique
    //PARAMETRES:Texte
    //PARAMETRES:Tutoriel
    //PARAMETRES:Accueil
    //[Page title]:Accueil
    public void tagScreen(final String chapter, final String title) {
        String chapterFormated = chapter.replace("& ", "");
        Log.d("TRACKING", getConvertedChapter(chapterFormated.toUpperCase()) + ":" + title);
        ATITracker.Screens().add(title, getConvertedChapter(chapterFormated.toUpperCase())).sendView();
        sendGoogleAnalyticsScreen(chapterFormated.toUpperCase() + " " + title);
    }

    //[CATEGORY]:ARTICLES:[titre]
    //5 INFOS A RETENIR:ARTICLES:[titre]
    //LIVE:ARTICLES:[titre]
    //LES PLUS LUS:ARTICLES:[titre]
    //RENDZ VOUS DU JOUR:ARTICLES:[titre]
    public void tagScreen(final String chapter, final String subChapter, final String title) {
        String chapterFormated = chapter.replace("& ", "");
        Log.d("TRACKING", getConvertedChapter(chapterFormated.toUpperCase()) + ":" + subChapter.toUpperCase() + ":" + title);
        ATITracker.Screens().add(title, getConvertedChapter(chapterFormated.toUpperCase()), subChapter.toUpperCase()).sendView();
        sendGoogleAnalyticsScreen(chapterFormated.toUpperCase() + " " + subChapter.toUpperCase() + " " + title);
    }

    private void sendGoogleAnalyticsScreen(String name) {
       // if (googleTracker == null) return;
//        googleTracker.setScreenName(name);
//        googleTracker.send(new HitBuilders.ScreenViewBuilder().build());
    }

    public static void sendAccengageThematics(A4S a4S, boolean[] values) {
        Bundle bundle = new Bundle();
        for (int i = 0; i < Constants.NOTIFICATION_KEYS.size(); i++) {
            Triple<String, String, Integer> t = Constants.NOTIFICATION_KEYS.get(i);
            bundle.putString(t.first(), values != null && values[i] ? "y" : "n");
        }
        a4S.updateDeviceInfo(bundle);
        Log.d("ACCENGAGE_THEMATICS", "Sent thematics preferences !");
    }

    private static final class AdjustLifecycleCallbacks implements Application.ActivityLifecycleCallbacks {

        @Override
        public void onActivityCreated(Activity activity, Bundle savedInstanceState) {

        }

        @Override
        public void onActivityStarted(Activity activity) {

        }

        @Override
        public void onActivityResumed(Activity activity) {
            Adjust.onResume();
        }

        @Override
        public void onActivityPaused(Activity activity) {
            Adjust.onPause();
        }

        @Override
        public void onActivityStopped(Activity activity) {

        }

        @Override
        public void onActivitySaveInstanceState(Activity activity, Bundle outState) {

        }

        @Override
        public void onActivityDestroyed(Activity activity) {

        }
    }

    public static AdjustLifecycleCallbacks  getAdjustLifecycleCallback() {
        return new AdjustLifecycleCallbacks();
    }

    private static String   getConvertedChapter(String chapter) {
        switch (chapter) {
            case TRACKING_CATEGORY_STOCK_MODEL:
                return TRACKING_CATEGORY_STOCK;
            case TRACKING_CATEGORY_FINANCES_MODEL:
                return TRACKING_CATEGORY_FINANCES;
            case TRACKING_CATEGORY_ECO_MODEL:
                return TRACKING_CATEGORY_ECO;
            case TRACKING_CATEGORY_CAREERS_MODEL:
                return TRACKING_CATEGORY_CAREERS;
            default:
                return chapter;
        }
    }
}
