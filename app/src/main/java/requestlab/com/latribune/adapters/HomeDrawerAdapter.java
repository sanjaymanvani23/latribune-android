package requestlab.com.latribune.adapters;

import android.app.Activity;
import android.content.Context;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import requestlab.com.latribune.BuildConfig;
import requestlab.com.latribune.R;
import requestlab.com.latribune.cache.TribuneCache;
import requestlab.com.latribune.data.Hebdo;
import requestlab.com.latribune.utils.ImageManager;
import requestlab.com.latribune.utils.NetworkUtils;

/**
 * Created by lchazal on 15/03/16.
 */
public class HomeDrawerAdapter extends ArrayAdapter<HomeDrawerAdapter.MenuElement> {

    public static final String TAG = "NAV_DRAWER";

    private final Context context;
    private List<MenuElement>  items;
    private OnDrawerButtonClickedListener listener;

    private ImageView hebdoImage;

    public HomeDrawerAdapter(Context context, List<MenuElement> list) {
        super(context, R.layout.nav_content_home, list);

        this.context = context;
        items = list;
    }

    public void addMenuHeader() {
        items.add(new MenuElement("", false, true, false));
    }

    public void addMenuElement(String content, int id) {
        addMenuElement(content, 0, id);
    }

    public void addMenuElement(String content, int id, boolean token) {
        MenuElement element = new MenuElement(content, 0, id);
        element.isTitle = false;
        element.isClickable = true;
        element.hasSeparator = true;
        items.add(element);
    }

    public void addMenuElement(String content, int iconId, int id) {
        items.add(new MenuElement(content, iconId, id));
    }

    public void addMenuSection(String title, String... elements) {
        items.add(new MenuElement(title, true, false, false));
        if (elements == null) return;
        for (String element : elements) {
            if (!element.equals(context.getString(R.string.category_live)) && !element.equals(context.getString(R.string.category_front))) {
                items.add(new MenuElement(element, false, true, true));

            }
        }
    }

    private View.OnClickListener createSubscribeListener() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NetworkUtils.openUrlInCustomTab((Activity) context, Uri.parse(BuildConfig.SUBSCRIBE_URL));
            }
        };
    }

    private View.OnClickListener createSubscriberListener() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                NetworkUtils.openUrlInCustomTab((Activity) context, Uri.parse(BuildConfig.SUBSCRIBERS_URL));
                NetworkUtils.openAppOrUrl(context, BuildConfig.SUBSCRIBERS_APP_PACKAGE);
            }
        };
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View rowView;

        MenuElement element = items.get(position);

//        if (element.hasSeparator) rowView = inflater.inflate(element.isTitle ? R.layout.nav_title_separator_home : R.layout.nav_content_separator_home, parent, false);
//        else {
            rowView = inflater.inflate(element.isTitle ? R.layout.nav_title_home : R.layout.nav_content_home, parent, false);
            rowView.findViewById(R.id.separator).setVisibility(element.hasSeparator ? View.VISIBLE : View.GONE);

            ((TextView) rowView.findViewById(R.id.text)).setText(element.content);
            if (element.iconId != 0 && element.isTitle) {
                ImageView icon = (ImageView) rowView.findViewById(R.id.icon);
                icon.setImageResource(element.iconId);
                icon.setVisibility(View.VISIBLE);
            }
//        }
        rowView.setId(element.id);

        return rowView;
    }

    public void refreshHebdoImage() {
        if (hebdoImage == null) return;

        Hebdo hebdo = TribuneCache.getInstance().getHebdo();
        if (hebdo != null && hebdo.getCoverUrl() != null && !hebdo.getCoverUrl().isEmpty()) {
            ImageManager.getInstance().fitImageIntoView(hebdo.getCoverUrl(), hebdoImage);
        }
    }

    @Override
    public boolean isEnabled(int position) {
        return items.get(position).isClickable;
    }

    public class MenuElement {
        String  content;
        boolean isTitle;
        boolean isClickable;
        boolean hasSeparator;
        int     iconId = 0;
        int     id = -1;

        public MenuElement(String content, int iconId, int id) {
            this(content, true, true, true);
            this.iconId = iconId;
            this.id = id;
        }

        public MenuElement(String content, boolean isTitle, boolean isClickable, boolean hasSeparator) {
            this.content = content;
            this.isTitle = isTitle;
            this.isClickable = isClickable;
            this.hasSeparator = hasSeparator;
        }

    }


    public interface OnDrawerButtonClickedListener {

    }
}