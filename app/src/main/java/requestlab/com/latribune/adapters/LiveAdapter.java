package requestlab.com.latribune.adapters;

import android.content.Context;
import android.content.Intent;

import android.text.Spannable;
import android.util.Log;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.SortedList;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;

import requestlab.com.latribune.C.Constants;
import requestlab.com.latribune.R;
import requestlab.com.latribune.activities.BetterArticleDetailsActivity;
import requestlab.com.latribune.activities.LiveDetailsActivity;
import requestlab.com.latribune.cache.TribuneCache;
import requestlab.com.latribune.data.Live;
import requestlab.com.latribune.utils.DateConverter;
import requestlab.com.latribune.utils.ImageManager;
import requestlab.com.latribune.utils.StringUtils;
import requestlab.com.latribune.views.fontviews.FontTextView;

/**
 * Created by lchazal on 01/04/16.
 */
public class LiveAdapter extends RecyclerView.Adapter<LiveAdapter.LiveViewHolder> {

    private Context         context;
    private SortedList<Live> sortedList;

    private LayoutInflater  inflater;

    private Calendar calendar;

    private static Spannable reutersString;

    public LiveAdapter(Context context) {
        this.context = context;

        calendar = GregorianCalendar.getInstance();

        inflater = LayoutInflater.from(context);
        sortedList = new SortedList<>(Live.class, new SortedList.Callback<Live>() {
            @Override
            public int compare(Live o1, Live o2) {
                return o2.getDate().compareTo(o1.getDate());
            }

            @Override
            public void onInserted(int position, int count) {
                notifyDataSetChanged();
            }

            @Override
            public void onRemoved(int position, int count) {
                notifyItemRangeRemoved(position, count);
            }

            @Override
            public void onMoved(int fromPosition, int toPosition) {
                notifyItemMoved(fromPosition, toPosition);
            }

            @Override
            public void onChanged(int position, int count) {
                notifyItemRangeChanged(position, count);
            }

            @Override
            public boolean areContentsTheSame(Live oldItem, Live newItem) {
                return oldItem.getTitle().equals(newItem.getTitle());
            }

            @Override
            public boolean areItemsTheSame(Live item1, Live item2) {
                return item1.getId() == item2.getId();
            }
        });
    }

    public void addItem(Live item) {
        sortedList.add(item);
        notifyItemInserted(sortedList.size() - 1);
    }

    public void addItemList(List<Live> items) {
        sortedList.beginBatchedUpdates();
        sortedList.addAll(items);
        sortedList.endBatchedUpdates();
    }

    @Override
    public LiveViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case 0:
                return new LiveViewHolder(inflater.inflate(R.layout.vh_article_live, parent, false));
            case 1:
            default:
                return new LiveViewHolder(inflater.inflate(R.layout.vh_article_live_small, parent, false));
        }
    }

    @Override
    public void onBindViewHolder(LiveViewHolder holder, int position) {
        bindArticleViewHolder(holder, position);
    }

    @Override
    public int getItemCount() {
        return sortedList.size();
    }

    @Override
    public int getItemViewType(int position) {
        if (sortedList.get(position).getType() == null) return -1;
        switch (sortedList.get(position).getType()) {
            case "article":
                return 0;
            case "depeche":
            default:
                return 1;
        }
    }

    private void bindArticleViewHolder(LiveViewHolder holder, int position) {
        final Live live = sortedList.get(position);
        String subHead = live.getSubHead();

        Log.d("LIVE_ADAPTER", "Binding live vh");

        if (holder.view != null) holder.view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("ARTCL_DETAILS", "Clicky id " + live.getId());

                Intent intent = new Intent(LiveAdapter.this.context, live.getType().equals("depeche") ? LiveDetailsActivity.class : BetterArticleDetailsActivity.class);
                intent.putExtra(BetterArticleDetailsActivity.ARTICLE_DETAIL_ID, live.getId());

                //Set read status here ?

                LiveAdapter.this.context.startActivity(intent);
            }
        });

        boolean isSmall = holder.image == null;

        calendar.setTime(live.getDate());
        if (holder.date != null)
            holder.date.setText(String.format(context.getString(R.string.time_format_live_time),
                    calendar.get(Calendar.HOUR_OF_DAY),
                    calendar.get(Calendar.MINUTE)));

        if (position == 0) { // First element, No date header, top margin
            Log.d("LIVE_ADAPTER", "Binding live at position 0");


            holder.headerLayout.setVisibility(View.GONE);
            holder.topPadding.setVisibility(View.VISIBLE);
            holder.top.setVisibility(View.VISIBLE);
        } else if (!DateConverter.isSameDay(sortedList.get(position - 1).getDate(), live.getDate())) { // First element od day, date header, top margin
            Log.d("LIVE_ADAPTER", "Binding first element of day (pos = " + position + ")");


            holder.headerLayout.setVisibility(View.VISIBLE);
            holder.topPadding.setVisibility(View.VISIBLE);
            holder.header.setText(String.format(context.getString(R.string.time_format_live_head),
                    calendar.getDisplayName(Calendar.DAY_OF_WEEK, Calendar.LONG, Locale.getDefault()),
                    calendar.get(Calendar.DAY_OF_MONTH),
                    calendar.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.getDefault())));
            holder.top.setVisibility(View.GONE);
        } else {
            Log.d("LIVE_ADAPTER", "Binding other (pos = " + position + ")");
            holder.headerLayout.setVisibility(View.GONE);
            holder.topPadding.setVisibility(View.GONE);
            holder.top.setVisibility(View.GONE);
        }

        if (position == sortedList.size() - 1 || !DateConverter.isSameDay(sortedList.get(position + 1).getDate(), live.getDate())) {
            holder.shadow.setVisibility(View.VISIBLE); // Last element of day, bottom shadow
        } else {
            holder.shadow.setVisibility(View.GONE);
        }

        if (isSmall) {
            if (reutersString == null) reutersString = StringUtils.getSpannableString(context,
                    "REUTERS",
                    StringUtils.Fonts.ROBOTO_REGULAR,
                    ContextCompat.getColor(context, R.color.live_time_text),
                    12);
            holder.text.setText(live.getTitle() + "  ");
            holder.text.append(reutersString);
        } else {
            if (holder.date != null)
                holder.date.setText(String.format(context.getString(R.string.time_format_live_time),
                        calendar.get(Calendar.HOUR_OF_DAY),
                        calendar.get(Calendar.MINUTE)));
            if (holder.title != null) {
                holder.title.setVisibility(subHead.isEmpty() ? View.GONE : View.VISIBLE);

                Pair<Integer, Integer> headerInfo = Constants.ARTICLE_SUBHEAD.get(subHead);

                String  subHeader = headerInfo == null ? subHead : context.getString(headerInfo.first);
                int     subHeaderColorId = headerInfo == null ? R.color.subhead_blue : headerInfo.second;
                holder.title.setText(subHeader);
                holder.title.setTextColor(ContextCompat.getColor(context, subHeaderColorId));

            }
            if (holder.text != null) holder.text.setText(sortedList.get(position).getTitle());

            if (holder.overlay != null) {
                if (subHead == null) {
                    holder.overlay.setVisibility(View.GONE);
                    return;
                }
                switch (subHead) {
                    case "Vidéo":
                    case "video":
                        holder.overlay.setVisibility(View.VISIBLE);
                        holder.overlay.setImageResource(R.drawable.icon_video);
                        break;
                    case "Diaporama":
                    case "diaporama":
                        holder.overlay.setVisibility(View.VISIBLE);
                        holder.overlay.setImageResource(R.drawable.icon_diaporama);
                        break;
                    default:
                        holder.overlay.setVisibility(View.GONE);
                        break;
                }
            }

            if (live.getMedia() == null) {
                holder.image.setImageDrawable(null);
            }
            else {
                String url = ImageManager.getBestUrl(live.getMedia(), true);
                if (url != null && !url.isEmpty()) {
                    ImageManager.getInstance().fitImageIntoView(url,
                            holder.image,
                            R.drawable.placeholder_large);
                }
            }
        }
    }

    public class LiveViewHolder extends RecyclerView.ViewHolder {
        public ImageView    image;
        public FontTextView date;
        public TextView     title;
        public TextView     text;
        public LinearLayout headerLayout;
        public FontTextView header;
        public ImageView    overlay;
        public View         topPadding;
        public View         shadow;
        public View         top;

        private View    view;

        public LiveViewHolder(View itemView) {
            super(itemView);

            view = itemView;

            image = (ImageView) itemView.findViewById(R.id.image);
            date = (FontTextView) itemView.findViewById(R.id.date);
            title = (TextView) itemView.findViewById(R.id.title);
            text = (TextView) itemView.findViewById(R.id.text);
            headerLayout = (LinearLayout) itemView.findViewById(R.id.header);
            header = (FontTextView) itemView.findViewById(R.id.date_header);
            overlay = (ImageView) itemView.findViewById(R.id.picto_overlay);
            topPadding = itemView.findViewById(R.id.top_padding);
            shadow = itemView.findViewById(R.id.shadow);
            top = itemView.findViewById(R.id.top);
        }
    }
}
