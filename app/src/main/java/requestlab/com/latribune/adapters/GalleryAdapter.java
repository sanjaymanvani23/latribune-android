package requestlab.com.latribune.adapters;

import android.content.Context;
import android.os.Bundle;

import android.util.Log;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import java.util.ArrayList;
import java.util.List;

import requestlab.com.latribune.fragments.GalleryFragment;

/**
 * Created by lchazal on 25/04/16.
 */
public class GalleryAdapter extends FragmentStatePagerAdapter {


    private List<MediaData>    content;

    public GalleryAdapter(FragmentManager fm, Context context, List<MediaData> mediaList) {
        super(fm);

        content = mediaList;
        if (content == null) {
            content = new ArrayList<>();
//            content.add("http://lorempixel.com/300/500/");
//            content.add("http://lorempixel.com/300/500/");
//            content.add("http://lorempixel.com/300/500/");
        }

        Log.d("GALLERY", "Gallery adapter created");
    }



    @Override
    public Fragment getItem(int position) {
        Fragment fragment = new GalleryFragment();

        Bundle payload = new Bundle();
        payload.putString("imageurl", content.get(position).mediaUrl);
        payload.putString("imagelegend", content.get(position).mediaLegend);

        fragment.setArguments(payload);

        return fragment;
    }

    @Override
    public int getCount() {
        return content.size();
    }

    public static class MediaData {
        public String mediaUrl;
        public String mediaLegend;

        public MediaData() {
        }
    }
}
