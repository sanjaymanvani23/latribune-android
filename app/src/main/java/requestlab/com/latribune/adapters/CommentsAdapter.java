package requestlab.com.latribune.adapters;

import android.content.Context;
import android.content.Intent;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import org.ocpsoft.prettytime.PrettyTime;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import requestlab.com.latribune.R;
import requestlab.com.latribune.activities.ReactActivity;
import requestlab.com.latribune.data.Comment;

/**
 * Created by lchazal on 22/03/16.
 */
public class CommentsAdapter extends RecyclerView.Adapter<CommentsAdapter.CommentViewHolder> {

    private LayoutInflater  inflator;

    private List<Comment>   values;

    private Context context;

    private int articleId;

    public CommentsAdapter(Context context, int articleId) {
        values = new ArrayList<>();

        this.context = context;
        this.articleId = articleId;

        inflator = LayoutInflater.from(context);
    }

    @Override
    public CommentViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View recipeView = inflator.inflate(R.layout.fragment_comment, parent, false);
        return new CommentViewHolder(recipeView);
    }

    @Override
    public void onBindViewHolder(CommentViewHolder holder, int position) {
        Comment comment = values.get(position);

        holder.bind(comment, inflator);
    }

    public void addComment(Comment comment) {
        values.add(comment);
        notifyItemInserted(values.size() - 1);
    }

    public void clear() {
        values.clear();
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return values.size();
    }

    public class CommentViewHolder extends RecyclerView.ViewHolder {

        private TextView    name;
        private TextView    time;
        private TextView    comment;
        private TextView    commentNumber;
        private View        commentNumberBox;
        private LinearLayout    subCommentBox;
        private LinearLayout    expandableView;
        private TextView    answer;

        public CommentViewHolder(View itemView) {
            super(itemView);

            name = (TextView) itemView.findViewById(R.id.name);
            time = (TextView) itemView.findViewById(R.id.time);
            comment = (TextView) itemView.findViewById(R.id.comment);
            commentNumberBox = itemView.findViewById(R.id.comment_number_box);
            commentNumber = (TextView) itemView.findViewById(R.id.comment_number);
            subCommentBox = (LinearLayout) itemView.findViewById(R.id.comment_box);
            expandableView = (LinearLayout) itemView.findViewById(R.id.expanding_layout);
            answer = (TextView) itemView.findViewById(R.id.answer);
        }

        public void bind(final Comment comment, LayoutInflater inlater) {
            this.name.setText(comment.getPseudo());

            Calendar calendar = Calendar.getInstance();
            PrettyTime p = new PrettyTime(Locale.getDefault());
            calendar.setTime(comment.getDate());
            time.setText(p.format(calendar));

            this.comment.setText(comment.getContent());
            if (comment.getResponses() != null && comment.getResponses().size() > 0) {
                commentNumberBox.setVisibility(View.VISIBLE);

                commentNumber.setText(context.getResources().getQuantityString(R.plurals.comment_toolbar_title, comment.getResponses().size(), comment.getResponses().size()));
                subCommentBox.removeAllViews();
                for (Comment sub :
                        comment.getResponses()) {
                    View v = inlater.inflate(R.layout.fragment_subcomment, null, false);
                    bindSubView(v, sub);
                    subCommentBox.addView(v);
                }
                expandableView.setVisibility(View.VISIBLE);
            } else {
                commentNumberBox.setVisibility(View.GONE);
                expandableView.setVisibility(View.GONE);
                subCommentBox.removeAllViews();
            }

            answer.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, ReactActivity.class);
                    intent.putExtra(ReactActivity.REACT_ARTICLE_ID, articleId);
                    intent.putExtra(ReactActivity.REACT_CALLER, CommentsAdapter.class.getSimpleName());
                    intent.putExtra(ReactActivity.REACT_PSEUDO, comment.getPseudo());
                    context.startActivity(intent);
                }
            });
        }

        public void bindSubView(View v, Comment response) {
            TextView name = (TextView) v.findViewById(R.id.name);
            TextView time = (TextView) v.findViewById(R.id.time);
            TextView comment = (TextView) v.findViewById(R.id.comment);

            if (name != null) name.setText(response.getPseudo());
            if (time != null) time.setText(response.getDate().toString());
            if (comment != null) comment.setText(response.getContent());
        }
    }
}
