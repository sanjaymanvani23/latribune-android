package requestlab.com.latribune.adapters;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.GridView;

import requestlab.com.latribune.C.Constants;
import requestlab.com.latribune.R;
import requestlab.com.latribune.activities.FavoritesActivity;
import requestlab.com.latribune.cache.TribuneCache;
import requestlab.com.latribune.data.Thematics;
import requestlab.com.latribune.utils.Triple;

/**
 * Created by lchazal on 05/04/16.
 */
public class CategoryPickerAdapter extends BaseAdapter {

    private Context context;

    private int viewWidth;
    private int viewHeight;

    private boolean[]   favorite = new boolean[Constants.NOTIFICATION_KEYS.size()];

//    private SharedPreferences prefs;

    private Thematics th;

    public CategoryPickerAdapter(Context c, int viewWidth, int viewHeight) {
        context = c;

//        prefs = c.getSharedPreferences(c.getString(R.string.shared_pref_session_file), Context.MODE_PRIVATE);
        this.viewWidth = viewWidth;
        this.viewHeight = viewHeight;

        th = TribuneCache.getInstance().getThematics();
        if (th == null) th = new Thematics();
        for (int i = 0; i < favorite.length; i++) {
            Triple<String, String, Integer> t = Constants.NOTIFICATION_KEYS.get(i);
            favorite[i] = FavoritesActivity.getValue(th, t.first());
        }
    }

    public int getCount() {
        return Constants.NOTIFICATION_KEYS.size();
    }

    public Object getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return 0;
    }

    // create a new ImageView for each item referenced by the Adapter
    public View getView(final int position, View convertView, ViewGroup parent) {
        ThemeViewHolder vh = null;

        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.button_category_picker, parent, false);
            vh = new ThemeViewHolder(convertView);
            convertView.setTag(vh);
        } else {
            vh = (ThemeViewHolder) convertView.getTag();
        }

        vh.bind(position);

//        View v = LayoutInflater.from(context).inflate(R.layout.button_category_picker, null);
//        v.setLayoutParams(new GridView.LayoutParams(viewWidth / 2, (int) context.getResources().getDimension(R.dimen.theme_button_height)));
//
//        Button button = (Button) v.findViewById(R.id.btn);
//        final View filter = v.findViewById(R.id.view);
//
//        v.setBackgroundResource(Constants.NOTIFICATION_KEYS.get(position).third());
//
//        button.setText(Constants.NOTIFICATION_KEYS.get(position).second());
////        if (prefs != null) {
//            Triple<String, String, Integer> t = Constants.NOTIFICATION_KEYS.get(position);
//
////            boolean isChecked = prefs.getBoolean(t.first(), false);
//            boolean isChecked = FavoritesActivity.getValue(th, t.first());
//            filter.setVisibility(isChecked ? View.VISIBLE : View.GONE);
//            favorite[position] = filter.getVisibility() == View.VISIBLE;
////        }
//        button.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                filter.setVisibility(filter.getVisibility() == View.GONE ? View.VISIBLE : View.GONE);
//                favorite[position] = filter.getVisibility() == View.VISIBLE;
////                SharedPreferences.Editor editor = prefs.edit();
////                editor.putBoolean(Constants.NOTIFICATION_KEYS.get(position).first(), favorite[position]);
////                editor.apply();
//            }
//        });

        return convertView;
    }

    public boolean[] getFavoritesStatus() {
        return favorite;
    }

    private class ThemeViewHolder {
        private View    view;
        private Button  button;
        private View    filter;

        public ThemeViewHolder(View v) {
            view = v;
            button = (Button) v.findViewById(R.id.btn);
            filter = v.findViewById(R.id.view);
        }

        public void     bind(final int position) {
            Triple<String, String, Integer> t = Constants.NOTIFICATION_KEYS.get(position);
            Log.d("THEME", "Binding View at position " + position);
            Log.d("THEME", "text " +t.second());
            Log.d("THEME", "key " + t.third());

            view.setBackgroundResource(t.third());
            button.setText(t.second());
            boolean isChecked = favorite[position];
            filter.setVisibility(isChecked ? View.VISIBLE : View.INVISIBLE);
            favorite[position] = filter.getVisibility() == View.VISIBLE;
            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    filter.setVisibility(filter.getVisibility() == View.INVISIBLE ? View.VISIBLE : View.INVISIBLE);
                    favorite[position] = filter.getVisibility() == View.VISIBLE;
                    Log.d("THEME", "Clicked position " + position);
                    Log.d("THEME", "Checked set to " + favorite[position]);
                }
            });
        }
    }
}