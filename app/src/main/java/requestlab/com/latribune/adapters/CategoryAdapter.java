package requestlab.com.latribune.adapters;

import android.content.Context;
import android.content.Intent;

import android.util.Log;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.SortedList;

import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.CustomEvent;

import java.util.List;

import requestlab.com.latribune.C.Constants;
import requestlab.com.latribune.R;
import requestlab.com.latribune.activities.BetterArticleDetailsActivity;
import requestlab.com.latribune.cache.TribuneCache;
import requestlab.com.latribune.data.Article;
import requestlab.com.latribune.utils.ImageManager;
import requestlab.com.latribune.utils.StringUtils;
import requestlab.com.latribune.utils.StylesManager;
import requestlab.com.latribune.viewholders.AViewHolder;
import requestlab.com.latribune.viewholders.MNGBannerViewHolder;
import requestlab.com.latribune.viewholders.NativeHolder;
import requestlab.com.latribune.views.fontviews.FontTextView;

/**
 * Created by lchazal on 01/04/16.
 */
public class CategoryAdapter extends RecyclerView.Adapter<AViewHolder> {

    private final int maxArticle;

    protected Context         context;
    protected SortedList<Article> sortedList;
    protected int[] styleSet;

    protected LayoutInflater  inflater;

    private static final int normalColorId = R.color.text_color;
    private static final int readColorId = R.color.text_color_read;

    public CategoryAdapter(Context context) {
        this(context, StylesManager.Style.DEFAULT);
    }

    public CategoryAdapter(Context context, StylesManager.Style style) {
        this.context = context;

        this.styleSet = StylesManager.getStyleArray(style);
        maxArticle = this.styleSet.length;
        inflater = LayoutInflater.from(context);
        sortedList = createSortedList();
    }

    protected void sendTrackingData(String title) {

    }

    public int[] getStyleSet() {
        return styleSet;
    }

    protected SortedList<Article>    createSortedList() {
        return new SortedList<>(Article.class, new SortedList.Callback<Article>() {
            @Override
            public int compare(Article o1, Article o2) {
                return o1.getPosition() - o2.getPosition();
            }

            @Override
            public void onInserted(int position, int count) {
                notifyItemRangeInserted(position, count);
            }

            @Override
            public void onRemoved(int position, int count) {
                notifyItemRangeRemoved(position, count);
            }

            @Override
            public void onMoved(int fromPosition, int toPosition) {
                notifyItemMoved(fromPosition, toPosition);
            }

            @Override
            public void onChanged(int position, int count) {
                notifyItemRangeChanged(position, count);
            }

            @Override
            public boolean areContentsTheSame(Article oldItem, Article newItem) {
                return oldItem.getTitle().equals(newItem.getTitle());
            }

            @Override
            public boolean areItemsTheSame(Article item1, Article item2) {
//                return item1.getCompositeKey().equals(item2.getCompositeKey());
                return item1.getId() == item2.getId();
            }
        });
    }

    public void addItemList(List<Article> items) {
        if (items == null) return;

        Log.d("CategoryAdapter", "Article list before : ");
        for (int i = 0; i < sortedList.size(); i++) {
            Log.d("CategoryAdapter", "" + i + " " + sortedList.get(i).getCompositeKey());
        }

        Log.d("CategoryAdapter", "Articles added : ");
        for (int i = 0; i < items.size(); i++) {
            Log.d("CategoryAdapter", "" + i + " " + items.get(i).getCompositeKey());
        }

        sortedList.beginBatchedUpdates();
        sortedList.addAll(items);
        sortedList.endBatchedUpdates();



        Log.d("CategoryAdapter", "Article list after : ");
        for (int i = 0; i < sortedList.size(); i++) {
            Log.d("CategoryAdapter", "" + i + " " + sortedList.get(i).getCompositeKey());
        }
    }

    public boolean remove(Article a) {
        return sortedList.remove(a);
    }

    public void     clear() {
        sortedList.clear();
    }

    @Override
    public AViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case StylesManager.ART_SMALL:
                return new ArticleViewHolder(inflater.inflate(R.layout.vh_article_small, parent, false), false);
            case StylesManager.ART_LARGE:
                return new ArticleViewHolder(inflater.inflate(R.layout.vh_article_large, parent, false), true);
            case StylesManager.ART_LARGE_CARD:
                return new ArticleViewHolder(inflater.inflate(R.layout.vh_article_large_card, parent, false), true);
            case StylesManager.AD_BANNER:
                return new MNGBannerViewHolder(inflater.inflate(R.layout.vh_mngad_banner, parent, false));
            case StylesManager.AD_NATIVE:
                return new NativeHolder(inflater.inflate(R.layout.ad_unit_list, parent, false));
                //return new MNGNativeViewHolder(inflater.inflate(R.layout.vh_mngad_native, parent, false));
            default:
                return new ArticleViewHolder(inflater.inflate(R.layout.vh_article_small_card_horizontal, parent, false), false);
        }
    }

    @Override
    public void onBindViewHolder(AViewHolder holder, int position) {
        if (getRealPosition(position) >= sortedList.size()) {
            holder.bindViewHolder(context, null, position);
        }
        else {
            holder.bindViewHolder(context, sortedList.get(getRealPosition(position)), position);
        }
    }

    @Override
    public int getItemCount() {
        if (sortedList.size() <= ExtraView.AD_NATIVE.viewOffset) return sortedList.size();
        return (sortedList.size() < maxArticle ? sortedList.size() : maxArticle);
    }

    @Override
    public int getItemViewType(int position) {
        if (position >= styleSet.length) {
            return styleSet[styleSet.length -  1];
        }
        return styleSet[position];
    }

    public boolean isEmpty() {
        return sortedList.size() <= 0;
    }

    protected int getRealPosition(int position) {
        if (position >= ExtraView.AD_BANNER.viewPosition) {
            return position - ExtraView.AD_BANNER.viewOffset;
        }
        if (position >= ExtraView.AD_NATIVE.viewPosition) {
            return position - ExtraView.AD_NATIVE.viewOffset;
        }
        return position;
    }

    enum ExtraView {
        AD_NATIVE(6, 1),
        AD_BANNER(9, 2);

        final int viewPosition;
        final int viewOffset;

        ExtraView(int position, int offset) {
            viewPosition = position;
            viewOffset = offset;
        }
    }

    public class ArticleViewHolder extends AViewHolder {

        private View    view;

        private ImageView    image;
        private FontTextView text;
        private ImageView   overlay;

        private boolean     isLarge = false;

//        public ArticleViewHolder(View itemView) {
//            this(itemView, false);
//        }

        public ArticleViewHolder(View itemView, boolean isLarge) {
            super(itemView);

            view = itemView;

            this.isLarge = isLarge;

            image = (ImageView) itemView.findViewById(R.id.image);
            text = (FontTextView) itemView.findViewById(R.id.text);
            overlay = (ImageView) itemView.findViewById(R.id.picto_overlay);
        }

        @Override
        public void bindViewHolder(final Context context, final Article article, final int position) {
            if (article == null) return;

            String subHead = article.getSubHeading();

            Log.d("BINDING", "Binding article " + article.getId() + " wich is read ? " + article.isRead());

            if (view != null) {
                view.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(CategoryAdapter.this.context, BetterArticleDetailsActivity.class);

                        intent.putExtra(BetterArticleDetailsActivity.ARTICLE_DETAIL_ID, article.getId());
                        intent.putExtra(BetterArticleDetailsActivity.ARTICLE_DETAIL_KEY, article.getCompositeKey());
                        sendTrackingData(article.getTitle());

                        CategoryAdapter.this.context.startActivity(intent);

                        TribuneCache.getInstance().setArticleReadStatus(article, true);
                        setFormattedSubHeadText(article);

//                        notifyDataSetChanged();
//                    listener.onClick(v, position);
                    }
                });
                view.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        Answers.getInstance().logCustom(new CustomEvent("Article flagged")
                                .putCustomAttribute("articleDesc", article.getId() + " " + article.getCompositeKey() + " " + article.getTitle().substring(0, 10) + " " + article.getPosition())
                                .putCustomAttribute("articleId", article.getId())
                                .putCustomAttribute("articleKey", article.getCompositeKey())
                                .putCustomAttribute("articleTitle", article.getTitle())
                                .putCustomAttribute("categoryId", article.getParentCategory()));
                        Log.e("LONGCLICK", "Long clicked article " + article.getTitle());
                        return true;
                    }
                });
            }

            setFormattedSubHeadText(article);

            if (image != null) {
                image.setImageDrawable(null);
                String url = ImageManager.getBestUrl(article.getMedia(), isLarge);

                if (url != null && !url.isEmpty()) {
                    ImageManager.getInstance().fitImageIntoView(url,
                            image,
                            R.drawable.placeholder_head);
                }
            }
            if (overlay != null) {
                if (isDiaporama(article)) {
                    overlay.setVisibility(View.VISIBLE);
                    overlay.setImageResource(R.drawable.icon_diaporama);
                    return;
                }
                if (subHead != null && (subHead.equals("video") || subHead.equals("Vidéo"))) {
                    overlay.setVisibility(View.VISIBLE);
                    overlay.setImageResource(R.drawable.icon_video);
                } else {
                    overlay.setVisibility(View.GONE);
                }
            }
        }

        private boolean isDiaporama(Article a) {
            return a.getMedias() != null && a.getMedias().size() > 0;
        }

        private void setFormattedSubHeadText(Article article) {
            if (text == null) return;

            if (isDiaporama(article)) {
                text.setText(StringUtils.getSpannableString(context,
                        "diaporama".toUpperCase() + "  ",
                        article.getTitle(),
                        StringUtils.Fonts.OPEN_SANS_CONDENSED_BOLD,
                        StringUtils.Fonts.ROBOTO_SLAB_REGULAR,
                        ContextCompat.getColor(context, R.color.subhead_blue),
                        ContextCompat.getColor(context, article.isRead() ? readColorId : normalColorId)
                ));
                return;
            }

            String subHeadText = article.getSubHeading();

            if (subHeadText == null || subHeadText.isEmpty()) {
                text.setText(article.getTitle());
                text.setTextColor(ContextCompat.getColor(context, article.isRead() ? readColorId : normalColorId));
            } else {
                Pair<Integer, Integer> headerInfo = Constants.ARTICLE_SUBHEAD.get(subHeadText);

                String  subHeader = headerInfo == null ? subHeadText : context.getString(headerInfo.first);
                int     subHeaderColorId = article.isRead() ? readColorId :
                        headerInfo == null ? R.color.subhead_blue : headerInfo.second;

                text.setText(StringUtils.getSpannableString(context,
                        subHeader.toUpperCase() + "  ",
                        article.getTitle(),
                        StringUtils.Fonts.OPEN_SANS_CONDENSED_BOLD,
                        StringUtils.Fonts.ROBOTO_SLAB_REGULAR,
                        ContextCompat.getColor(context, subHeaderColorId),
                        ContextCompat.getColor(context, article.isRead() ? readColorId : normalColorId)
                ));
            }
        }
    }
}
