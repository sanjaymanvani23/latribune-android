package requestlab.com.latribune.adapters;

import android.content.Context;
import android.os.Bundle;


import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import java.util.List;

import requestlab.com.latribune.C.Constants;
import requestlab.com.latribune.cache.TribuneCache;
import requestlab.com.latribune.data.Category;
import requestlab.com.latribune.fragments.CategoryFragment;
import requestlab.com.latribune.fragments.LiveFragment;

/**
 * Created by lchazal on 15/03/16.
 */
public class HomePagerAdapter extends FragmentStatePagerAdapter {

    private Context context;
    private List<Category>  content;

    public HomePagerAdapter(FragmentManager fm, Context context) {
        super(fm);

        this.context = context;
        content = TribuneCache.getInstance().getCategoryByLevel(1);
    }

    @Override
    public Fragment getItem(int position) {
        Fragment newFragment;

        switch (position) {
            case 0: //Live
                newFragment = new LiveFragment();
                break;
            default: //Dynamic
                int id = content.get(position).getId();
                switch (id) {
                    default:
                        newFragment = new CategoryFragment();
                        break;
                }
        }
        if (position > 0) {
            Bundle payload = new Bundle();
            payload.putInt(CategoryFragment.KEY_CATEGORY_ID, content.get(position).getId());
            newFragment.setArguments(payload);
        }
        return newFragment;
    }

    @Override
    public int getCount() {
        return content.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
//        return titles[position];
        String name = content.get(position).getName();

        if (Constants.CATEGORY_NAMES.containsKey(name))
            return context.getString(Constants.CATEGORY_NAMES.get(name));
        else
            return name;
    }
}
