package requestlab.com.latribune.adapters;

import android.content.Context;


import androidx.recyclerview.widget.SortedList;

import requestlab.com.latribune.R;
import requestlab.com.latribune.data.Article;
import requestlab.com.latribune.tracking.TrackerManager;
import requestlab.com.latribune.viewholders.AViewHolder;

/**
 * Created by lchazal on 01/04/16.
 */
public class MostReadAdapter extends CategoryAdapter {

    public MostReadAdapter(Context context) {
        super(context);
    }

    @Override
    protected void sendTrackingData(String title) {
        TrackerManager.getInstance().tagScreen(context.getString(R.string.tracking_most_read_chapter),
                context.getString(R.string.tracking_sub_articles),
                title);
    }

    @Override
    protected SortedList<Article> createSortedList() {
        return new SortedList<>(Article.class, new SortedList.Callback<Article>() {
            @Override
            public int compare(Article o1, Article o2) {
                return o2.getViewsCount() - o1.getViewsCount();
            }

            @Override
            public void onInserted(int position, int count) {
                notifyItemRangeInserted(position, count);
            }

            @Override
            public void onRemoved(int position, int count) {
                notifyItemRangeRemoved(position, count);
            }

            @Override
            public void onMoved(int fromPosition, int toPosition) {
                notifyItemMoved(fromPosition, toPosition);
            }

            @Override
            public void onChanged(int position, int count) {
                notifyItemRangeChanged(position, count);
            }

            @Override
            public boolean areContentsTheSame(Article oldItem, Article newItem) {
                return oldItem.getTitle().equals(newItem.getTitle());
            }

            @Override
            public boolean areItemsTheSame(Article item1, Article item2) {
                return item1.getCompositeKey().equals(item2.getCompositeKey());
            }
        });
    }

    @Override
    public void onBindViewHolder(AViewHolder holder, int position) {
        holder.bindViewHolder(context, sortedList.get(position), position);
    }
}
