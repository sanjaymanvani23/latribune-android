package requestlab.com.latribune.C;

import android.util.Pair;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import requestlab.com.latribune.R;
import requestlab.com.latribune.utils.Triple;

/**
 * Created by lchazal on 14/04/16.
 *
 */
public class Constants {


    public static final int CATEGORY_ID_5_INFOS = -1;
    public static final int CATEGORY_ID_MOST_READ = -2;
    public static final int CATEGORY_ID_RDV = -3;
    public static final int CATEGORY_ID_LIVE = -4;
    public static final int CATEGORY_ID_TOP = 1;


    public static final Map<String, Pair<Integer, Integer>>   ARTICLE_SUBHEAD;
    static {
        Map<String, Pair<Integer, Integer>> map = new HashMap<>();
        map.put("exclusive", new Pair<>(R.string.subhead_exclusive, R.color.subhead_red));
        map.put("Exclusif", new Pair<>(R.string.subhead_exclusive, R.color.subhead_red));

        ARTICLE_SUBHEAD = Collections.unmodifiableMap(map);
    }

    public static final Map<String, Integer>   CATEGORY_NAMES;
    static {
        Map<String, Integer> map = new HashMap<>();
        map.put("Régions", R.string.category_area);
        map.put("Opinions", R.string.category_ideas);
        map.put("Technos & Medias", R.string.category_techno);

        CATEGORY_NAMES = Collections.unmodifiableMap(map);
    }

    public static final List<Triple<String, String, Integer>> NOTIFICATION_KEYS;
    static {
        List<Triple<String, String, Integer>> list = new ArrayList<>();
        list.add(new Triple<>("optin_economie_france", "ECONOMIE\nFRANCE", R.drawable.push_economie_france));
        list.add(new Triple<>("optin_economie_europe_inter", "ECONOMIE EUROPE\n& INTERNATIONALE", R.drawable.push_eco_europe));
        list.add(new Triple<>("optin_bourse_marches", "MARCHÉS\nFINANCIERS", R.drawable.push_marches_financier));
        list.add(new Triple<>("optin_banque-assurance", "BANQUE\n& ASSURANCE", R.drawable.push_banque_assur));
        list.add(new Triple<>("optin_transport", "TRANSPORT /\nAUTOMOBILE", R.drawable.push_transport));
        list.add(new Triple<>("optin_grande_distrib", "DISTRIBUTION\n& CONSO", R.drawable.push_distrib));
        list.add(new Triple<>("optin_aero_defense", "AÉRONAUTIQUE\n& DÉFENSE", R.drawable.push_aeronautique));
        list.add(new Triple<>("optin_techno_medias_telecoms", "TECHNO, MÉDIAS\n& TÉLÉCOM", R.drawable.push_techno_medias));
        list.add(new Triple<>("optin_finances_immo", "FINANCES PERSO\n& IMMOBILIER", R.drawable.push_finances_persos));
        list.add(new Triple<>("optin_energie_green", "ENERGIE\n& GREEN BUSINESS", R.drawable.push_energie_green));

        NOTIFICATION_KEYS = Collections.unmodifiableList(list);
    }
//
//    public static final Map<String, String>     NOTIFICATION_KEYS;
//    static {
//        Map<String, String> map = new HashMap<>();
//        map.put("optin_economie_france", "ECONOMIE FRANCE");
//        map.put("optin_economie_europe_inter", "ECONOMIE EUROPE & INTERNATIONALE");
//        map.put("optin_bourse_marches", "MARCHÉS FINANCIERS");
//        map.put("optin_banque-assurance", "BANQUE & ASSURANCE");
//        map.put("optin_transport", "TRANSPORT / AUTOMOBILE");
//        map.put("optin_grande_distrib", "DISTRIBUTION & CONSO");
//        map.put("optin_aero_defense", "AÉRONAUTIQUE & DÉFENSE");
//        map.put("optin_techno_medias_telecoms", "TECHNO, MÉDIAS & TÉLÉCOM");
//        map.put("optin_finances_immo", "FINANCES PERSO & IMMOBILIER");
//        map.put("optin_energie_green", "ENERGIE & GREEN BUSINESS");
//
//        NOTIFICATION_KEYS = Collections.unmodifiableMap(map);
//    }

//
//    public static final Map<String, Integer>   CONTESTS_KEYS;
//    static {
//        Map<String, Integer> map = new HashMap<>();
//        map.put("IN REVIEW", R.string.in_review);
//        map.put("CANCELED", R.string.canceled);
//        map.put("UPCOMING", R.string.upcoming);
//        map.put("STANDBY", R.string.standby);
//        map.put("IN PROGRESS", R.string.in_progress);
//        map.put("REPORTED", R.string.reported);
//        map.put("TENTATIVE", R.string.tentative);
//        map.put("DELETED", R.string.deleted);
//
//        map.put("APPROUVED", R.string.approuved);
//        map.put("PUBLISHED", R.string.published);
//
//        map.put("COMPLETED", R.string.completed);
//        map.put("REJECTED", R.string.rejected);
//
//        CONTESTS_KEYS = Collections.unmodifiableMap(map);
//    }
}
