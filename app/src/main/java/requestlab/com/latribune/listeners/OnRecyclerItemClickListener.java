package requestlab.com.latribune.listeners;

import android.view.View;

/**
 * Created by lchazal on 13/04/16.
 */
public interface OnRecyclerItemClickListener {
    void onClick(View view, int position);
    void onLongCLick(View view, int position);
}
