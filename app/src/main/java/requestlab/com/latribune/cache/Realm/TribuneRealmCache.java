package requestlab.com.latribune.cache.Realm;

import android.app.Application;
import android.util.Log;

import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSyntaxException;
import com.google.gson.TypeAdapter;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.google.gson.stream.JsonWriter;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.Collections;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.RealmQuery;
import io.realm.RealmResults;
import requestlab.com.latribune.C.Constants;
import requestlab.com.latribune.cache.ITribuneCache;
import requestlab.com.latribune.cache.Realm.data.RealmInt;
import requestlab.com.latribune.cache.Realm.data.RealmString;
import requestlab.com.latribune.data.Article;
import requestlab.com.latribune.data.Category;
import requestlab.com.latribune.data.Hebdo;
import requestlab.com.latribune.data.Infos;
import requestlab.com.latribune.data.Media;
import requestlab.com.latribune.data.Stock;
import requestlab.com.latribune.data.Thematics;
import requestlab.com.latribune.data.Update;
import requestlab.com.latribune.utils.DateConverter;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by lchazal on 02/02/16.
 *
 * Officin cache implementation using io.Realm
 */
public class TribuneRealmCache implements ITribuneCache {

    private static RealmConfiguration mConfiguration;
    private static Realm mRealmInstance;

    public TribuneRealmCache() {
    }

    @Override
    public <O extends RealmObject> List<O>  getAll(Class<O> type) {
        if (mRealmInstance == null) {
            Log.d("REALM", "Realm instance is null, call initialize() first");
            return null;
        }
        return mRealmInstance.where(type)
                .findAll();
    }

    @Override
    public void addOrUpdateObject(RealmObject object) {
        if (mRealmInstance == null) {
            Log.d("REALM", "Realm instance is null, call initialize() first");
            return;
        }

        mRealmInstance.beginTransaction();
        mRealmInstance.copyToRealmOrUpdate(object);
        mRealmInstance.commitTransaction();
    }

    @Override
    public void addOrUpdateObjectList(List<? extends RealmObject> list) {
        if (mRealmInstance == null) {
            Log.d("REALM", "Realm instance is null, call initialize() first");
            return;
        }
        mRealmInstance.beginTransaction();
        mRealmInstance.copyToRealmOrUpdate(list);
        mRealmInstance.commitTransaction();
    }

    @Override
    public <O extends RealmObject> O getObject(Class<O> type, int id) {
        if (mRealmInstance == null) {
            Log.d("REALM", "Realm instance is null, call initialize() first");
            return null;
        }
        return mRealmInstance.where(type)
                .equalTo("id", id)
                .findFirst();
    }

    @Override
    public <O extends RealmObject> RealmResults<O> getObjectList(Class<O> type) {
        if (mRealmInstance == null) {
            Log.d("REALM", "Realm instance is null, call initialize() first");
            return null;
        }
        return mRealmInstance.where(type)
                .findAll();
    }

    @Override
    public void deleteObject(RealmObject object) {
        if (mRealmInstance == null) {
            Log.d("REALM", "Realm instance is null, call initialize() first");
            return;
        }

        mRealmInstance.beginTransaction();
        object.deleteFromRealm();
        mRealmInstance.commitTransaction();
    }

    @Override
    public void deleteObjectList(RealmResults list) {
        if (mRealmInstance == null) {
            Log.d("REALM", "Realm instance is null, call initialize() first");
            return;
        }

        mRealmInstance.beginTransaction();
        list.deleteAllFromRealm();
        mRealmInstance.commitTransaction();
    }

    @Override
    public Article getArticle(String key) {
        if (mRealmInstance == null) {
            Log.d("REALM", "Realm instance is null, call initialize() first");
            return null;
        }
        return mRealmInstance.where(Article.class)
                .equalTo("compositeKey", key)
                .findFirst();
    }

    @Override
    public Stock getStock(String stockName) {
        if (mRealmInstance == null) {
            Log.d("REALM", "Realm instance is null, call initialize() first");
            return null;
        }
        return mRealmInstance.where(Stock.class)
                .equalTo("indice", stockName)
                .findFirst();
    }

    @Override
    public void removeArticlesNotIn(List<Article> list, int categoryId) {
        if (mRealmInstance == null) {
            Log.d("REALM", "Realm instance is null, call initialize() first");
            return;
        }

        RealmQuery<Article> allArticles = mRealmInstance.where(Article.class)
                .equalTo("parentCategory", categoryId);
        for(Article newArticle : list) {
            allArticles = allArticles.notEqualTo("compositeKey", newArticle.getCompositeKey());
        }

        RealmResults<Article> results = allArticles.findAll();
        if (results.size() > 0) {
            Log.d("REALM", "Deleting " + results.size() + " obsolete article(s)");
            mRealmInstance.beginTransaction();
            results.deleteAllFromRealm();
            mRealmInstance.commitTransaction();
        }
    }

    @Override
    public RealmResults<Article> getObsoleteArticles(List<Article> list, int categoryId) {
        if (mRealmInstance == null) {
            Log.d("REALM", "Realm instance is null, call initialize() first");
            return null;
        }

        Log.d("CLEARING " + categoryId, "Found " + list.size() + " new articles");

        RealmQuery<Article> allArticles = mRealmInstance.where(Article.class)
                .equalTo("parentCategory", categoryId);

        Log.d("CLEARING " + categoryId, "Have " + allArticles.findAll().size() + " old articles");

        for(Article newArticle : list) {
            allArticles = allArticles.notEqualTo("compositeKey", newArticle.getCompositeKey());
        }

        RealmResults<Article> results = allArticles.findAll();

        Log.d("CLEARING " + categoryId, "Among those,  " + results.size() + " are obsolete");

        return results;

//        if (results.size() > 0) {
//            Log.d("REALM", "Deleting " + results.size() + " obsolete article(s)");
//            mRealmInstance.beginTransaction();
//            results.clear();
//            mRealmInstance.commitTransaction();
//        }
    }

    @Override
    public Update getUpdate(int categoryId) {
        if (mRealmInstance == null) {
            Log.d("REALM", "Realm instance is null, call initialize() first");
            return null;
        }
        return mRealmInstance.where(Update.class)
                .equalTo("categoryId", categoryId)
                .findFirst();
    }

    @Override
    public void setUpdate(int categoryId, Update update) {
        update.setCategoryId(categoryId);
        addOrUpdateObject(update);
    }

    @Override
    public Infos getInfos() {
        if (mRealmInstance == null) {
            Log.d("REALM", "Realm instance is null, call initialize() first");
            return null;
        }
        return mRealmInstance.where(Infos.class)
                .findFirst();
    }

    @Override
    public void addOrUpdateInfos(Infos infos) {
        infos.setKey(1);
        if (infos.getArticles() != null) {
            for (Article a :
                    infos.getArticles()) {
                setArticleExtraData(a, Constants.CATEGORY_ID_5_INFOS);
            }
        }
        addOrUpdateObject(infos);
    }

    @Override
    public void addOrUpdateHebdo(Hebdo hebdo) {
        hebdo.setKey(1);
        addOrUpdateObject(hebdo);
    }

    @Override
    public Hebdo getHebdo() {
        if (mRealmInstance == null) {
            Log.d("REALM", "Realm instance is null, call initialize() first");
            return null;
        }
        return mRealmInstance.where(Hebdo.class)
                .findFirst();
    }

    @Override
    public List<Category> getCategoryByLevel(int level) {
        if (mRealmInstance == null) {
            Log.d("REALM", "Realm instance is null, call initialize() first");
            return null;
        }
        return mRealmInstance.where(Category.class)
                .equalTo("level", 1)
                .notEqualTo("id", 89)
                .sort("position")
                .findAll();
    }

    @Override
    public void setArticleExtraData(Article article, int categoryId) {
        article.setParentCategory(categoryId);
        article.setCompositeKey(Integer.toString(article.getId()) + ":" + Integer.toString(article.getParentCategory()));

        if (article.getComments() != null) article.getComments().setId(article.getId());


        Article storedArticle = getArticle(article.getCompositeKey());
        if (storedArticle != null) article.setRead(storedArticle.isRead());
    }

    @Override
    public void setArticleReadStatus(Article article, boolean read) {
//        Article storedArticle = getArticle(article.getCompositeKey());
        Log.d("SETREAD", "Setting " + article.getCompositeKey() + "read status to " + read);
//        if (storedArticle == null) {
//            Log.d("SETREAD", "Doesn't exist :<");
//            return;
//        }

        addOrUpdateObject(article);

        mRealmInstance.beginTransaction();
        article.setRead(read);
//        storedArticle.setRead(read);
        mRealmInstance.commitTransaction();
    }

    @Override
    public List<Article> getArticlesByCategory(int categoryId) {
        return mRealmInstance.where(Article.class)
                .equalTo("parentCategory", categoryId)
                .findAll();
    }

    @Override
    public Thematics getThematics() {
        return mRealmInstance.where(Thematics.class)
                .equalTo("id", 1)
                .findFirst();
    }

    @Override
    public void clearCategory(int id) {
        RealmResults<Article> allArticles = mRealmInstance.where(Article.class)
                .equalTo("parentCategory", id)
                .findAll();

        mRealmInstance.beginTransaction();
        allArticles.deleteAllFromRealm();
        mRealmInstance.commitTransaction();
    }

    @Override
    public void initialize(Application application) {
        Log.d("REALM", "Realm initialized");
        Realm.init(application);
        mConfiguration = new RealmConfiguration.Builder()
                .deleteRealmIfMigrationNeeded()
                .build();
        Realm.setDefaultConfiguration(mConfiguration);
        mRealmInstance = Realm.getDefaultInstance();
        clearUpdateData();
    }

    @Override
    public void clearCache() {
        if (!mRealmInstance.isClosed()) mRealmInstance.close();
        mRealmInstance = null;
        Realm.deleteRealm(mConfiguration);
    }

    private void clearUpdateData() {
        RealmResults<Update> updates = mRealmInstance.where(Update.class).findAll();

        mRealmInstance.beginTransaction();
        updates.deleteAllFromRealm();
        mRealmInstance.commitTransaction();
    }

    /**
     * Create a Realm specific GSON converter to use with Retrofit deserialization,
     * fixing a Realm-related GSON bug and a GSON-related Realm bug.
     *
     * Also convert timestamps to Date object where applicable.
     *
     * @return generated GsonConvertFactory
     */
    public static GsonConverterFactory getRealmGSONConverter() {
        final Gson gson = new GsonBuilder()
                .setExclusionStrategies(new ExclusionStrategy() { // Dodge GSON's bug causing stack overflow on RealmObject parsing
                    @Override
                    public boolean shouldSkipField(FieldAttributes f) {
                        return f.getDeclaringClass().equals(RealmObject.class);
                    }

                    @Override
                    public boolean shouldSkipClass(Class<?> clazz) {
                        return false;
                    }
                })
                .registerTypeAdapter(new TypeToken<RealmList<RealmInt>>() {
                }.getType(), new TypeAdapter<RealmList<RealmInt>>() {
                    // Allow parsing of RealmInt inside RealmLists without crash

                    @Override
                    public void write(JsonWriter out, RealmList<RealmInt> value) throws IOException {
                        // Ignore
                    }

                    @Override
                    public RealmList<RealmInt> read(JsonReader in) throws IOException {
                        RealmList<RealmInt> list = new RealmList<RealmInt>();
                        in.beginArray();
                        while (in.hasNext()) {
                            list.add(new RealmInt(in.nextInt()));
                        }
                        in.endArray();
                        return list;
                    }
                })
                .registerTypeAdapter(new TypeToken<RealmList<RealmString>>(){}.getType(), new TypeAdapter<RealmList<RealmString>>() {
                 // Allow parsing of RealmString inside RealmLists without crash

                    @Override
                    public void write(JsonWriter out, RealmList<RealmString> value) throws IOException {
                        // Ignore
                    }

                    @Override
                    public RealmList<RealmString> read(JsonReader in) throws IOException {
                        RealmList<RealmString> list = new RealmList<RealmString>();
                        in.beginArray();
                        while (in.hasNext()) {
                            list.add(new RealmString(in.nextString()));
                        }
                        in.endArray();
                        return list;
                    }
                })
                .registerTypeAdapter(Media.class, new TypeAdapter<Media>() {
                    // Allow parsing of RealmString inside RealmLists without crash

                    @Override
                    public void write(JsonWriter out, Media value) throws IOException {
                        // Ignore
                    }

                    @Override
                    public Media read(JsonReader in) throws IOException {
                        if (in.peek() == JsonToken.NULL) {
                            in.nextNull();
                            return null;
                        }
                        if (in.peek() == JsonToken.STRING) {
                            in.nextString();
                            return null;
                        }

                        // || in.peek() == JsonToken.STRING

                        return new Gson().fromJson(in, Media.class);
                    }
                })
                .setDateFormat(DateConverter.TIMESTAMP_FORMAT) // Allow timestamps to be converted to Date object directly when necessary
                .create();

        return GsonConverterFactory.create(gson);
    }
}

