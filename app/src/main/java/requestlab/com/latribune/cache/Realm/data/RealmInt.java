package requestlab.com.latribune.cache.Realm.data;

import io.realm.RealmObject;

/**
 * Created by lchazal on 02/02/16.
 */
public class RealmInt extends RealmObject {

    private int value;

    public RealmInt() {

    }

    public RealmInt(int val) {
        this.value = val;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

}