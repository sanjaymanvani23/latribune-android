package requestlab.com.latribune.cache;


import requestlab.com.latribune.cache.Realm.TribuneRealmCache;

/**
 * Created by lchazal on 02/02/16.
 *
 * Build and instance of the current Cache implementation
 */
public class TribuneCacheFactory {

    public static ITribuneCache getCacheImplementation() {
        return new TribuneRealmCache();
    }
}
