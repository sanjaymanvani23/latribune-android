package requestlab.com.latribune.cache;

import android.app.Application;

import java.util.List;

import io.realm.RealmObject;
import io.realm.RealmResults;
import requestlab.com.latribune.data.Article;
import requestlab.com.latribune.data.Category;
import requestlab.com.latribune.data.Hebdo;
import requestlab.com.latribune.data.Infos;
import requestlab.com.latribune.data.Stock;
import requestlab.com.latribune.data.Thematics;
import requestlab.com.latribune.data.Update;

/**
 * Created by lchazal on 02/02/16.
 *
 */
public interface ITribuneCache {

    void            initialize(Application application);

    void            clearCache();

    <O extends RealmObject> List<O>            getAll(Class<O> type);

    void            addOrUpdateObject(RealmObject object);
    void            addOrUpdateObjectList(List<? extends RealmObject> list);
    <O extends RealmObject> O               getObject(Class<O> type, int id);
    <O extends RealmObject> RealmResults<O> getObjectList(Class<O> type);
    void            deleteObject(RealmObject object);
    void            deleteObjectList(RealmResults list);

    Article         getArticle(String key);

    List<Category>  getCategoryByLevel(int level);

    void setArticleExtraData(Article article, int category);
    void            setArticleReadStatus(Article article, boolean read);

    List<Article>            getArticlesByCategory(int categoryId);

    void            removeArticlesNotIn(List<Article> list, int categoryId);
    RealmResults<Article> getObsoleteArticles(List<Article> list, int categoryId);

    Infos           getInfos();
    void            addOrUpdateInfos(Infos infos);

    Hebdo           getHebdo();
    void            addOrUpdateHebdo(Hebdo hebdo);

    Stock           getStock(String stockName);

    Update          getUpdate(int categoryId);
    void            setUpdate(int categoryId, Update update);

    Thematics       getThematics();
    void            clearCategory(int id);
}
