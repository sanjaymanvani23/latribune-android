package requestlab.com.latribune.cache.Realm.data;

import io.realm.RealmObject;

/**
 * Created by lchazal on 09/02/16.
 */
public class RealmString extends RealmObject {

    private String value;

    public RealmString() {

    }

    public RealmString(String val) {
        this.value = val;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

}