package requestlab.com.latribune.cache;

/**
 * Created by lchazal on 02/02/16.
 *
 * Cache API handler, relay calls to current cache API implementation provided by TribuneCacheFactory
 */
public class TribuneCache {
    private static ITribuneCache ourInstance = TribuneCacheFactory.getCacheImplementation();

    public static ITribuneCache getInstance() {
        return ourInstance;
    }
}
