package requestlab.com.latribune.services;

import android.content.Context;

import com.ad4screen.sdk.A4SIdsProvider;

import requestlab.com.latribune.BuildConfig;

/**
 * Created by lchazal on 10/08/16.
 */
public class MyA4SIdsProvider implements A4SIdsProvider {
    @Override
    public String getPartnerId(Context context) {
        return BuildConfig.ACCENGAGE_PARTNER_ID; //latribune89fa4edf4cd2bf0
    }
    @Override
    public String getPrivateKey(Context context) {
        return BuildConfig.ACCENGAGE_PARTNER_KEY; //4cd28c5ee4886a92fad9ce3991b6569c46f50608
    }
}