package requestlab.com.latribune.data;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

import java.util.Date;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by lchazal on 27/04/16.
 */
public class Infos extends RealmObject {

    @PrimaryKey
    private int key = 1;

    @SerializedName("id")
	@JsonProperty("id")
    private int id;

    @SerializedName("body_html")
	@JsonProperty("body_html")
    private String bodyHTML;

    @SerializedName("body_text")
	@JsonProperty("body_text")
    private String bodyText;

    @SerializedName("publication_date")
	@JsonProperty("publication_date")
    private Date publicationDate;

    @SerializedName("creation_date")
	@JsonProperty("creation_date")
    private Date creationDate;

    @SerializedName("last_modification_date")
	@JsonProperty("last_modification_date")
    private Date lastUpdateDate;

    @SerializedName("informations")
	@JsonProperty("informations")
    private RealmList<Article>  articles;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getBodyHTML() {
        return bodyHTML;
    }

    public void setBodyHTML(String bodyHTML) {
        this.bodyHTML = bodyHTML;
    }

    public String getBodyText() {
        return bodyText;
    }

    public void setBodyText(String bodyText) {
        this.bodyText = bodyText;
    }

    public Date getPublicationDate() {
        return publicationDate;
    }

    public void setPublicationDate(Date publicationDate) {
        this.publicationDate = publicationDate;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public Date getLastUpdateDate() {
        return lastUpdateDate;
    }

    public void setLastUpdateDate(Date lastUpdateDate) {
        this.lastUpdateDate = lastUpdateDate;
    }

    public RealmList<Article> getArticles() {
        return articles;
    }

    public void setArticles(RealmList<Article> articles) {
        this.articles = articles;
    }

    public int getKey() {
        return key;
    }

    public void setKey(int key) {
        this.key = key;
    }
}