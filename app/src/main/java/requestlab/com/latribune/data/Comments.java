package requestlab.com.latribune.data;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by lchazal on 21/04/16.
 */
public class Comments extends RealmObject {

    @PrimaryKey
    private int id;

    @SerializedName("count")
	@JsonProperty("count")
    private int count;

    @SerializedName("last_comment")
	@JsonProperty("last_comment")
    private Comment lastComment;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }


    public Comment getLastComment() {
        return lastComment;
    }

    public void setLastComment(Comment lastComment) {
        this.lastComment = lastComment;
    }
}
