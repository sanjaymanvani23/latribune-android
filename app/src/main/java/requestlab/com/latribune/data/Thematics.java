package requestlab.com.latribune.data;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by lchazal on 11/07/16.
 */
public class Thematics extends RealmObject {

    @PrimaryKey
    private int id = 1;

    private boolean optin_economie_france;
    private boolean optin_economie_europe_inter;
    private boolean optin_bourse_marches;
    private boolean optin_banque_assurance;
    private boolean optin_transport;
    private boolean optin_grande_distrib;
    private boolean optin_aero_defense;
    private boolean optin_techno_medias_telecoms;
    private boolean optin_finances_immo;
    private boolean optin_energie_green;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean isOptin_economie_france() {
        return optin_economie_france;
    }

    public void setOptin_economie_france(boolean optin_economie_france) {
        this.optin_economie_france = optin_economie_france;
    }

    public boolean isOptin_economie_europe_inter() {
        return optin_economie_europe_inter;
    }

    public void setOptin_economie_europe_inter(boolean optin_economie_europe_inter) {
        this.optin_economie_europe_inter = optin_economie_europe_inter;
    }

    public boolean isOptin_bourse_marches() {
        return optin_bourse_marches;
    }

    public void setOptin_bourse_marches(boolean optin_bourse_marches) {
        this.optin_bourse_marches = optin_bourse_marches;
    }

    public boolean isOptin_banque_assurance() {
        return optin_banque_assurance;
    }

    public void setOptin_banque_assurance(boolean optin_banque_assurance) {
        this.optin_banque_assurance = optin_banque_assurance;
    }

    public boolean isOptin_transport() {
        return optin_transport;
    }

    public void setOptin_transport(boolean optin_transport) {
        this.optin_transport = optin_transport;
    }

    public boolean isOptin_grande_distrib() {
        return optin_grande_distrib;
    }

    public void setOptin_grande_distrib(boolean optin_grande_distrib) {
        this.optin_grande_distrib = optin_grande_distrib;
    }

    public boolean isOptin_aero_defense() {
        return optin_aero_defense;
    }

    public void setOptin_aero_defense(boolean optin_aero_defense) {
        this.optin_aero_defense = optin_aero_defense;
    }

    public boolean isOptin_techno_medias_telecoms() {
        return optin_techno_medias_telecoms;
    }

    public void setOptin_techno_medias_telecoms(boolean optin_techno_medias_telecoms) {
        this.optin_techno_medias_telecoms = optin_techno_medias_telecoms;
    }

    public boolean isOptin_finances_immo() {
        return optin_finances_immo;
    }

    public void setOptin_finances_immo(boolean optin_finances_immo) {
        this.optin_finances_immo = optin_finances_immo;
    }

    public boolean isOptin_energie_green() {
        return optin_energie_green;
    }

    public void setOptin_energie_green(boolean optin_energie_green) {
        this.optin_energie_green = optin_energie_green;
    }
}
