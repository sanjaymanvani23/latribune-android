package requestlab.com.latribune.data;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;

/**
 * Created by lchazal on 20/04/16.
 */
public class Related extends RealmObject {

    @SerializedName("article_id")
	@JsonProperty("article_id")
    private int articleId;

    @SerializedName("title")
	@JsonProperty("title")
    private String title;

    @SerializedName("url")
	@JsonProperty("url")
    private String  url;

    public int getArticleId() {
        return articleId;
    }

    public void setArticleId(int articleId) {
        this.articleId = articleId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
