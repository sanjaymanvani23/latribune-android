package requestlab.com.latribune.data;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;

/**
 * Created by lchazal on 01/04/16.
 */
public class MediaFormats extends RealmObject {

    @SerializedName("original")
	@JsonProperty("original")
    private String original;

    @SerializedName("large")
	@JsonProperty("large")
    private String large;

    @SerializedName("small")
	@JsonProperty("small")
    private String small;

    public String getOriginal() {
        return original;
    }

    public void setOriginal(String original) {
        this.original = original;
    }

    public String getLarge() {
        return large;
    }

    public void setLarge(String large) {
        this.large = large;
    }

    public String getSmall() {
        return small;
    }

    public void setSmall(String small) {
        this.small = small;
    }
}
