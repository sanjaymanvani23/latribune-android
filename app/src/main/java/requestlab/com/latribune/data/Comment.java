package requestlab.com.latribune.data;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

import java.util.Date;
import java.util.List;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by lchazal on 21/04/16.
 */
public class Comment extends RealmObject {

    @SerializedName("id")
	@JsonProperty("id")
    @PrimaryKey
    private int id;

    @SerializedName("pseudo")
	@JsonProperty("pseudo")
    private String pseudo;

    @SerializedName("content")
	@JsonProperty("content")
    private String content;

    @SerializedName("date")
	@JsonProperty("date")
    private Date    date;

    @SerializedName("responses")
	@JsonProperty("responses")
    private RealmList<Comment> responses;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPseudo() {
        return pseudo;
    }

    public void setPseudo(String pseudo) {
        this.pseudo = pseudo;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public RealmList<Comment> getResponses() {
        return responses;
    }

    public void setResponses(RealmList<Comment> responses) {
        this.responses = responses;
    }
}
