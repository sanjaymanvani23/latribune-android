package requestlab.com.latribune.data;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by lchazal on 31/03/16.
 */
public class Category extends RealmObject {

    @PrimaryKey
    @SerializedName("id")
	@JsonProperty("id")
    private int     id;

    @SerializedName("position")
	@JsonProperty("position")
    private int     position;

    @SerializedName("name")
	@JsonProperty("name")
    private String  name;

    @SerializedName("lvl")
	@JsonProperty("lvl")
    private int     level;

    @SerializedName("childs")
	@JsonProperty("childs")
    private RealmList<Category>   categories;

    private boolean hidden;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public RealmList<Category> getCategories() {
        return categories;
    }

    public void setCategories(RealmList<Category> categories) {
        this.categories = categories;
    }

    public boolean isHidden() {
        return hidden;
    }

    public void setHidden(boolean hidden) {
        this.hidden = hidden;
    }
}
