package requestlab.com.latribune.data;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

import java.util.Date;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by lchazal on 04/04/16.
 */
public class Live extends RealmObject {

    @SerializedName("id")
	@JsonProperty("id")
    @PrimaryKey
    private int id;

    @SerializedName("title")
	@JsonProperty("title")
    private String title;

    @SerializedName("type")
	@JsonProperty("type")
    private String type;

    @SerializedName("date")
	@JsonProperty("date")
    private Date date;

    @SerializedName("subhead")
	@JsonProperty("subhead")
    private String subHead;

    @SerializedName("media")
	@JsonProperty("media")
    private Media media;

    @SerializedName("api_link")
	@JsonProperty("api_link")
    private String apiLink;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getSubHead() {
        return subHead;
    }

    public void setSubHead(String subHead) {
        this.subHead = subHead;
    }

    public Media getMedia() {
        return media;
    }

    public void setMedia(Media media) {
        this.media = media;
    }

    public String getApiLink() {
        return apiLink;
    }

    public void setApiLink(String apiLink) {
        this.apiLink = apiLink;
    }

//    public void setDate(String date) {
//        this.date = date;
//    }
//
//    public String getDate() {
//        return date;
//    }
}
