package requestlab.com.latribune.data;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import requestlab.com.latribune.cache.Realm.data.RealmString;

/**
 * Created by lchazal on 06/04/16.
 */
public class Stock extends RealmObject {

//    @SerializedName("INDICE")
	@JsonProperty("INDICE")
    @PrimaryKey
    private String  indice;

//    @SerializedName("PRICE")
	@JsonProperty("PRICE")
    private RealmList<RealmString> price;

//    @SerializedName("DATETIME_PRICE")
	@JsonProperty("DATETIME_PRICE")
    private RealmList<RealmString> dateTimePrice;

//    @SerializedName("PERFORMANCE")
	@JsonProperty("PERFORMANCE")
    private RealmList<RealmString> performance;

//    @SerializedName("PERFORMANCE_PCT")
	@JsonProperty("PERFORMANCE_PCT")
    private RealmList<RealmString> performancePct;

//    @SerializedName("CURRENCY")
	@JsonProperty("CURRENCY")
    private RealmList<RealmString> currency;

//    @SerializedName("URL_GRAPHE")
	@JsonProperty("URL_GRAPHE")
    private RealmList<RealmString> grapheUrl;

//    @SerializedName("URL_SITE")
	@JsonProperty("URL_SITE")
    private RealmList<RealmString> siteUrl;

//    @SerializedName("GOOD_URL_SITE")
	@JsonProperty("GOOD_URL_SITE")
    private RealmList<RealmString> goodSiteUrl;

    public String getIndice() {
        return indice;
    }

    public void setIndice(String indice) {
        this.indice = indice;
    }

    public RealmList<RealmString> getPrice() {
        return price;
    }

    public void setPrice(RealmList<RealmString> price) {
        this.price = price;
    }

    public RealmList<RealmString> getDateTimePrice() {
        return dateTimePrice;
    }

    public void setDateTimePrice(RealmList<RealmString> dateTimePrice) {
        this.dateTimePrice = dateTimePrice;
    }

    public RealmList<RealmString> getPerformance() {
        return performance;
    }

    public void setPerformance(RealmList<RealmString> performance) {
        this.performance = performance;
    }

    public RealmList<RealmString> getPerformancePct() {
        return performancePct;
    }

    public void setPerformancePct(RealmList<RealmString> performancePct) {
        this.performancePct = performancePct;
    }

    public RealmList<RealmString> getCurrency() {
        return currency;
    }

    public void setCurrency(RealmList<RealmString> currency) {
        this.currency = currency;
    }

    public RealmList<RealmString> getGrapheUrl() {
        return grapheUrl;
    }

    public void setGrapheUrl(RealmList<RealmString> grapheUrl) {
        this.grapheUrl = grapheUrl;
    }

    public RealmList<RealmString> getSiteUrl() {
        return siteUrl;
    }

    public void setSiteUrl(RealmList<RealmString> siteUrl) {
        this.siteUrl = siteUrl;
    }

    public RealmList<RealmString> getGoodSiteUrl() {
        return goodSiteUrl;
    }

    public void setGoodSiteUrl(RealmList<RealmString> goodSiteUrl) {
        this.goodSiteUrl = goodSiteUrl;
    }
}
