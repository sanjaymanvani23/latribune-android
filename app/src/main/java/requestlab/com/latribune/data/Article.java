package requestlab.com.latribune.data;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

import java.util.Date;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by lchazal on 01/04/16.
 */
//@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class Article extends RealmObject {

    @PrimaryKey
    private String compositeKey;

    @SerializedName("id")
	@JsonProperty("id")
    private int id;

    private int parentCategory;

    @SerializedName("title")
	@JsonProperty("title")
    private String title;

    @SerializedName("subtitle")
	@JsonProperty("subtitle")
    private String subTitle;

    @SerializedName("lead")
	@JsonProperty("lead")
    private String header;

    @SerializedName("body_html")
	@JsonProperty("body_html")
    private String bodyHTML;

    @SerializedName("body_text")
	@JsonProperty("body_text")
    private String bodyText;

    @SerializedName("media")
	@JsonProperty("media")
    private Media media;

    @SerializedName("media_legend")
	@JsonProperty("media_legend")
    private String mediaLegend;

    @SerializedName("signature")
	@JsonProperty("signature")
    private String signature;

    @SerializedName("author_twitter")
	@JsonProperty("author_twitter")
    private String twitter;

    @SerializedName("author_img")
	@JsonProperty("author_img")
    private Media   authorMedia;

    @SerializedName("publication_date")
	@JsonProperty("publication_date")
    private Date publicationDate;

    @SerializedName("creation_date")
	@JsonProperty("creation_date")
    private Date creationDate;

    @SerializedName("last_modification_date")
	@JsonProperty("last_modification_date")
    private Date lastModificationDate;

    @SerializedName("date")
	@JsonProperty("date")
    private Date date;

    @SerializedName("url")
	@JsonProperty("url")
    private String url;

    @SerializedName("subheading_label")
	@JsonProperty("subheading_label")
    private String subHeading;

    @SerializedName("related")
	@JsonProperty("related")
    private RealmList<Related> related;

    @SerializedName("comment")
	@JsonProperty("comment")
    private Comments    comments;

    @SerializedName("estimated_reading_time")
	@JsonProperty("estimated_reading_time")
    private int     readingTime;

    @SerializedName("position")
	@JsonProperty("position")
    private int     position;

    @SerializedName("medias")
	@JsonProperty("medias")
    private RealmList<Media> medias;

    @SerializedName("type")
	@JsonProperty("type")
    private String type;

    @SerializedName("api_link")
	@JsonProperty("api_link")
    private String apiLink;

    @SerializedName("rdv_position")
	@JsonProperty("rdv_position")
    private int     RDVPosition;

    @SerializedName("views_count")
	@JsonProperty("views_count")
    private int     viewsCount;

    @SerializedName("protected")
    @JsonProperty("protected")
    private boolean   protectedArticle;


    private boolean read;

    public int getReadingTime() {
        return readingTime;
    }

    public void setReadingTime(int readingTime) {
        this.readingTime = readingTime;
    }

    public RealmList<Related> getRelated() {
        return related;
    }

    public void setRelated(RealmList<Related> related) {
        this.related = related;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSubTitle() {
        return subTitle;
    }

    public void setSubTitle(String subTitle) {
        this.subTitle = subTitle;
    }

    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    public String getBodyHTML() {
        return bodyHTML;
    }

    public void setBodyHTML(String bodyHTML) {
        this.bodyHTML = bodyHTML;
    }

    public String getBodyText() {
        return bodyText;
    }

    public void setBodyText(String bodyText) {
        this.bodyText = bodyText;
    }

    public int getParentCategory() {
        return parentCategory;
    }

    public void setParentCategory(int parentCategory) {
        this.parentCategory = parentCategory;
    }

    public String getSignature() {
        return signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Media getMedia() {
        return media;
    }

    public void setMedia(Media media) {
        this.media = media;
    }

    public String getMediaLegend() {
        return mediaLegend;
    }

    public void setMediaLegend(String mediaLegend) {
        this.mediaLegend = mediaLegend;
    }

    public String getCompositeKey() {
        return compositeKey;
    }

    public void setCompositeKey(String compositeKey) {
        this.compositeKey = compositeKey;
    }

    public Date getPublicationDate() {
        return publicationDate;
    }

    public void setPublicationDate(Date publicationDate) {
        this.publicationDate = publicationDate;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public Date getLastModificationDate() {
        return lastModificationDate;
    }

    public void setLastModificationDate(Date lastModificationDate) {
        this.lastModificationDate = lastModificationDate;
    }

    public String getSubHeading() {
        return subHeading;
    }

    public void setSubHeading(String subHeading) {
        this.subHeading = subHeading;
    }

    public Comments getComments() {
        return comments;
    }

    public void setComments(Comments comments) {
        this.comments = comments;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public RealmList<Media> getMedias() {
        return medias;
    }

    public void setMedias(RealmList<Media> medias) {
        this.medias = medias;
    }

    public String getTwitter() {
        return twitter;
    }

    public void setTwitter(String twitter) {
        this.twitter = twitter;
    }

    public Media getAuthorMedia() {
        return authorMedia;
    }

    public void setAuthorMedia(Media authorMedia) {
        this.authorMedia = authorMedia;
    }

    public boolean isRead() {
        return read;
    }

    public void setRead(boolean read) {
        this.read = read;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getApiLink() {
        return apiLink;
    }

    public void setApiLink(String apiLink) {
        this.apiLink = apiLink;
    }

    public int getRDVPosition() {
        return RDVPosition;
    }

    public void setRDVPosition(int RDVPosition) {
        this.RDVPosition = RDVPosition;
    }

    public int getViewsCount() {
        return viewsCount;
    }

    public void setViewsCount(int viewsCount) {
        this.viewsCount = viewsCount;
    }

    public boolean isProtectedArticle() {
        return protectedArticle;
    }

    public void setProtectedArticle(boolean protectedArticle) {
        this.protectedArticle = protectedArticle;
    }
}
