package requestlab.com.latribune.data;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

import java.util.Date;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by lchazal on 25/05/16.
 */
public class Depeche extends RealmObject {

    @SerializedName("id")
	@JsonProperty("id")
    @PrimaryKey
    private int     id;

    @SerializedName("title")
	@JsonProperty("title")
    private String  title;

    @SerializedName("body_html")
	@JsonProperty("body_html")
    private String  bodyHtml;

    @SerializedName("body_text")
	@JsonProperty("body_text")
    private String  bodyText;

    @SerializedName("media")
	@JsonProperty("media")
    private Media   media;

    @SerializedName("media_legend")
	@JsonProperty("media_legend")
    private String  mediaLegend;

    @SerializedName("signature")
	@JsonProperty("signature")
    private String  signature;

    @SerializedName("publication_date")
	@JsonProperty("publication_date")
    private Date  publicationDate;

    @SerializedName("url")
	@JsonProperty("url")
    private String  url;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBodyHtml() {
        return bodyHtml;
    }

    public void setBodyHtml(String bodyHtml) {
        this.bodyHtml = bodyHtml;
    }

    public String getBodyText() {
        return bodyText;
    }

    public void setBodyText(String bodyText) {
        this.bodyText = bodyText;
    }

    public Media getMedia() {
        return media;
    }

    public void setMedia(Media media) {
        this.media = media;
    }

    public String getMediaLegend() {
        return mediaLegend;
    }

    public void setMediaLegend(String mediaLegend) {
        this.mediaLegend = mediaLegend;
    }

    public String getSignature() {
        return signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }

    public Date getPublicationDate() {
        return publicationDate;
    }

    public void setPublicationDate(Date publicationDate) {
        this.publicationDate = publicationDate;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
