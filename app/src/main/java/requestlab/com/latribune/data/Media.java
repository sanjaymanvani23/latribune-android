package requestlab.com.latribune.data;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by lchazal on 01/04/16.
 */
public class Media extends RealmObject {

    @SerializedName("id")
	@JsonProperty("id")
    @PrimaryKey
    private int id;

    @SerializedName("title")
	@JsonProperty("title")
    private String title;

    @SerializedName("formats")
	@JsonProperty("formats")
    private MediaFormats    formats;

    @SerializedName("url")
	@JsonProperty("url")
    private String url;

    @SerializedName("media_type")
	@JsonProperty("media_type")
    private String mediaType;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public MediaFormats getFormats() {
        return formats;
    }

    public void setFormats(MediaFormats formats) {
        this.formats = formats;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getMediaType() {
        return mediaType;
    }

    public void setMediaType(String mediaType) {
        this.mediaType = mediaType;
    }
}
