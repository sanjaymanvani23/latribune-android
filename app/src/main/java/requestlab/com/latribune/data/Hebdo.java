package requestlab.com.latribune.data;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

import java.util.Date;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by lchazal on 27/04/16.
 */
public class Hebdo extends RealmObject {

    @PrimaryKey
    private int key;

    @SerializedName("id")
	@JsonProperty("id")
    private int id;

    @SerializedName("title")
	@JsonProperty("title")
    private String title;

    @SerializedName("publication_date")
	@JsonProperty("publication_date")
    private Date publicationDate;

    @SerializedName("cover_url")
	@JsonProperty("cover_url")
    private String coverUrl;

    public int getKey() {
        return key;
    }

    public void setKey(int key) {
        this.key = key;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Date getPublicationDate() {
        return publicationDate;
    }

    public void setPublicationDate(Date publicationDate) {
        this.publicationDate = publicationDate;
    }

    public String getCoverUrl() {
        return coverUrl;
    }

    public void setCoverUrl(String coverUrl) {
        this.coverUrl = coverUrl;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
