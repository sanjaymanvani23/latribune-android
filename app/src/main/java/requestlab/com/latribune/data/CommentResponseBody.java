package requestlab.com.latribune.data;

/**
 * Created by lchazal on 29/06/16.
 */
public class CommentResponseBody {

    public String   api_time;
    public String   api_key;
    public String   api_signature;
    public String   content;
    public String   pseudo;
    public String   email;

    public String getApi_time() {
        return api_time;
    }

    public void setApi_time(String api_time) {
        this.api_time = api_time;
    }

    public String getApi_key() {
        return api_key;
    }

    public void setApi_key(String api_key) {
        this.api_key = api_key;
    }

    public String getApi_signature() {
        return api_signature;
    }

    public void setApi_signature(String api_signature) {
        this.api_signature = api_signature;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getPseudo() {
        return pseudo;
    }

    public void setPseudo(String pseudo) {
        this.pseudo = pseudo;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
