package requestlab.com.latribune.views.imageviewtouch;

public interface IDisposable {

	void dispose();
}
