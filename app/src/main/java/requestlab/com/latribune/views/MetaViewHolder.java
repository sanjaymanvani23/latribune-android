package requestlab.com.latribune.views;


import android.util.Log;
import android.view.View;

import androidx.recyclerview.widget.RecyclerView;

import java.lang.reflect.Field;

/**
 * Created by lchazal on 28/04/16.
 */
public class MetaViewHolder extends RecyclerView.ViewHolder {

    public MetaViewHolder(View itemView, int viewType) {
        super(itemView);

        setItemViewType(viewType);
    }

    public void setItemViewType(int viewType) {
        try {
            Field privateStringField =
                    RecyclerView.ViewHolder.class.getDeclaredField("mItemViewType");
            privateStringField.setAccessible(true);

            privateStringField.set(this, viewType);

//            String fieldValue = (String) privateStringField.get(privateObject);
            Log.d("MetaViewHolder", "Succesfully set field mItemViewType");

        } catch (NoSuchFieldException e) {
            Log.e("MetaViewHolder", "field mItemViewType not found");
        } catch (IllegalAccessException e) {
            Log.e("MetaViewHolder", "field mItemViewType fucked");
        }
    }
}
