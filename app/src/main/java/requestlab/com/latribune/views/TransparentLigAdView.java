package requestlab.com.latribune.views;

import android.content.Context;
import android.graphics.Color;
import android.util.AttributeSet;

import com.ligatus.android.adframework.LigAdView;

/**
 * Created by lchazal on 08/06/16.
 */
public class TransparentLigAdView extends LigAdView {
    public TransparentLigAdView(Context aContext, AttributeSet aAttributeSet) {
        super(aContext, aAttributeSet);

        setBackgroundColor(Color.TRANSPARENT);

    }
}
