package requestlab.com.latribune.views.fontviews;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

import androidx.appcompat.widget.AppCompatTextView;

import requestlab.com.latribune.R;

/**
 * Created by lchazal on 23/09/15.
 *
 * Custom TextView adding a font name as parameter
 */
public class FontTextView extends AppCompatTextView {

    public FontTextView(Context context, AttributeSet attrs) {
        super(context, attrs);

        if (isInEditMode()) return;

        Typeface typeface = createCustomTypeface(context, attrs);

        if (typeface != null) {
            setTypeface(createCustomTypeface(context, attrs));
        }
    }

    private Typeface createCustomTypeface(Context context, AttributeSet attrs) {
        TypedArray styledAttrs = context.obtainStyledAttributes(attrs, R.styleable.FontTextView);
        String fontName = styledAttrs.getString(R.styleable.FontTextView_customTypeface);
        styledAttrs.recycle();

        if (fontName != null) {
            return Typeface.createFromAsset(context.getAssets(), fontName);
        }
        return null;
    }
}
