package requestlab.com.latribune.views;


import android.content.Context;

import android.util.Log;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

/**
 * Created by Léo on 19/12/2015.
 *
 * Custom layout manager used to passively catch IOOBException caused by a bug with some dataset
 * notification methods of the RecyclerView adapters.
 * Any method throwing this exception should be avoided if possible.
 */
public class WrapContentLinearLayoutManager extends LinearLayoutManager {

    public WrapContentLinearLayoutManager(Context context, int orientation, boolean reverseLayout) {
        super(context, orientation, reverseLayout);
    }

    @Override
    public void onLayoutChildren(RecyclerView.Recycler recycler, RecyclerView.State state) {
        try {
            super.onLayoutChildren(recycler, state);
        } catch (IndexOutOfBoundsException e) {
            Log.e("WCLLManager", "Met an IOOBE in RecyclerView");
        }
    }
}
