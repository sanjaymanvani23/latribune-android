package requestlab.com.latribune.views.fontviews;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.Button;

import requestlab.com.latribune.R;

/**
 * Created by lchazal on 21/12/15.
 */
public class FontButton extends Button{

    public FontButton(Context context, AttributeSet attrs) {
        super(context, attrs);

        Typeface typeface = createCustomTypeface(context, attrs);

        if (typeface != null) {
            setTypeface(createCustomTypeface(context, attrs));
        }
    }

    private Typeface createCustomTypeface(Context context, AttributeSet attrs) {
        TypedArray styledAttrs = context.obtainStyledAttributes(attrs, R.styleable.FontTextView);
        String fontName = styledAttrs.getString(R.styleable.FontTextView_customTypeface);
        styledAttrs.recycle();

        if (fontName != null) {
            Typeface typeface = Typeface.createFromAsset(context.getAssets(), fontName);
            return typeface;
        }
        return null;
    }
}
