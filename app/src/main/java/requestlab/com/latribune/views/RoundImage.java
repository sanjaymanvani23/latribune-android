package requestlab.com.latribune.views;

import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.PixelFormat;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Shader;
import android.graphics.drawable.Drawable;

/**
 * Created by lchazal on 08/09/15.
 *
 */
public class RoundImage extends Drawable {

    private final Bitmap    _bmp;
    private final Paint     _paint;
    private final RectF     _rect;
    private final int       _height;
    private final int       _width;

    public RoundImage(Bitmap bitmap) {
        this._bmp = bitmap;
        this._paint = new Paint();
        this._rect = new RectF();
        this._paint.setAntiAlias(true);
        this._paint.setShader(new BitmapShader(bitmap, Shader.TileMode.CLAMP, Shader.TileMode.CLAMP));
        this._height = this._bmp.getHeight();
        this._width = this._bmp.getWidth();
    }

    @Override
    public void draw(Canvas canvas) {
        canvas.drawOval(this._rect, this._paint);
    }

    @Override
    protected void onBoundsChange(Rect bounds) {
        super.onBoundsChange(bounds);
        this._rect.set(bounds);
    }

    @Override
    public void setAlpha(int alpha) {
        if (this._paint.getAlpha() != alpha) {
            this._paint.setAlpha(alpha);
            invalidateSelf();
        }
    }

    @Override
    public void setColorFilter(ColorFilter colorFilter) {
        this._paint.setColorFilter(colorFilter);
    }

    @Override
    public int getOpacity() {
        return PixelFormat.TRANSLUCENT;
    }

    @Override
    public int getIntrinsicWidth() {
        return this._width;
    }

    @Override
    public int getIntrinsicHeight() {
        return this._height;
    }

    public void setAntiAlias(boolean aa) {
        this._paint.setAntiAlias(aa);
        invalidateSelf();
    }

    @Override
    public void setFilterBitmap(boolean filter) {
        this._paint.setFilterBitmap(filter);
        invalidateSelf();
    }

    public Bitmap getBitmap() {
        return this._bmp;
    }
}
