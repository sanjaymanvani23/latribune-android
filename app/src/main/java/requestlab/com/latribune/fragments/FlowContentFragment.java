package requestlab.com.latribune.fragments;

import android.graphics.Color;
import android.os.Bundle;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.viewpager.widget.ViewPager;

import requestlab.com.latribune.R;
import requestlab.com.latribune.adapters.HomePagerAdapter;
import requestlab.com.latribune.tracking.TrackerManager;
import requestlab.com.latribune.views.slidingtablayout.SlidingTabLayout;

/**
 * Created by lchazal on 19/04/16.
 */
public class FlowContentFragment extends AContentFragment implements ViewPager.OnPageChangeListener {

    private HomePagerAdapter pagerAdapter;
    private ViewPager viewPager;
    private SlidingTabLayout tabLayout;

    private String[]    titles;

    protected int currentPosition = -1;
    protected int newPosition = 1;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.content_home, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        setupAdapter();
        setupViewPager(view);
        setupTabLayout(view);

//        drawerLayout.addDrawerListener(new DrawerLayout.SimpleDrawerListener() {
//            @Override
//            public void onDrawerClosed(View drawerView) {
//                super.onDrawerClosed(drawerView);
//
//                updatePagerPosition();
//            }
//        });
        updatePagerPosition();

        Log.d("FLOW", "drawer item selected : " + newPosition);



        // C'est cool, ça marche
//        try {
//            ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Yolo");
//
//        } catch (NullPointerException e) {
//            Log.e("HOME_PAGER_F", e.getMessage());
//        }
    }

    private void updatePagerPosition() {
        Log.d("MENU", "updating position from " + currentPosition + " to " + newPosition);
        if (currentPosition == newPosition) return;

        if (viewPager != null) {
            viewPager.setCurrentItem(newPosition);
            currentPosition = newPosition;
        }
        else         Log.d("FLOW", "but pager null ... : " + newPosition);
    }

    private void setupAdapter() {
        pagerAdapter = new HomePagerAdapter(getChildFragmentManager(), getContext());
    }

    private void setupViewPager(View v) {
        viewPager = (ViewPager) v.findViewById(R.id.pager);

        viewPager.setAdapter(pagerAdapter);
        viewPager.setOffscreenPageLimit(1);
        viewPager.addOnPageChangeListener(this);
    }

    private void setupTabLayout(View v) {
        tabLayout = (SlidingTabLayout) v.findViewById(R.id.sliding_tab_layout);
        tabLayout.setCustomTabView(R.layout.tab_home, R.id.tab_text);
        tabLayout.setCustomTabColorizer(new SlidingTabLayout.TabColorizer() {
            @Override
            public int getIndicatorColor(int position) {
                return Color.WHITE;
            }
        });
        tabLayout.setDistributeEvenly(false);
        tabLayout.setViewPager(viewPager);
    }

    @Override
    public void onDrawerItemSelected(int position) {
        Log.d("MENU", "drawer item selected : " + position);
        newPosition = position;
        updatePagerPosition();
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
    }

    @Override
    public void onPageSelected(int position) {
        currentPosition = position;
        TrackerManager.getInstance().tagScreen(pagerAdapter.getPageTitle(position).toString(), getString(R.string.tracking_title_main));
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }
}
