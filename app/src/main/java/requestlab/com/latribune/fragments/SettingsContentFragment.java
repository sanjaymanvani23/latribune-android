package requestlab.com.latribune.fragments;

import android.content.Intent;
import android.os.Bundle;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;

import requestlab.com.latribune.R;
import requestlab.com.latribune.activities.FavoritesActivity;
import requestlab.com.latribune.activities.LegalActivity;
import requestlab.com.latribune.activities.SendReportActivity;
import requestlab.com.latribune.activities.TextSizeActivity;
import requestlab.com.latribune.tracking.TrackerManager;
import requestlab.com.latribune.utils.AnimationUtils;

/**
 * Created by lchazal on 19/04/16.
 */
public class SettingsContentFragment extends AContentFragment implements View.OnClickListener {

    @Override
    public void onDrawerItemSelected(int position) {

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.content_settings, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        view.findViewById(R.id.settings_text_size).setOnClickListener(this);
        view.findViewById(R.id.settings_suggestion).setOnClickListener(this);
        view.findViewById(R.id.settings_support).setOnClickListener(this);
        view.findViewById(R.id.settings_legal).setOnClickListener(this);
    }

    @Override
    public void onResume() {
        super.onResume();

        TrackerManager.getInstance().tagScreen(getString(R.string.tracking_settings_chapter), getString(R.string.tracking_title_main));
    }

    public void onTextSizeButtonClicked() {
        startActivity(new Intent(getActivity(), TextSizeActivity.class));
        AnimationUtils.animateIn(getActivity());
    }

    public void onSuggestionButtonClicked() {
        Intent intent = new Intent(getActivity(), SendReportActivity.class);
        intent.putExtra("title", true);
        startActivity(intent);

        AnimationUtils.animateIn(getActivity());
    }

    public void onTechnicalProblemButtonClicked() {
        Intent intent = new Intent(getActivity(), SendReportActivity.class);
        intent.putExtra("title", false);
        startActivity(intent);
        AnimationUtils.animateIn(getActivity());
    }

    public void onLegalMentionsButtonClicked() {
        startActivity(new Intent(getActivity(), LegalActivity.class));
        AnimationUtils.animateIn(getActivity());
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.settings_text_size:
                onTextSizeButtonClicked();
                break;
            case R.id.settings_suggestion:
                onSuggestionButtonClicked();
                break;
            case R.id.settings_support:
                onTechnicalProblemButtonClicked();
                break;
            case R.id.settings_legal:
                onLegalMentionsButtonClicked();
                break;
        }
    }
}
