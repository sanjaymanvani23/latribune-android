package requestlab.com.latribune.fragments;

import android.os.Bundle;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import java.util.List;

import requestlab.com.latribune.R;
import requestlab.com.latribune.adapters.CategoryAdapter;
import requestlab.com.latribune.api.TribuneAPI;
import requestlab.com.latribune.api.listeners.APICallbackListener;
import requestlab.com.latribune.cache.TribuneCache;
import requestlab.com.latribune.data.Article;
import requestlab.com.latribune.data.Update;
import requestlab.com.latribune.decorators.CategoryDecorator;
import requestlab.com.latribune.views.WrapContentLinearLayoutManager;
import retrofit2.Call;

/**
 * Created by lchazal on 01/04/16.
 *
 */
public class CategoryFragment extends Fragment {

    public static RecyclerView.RecycledViewPool viewPool = new RecyclerView.RecycledViewPool();

    public static final String KEY_CATEGORY_ID = "categoryidkey";

    protected CategoryAdapter                 adapter;
    protected WrapContentLinearLayoutManager    linearLayoutManager;
    protected SwipeRefreshLayout swipeRefreshLayout;
    protected View    refreshView;
    protected View    loadingView;

    protected int categoryId = -1;

    protected Update    currentUpdate = null;
    protected Update    newUpdate = null;

    protected Call call;

    private static final int MAX_RETRY_COUNT = 3;

    private int updateCountdown = 0;
    private int retrieveCountdown = 0;

    private boolean isLive = false;
    private boolean firstLoad = false;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        firstLoad = true;

        retrieveCategoryId();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_article, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        setupAdapter();
        setupLayoutManager();
        setupRecyclerView(view);
        setupSwipeRefreshLayout(view);
        setupRefreshView(view);
        loadingView = view.findViewById(R.id.empty_view);
    }

    @Override
    public void onStart() {
        super.onStart();

        if (!firstLoad) return;

        Log.d("MYLIFE", "start");
        Log.d("ARTICLE_FETCH " + categoryId, "Resuming " + categoryId);

        isLive = true;
        updateCountdown = 0;
        retrieveCountdown = 0;

        retrieveArticlesFromCache();
        retrieveUpdateStatusFromCache();
        retrieveUpdateStatusFromAPI();
    }

    private void retrieveUpdateStatusFromAPI() {
        Log.d("ARTICLE_FETCH " + categoryId, "Fetching update from api");

        TribuneAPI.getInstance().getLastCategoryUpdate(categoryId, new APICallbackListener<Update>() {
            @Override
            public void onResponseSuccess(Update response) {
                firstLoad = false;
                onUpdateReceivedFromAPI(response);
            }

            @Override
            public void onResponseFailure(String message) {
                onForceUpdateReceivedFromAPI();
                onUpdateCallFailure();
            }

            @Override
            public void onFailure(String message) {
                onForceUpdateReceivedFromAPI();
                onUpdateCallFailure();
            }
        });
    }

    private void onForceUpdateReceivedFromAPI() {
        retrieveArticlesFromAPI();
    }

    private void onUpdateReceivedFromAPI(Update update) {
        if (currentUpdate != null && !currentUpdate.isValid()) currentUpdate = null;

        Log.d("ARTICLE_FETCH " + categoryId, "Current update : " + (currentUpdate == null ? "0" : currentUpdate.getTimestamp()) + " New update : " + update.getTimestamp());

        if (currentUpdate == null || update.getTimestamp() != currentUpdate.getTimestamp()) {
//        if (currentUpdate == null) {
            Log.d("ARTICLE_FETCH " + categoryId, "First Update retrieval");
            newUpdate = update;
            retrieveArticlesFromAPI();
//        } else if (update.getTimestamp() != currentUpdate.getTimestamp()) {
//            Log.d("ARTICLE_FETCH " + categoryId, "API update doesn't match ours");
//            newUpdate = update;
//            showRefreshButton();
        } else {
            Log.d("ARTICLE_FETCH " + categoryId, "API update is the same as ours");
            swipeRefreshLayout.setRefreshing(false);
        }
    }

    private void onRetrieveCallFailed() {
        if (!isLive) return;
        if (retrieveCountdown < MAX_RETRY_COUNT) {
            retrieveCountdown++;
            Log.e("ARTICLE_FETCH " + categoryId, "Retrieve retry n° " + retrieveCountdown);
            retrieveArticlesFromAPI();
        } else {
            Log.e("ARTICLE_FETCH " + categoryId, "failure call Retrieve");
            swipeRefreshLayout.setRefreshing(false);
        }
    }

    private void retrieveArticlesFromCache() {
        Log.d("ARTICLE_FETCH " + categoryId, "Fetching articles from cache");

        List<Article> articles = TribuneCache.getInstance().getArticlesByCategory(categoryId);

        if (articles != null) {
            Log.d("ARTICLE_FETCH " + categoryId, "Fetched " + articles.size() + " articles from cache");
            adapter.addItemList(articles);
            linearLayoutManager.scrollToPosition(0);
            updateLoadingViewVisibility();
        }
    }

    private void retrieveUpdateStatusFromCache() {
        Log.d("ARTICLE_FETCH " + categoryId, "Fetching update from cache");

        currentUpdate = TribuneCache.getInstance().getUpdate(categoryId);
    }

    private void retrieveArticlesFromAPI() {
        Log.d("ARTICLE_FETCH " + categoryId, "Fetching articles from api");
        if (categoryId == -1) return;

        if (call != null) call.cancel();
        call = TribuneAPI.getInstance().getCategory(categoryId, new APICallbackListener<List<Article>>() {
            @Override
            public void onResponseSuccess(List<Article> response) {
                Log.d("ARTICLE_FETCH " + categoryId, "Fetched " + response.size() + " articles from API");
                swipeRefreshLayout.setRefreshing(false);
                onArticlesReceivedFromAPI(response);
            }

            @Override
            public void onResponseFailure(String message) {
                onRetrieveCallFailed();
            }

            @Override
            public void onFailure(String message) {
                onRetrieveCallFailed();
            }
        });
    }

//    private List<Article>    fakeArticles() {
//        List<Article> list = new ArrayList<>();
//
//        for (int i = iop; i < iop + 20; i++) {
//            Article a = new Article();
//            a.setId(i);
//            a.setTitle("Article " + i);
//            a.setPosition(i);
//            list.add(a);
//        }
//        iop++;
//        return list;
//    }
//
//    static int iop = 0;

    private void onArticlesReceivedFromAPI(List<Article> articles) {
//        articles = fakeArticles();

        Log.d("CategoryFragment", "Received from api : " + articles.size() + " article(s)");
        for (Article a :
                articles) {
            TribuneCache.getInstance().setArticleExtraData(a, categoryId);
        }

        adapter.clear();
        adapter.addItemList(articles);
        TribuneCache.getInstance().clearCategory(categoryId);
        TribuneCache.getInstance().addOrUpdateObjectList(articles);


        swipeRefreshLayout.setRefreshing(false);
        linearLayoutManager.scrollToPosition(0);
        updateLoadingViewVisibility();

        currentUpdate = newUpdate;
        if (currentUpdate != null) {
            Log.d("ARTICLE_FETCH " + categoryId, "Current update set to " + currentUpdate.getTimestamp());
            TribuneCache.getInstance().setUpdate(categoryId, currentUpdate);
        }
    }

    private void onUpdateCallFailure() {
        Log.e("ARTICLE_FETCH " + categoryId, "Update call failed");
        if (!isLive) return;
        if (updateCountdown < MAX_RETRY_COUNT) {
            updateCountdown++;
            Log.e("ARTICLE_FETCH " + categoryId, "Update retry n° " + updateCountdown);
            retrieveUpdateStatusFromAPI();
        } else {
            Log.e("ARTICLE_FETCH " + categoryId, "failure call Update");
            swipeRefreshLayout.setRefreshing(false);
        }
    }

    private void updateLoadingViewVisibility() {
        Log.d("ARTICLE_FETCH " + categoryId, "Setting loading visibility to " + (adapter.getItemCount() > 0 ? "GONE" : "VISIBLE"));
        loadingView.setVisibility(adapter.isEmpty() ? View.VISIBLE : View.GONE);
    }

    private void setupRefreshView(View view) {
        refreshView = view.findViewById(R.id.refresh_view);
        refreshView.setVisibility(View.GONE);
        refreshView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                hideRefreshButton();
                retrieveArticlesFromAPI();
            }
        });
    }

    protected void setupAdapter() {
        adapter = new CategoryAdapter(getContext());
    }

    protected void setupLayoutManager() {
        linearLayoutManager = new WrapContentLinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
    }

    protected void setupRecyclerView(View view) {
        RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setHasFixedSize(true);
        recyclerView.setRecycledViewPool(viewPool);
        recyclerView.addItemDecoration(createDecorator());
        recyclerView.setAdapter(adapter);
    }

    protected CategoryDecorator createDecorator() {
        return new CategoryDecorator(adapter.getStyleSet(), (int) getContext().getResources().getDimension(R.dimen.article_margin_default));
    }

    private void setupSwipeRefreshLayout(View view) {
        swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipeRefreshLayout);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
//                hideRefreshButton();
                retrieveUpdateStatusFromAPI();
                retrieveCountdown = 0;
                updateCountdown = 0;
            }
        });
    }

    private void    showRefreshButton() {
        if (refreshView == null || refreshView.getVisibility() == View.VISIBLE) return;
        Log.d("ARTICLE_FETCH " + categoryId, "SHOW REFRESH");

        Animation drawerAnimation = AnimationUtils.loadAnimation(getContext(), R.anim.in_from_top);

        refreshView.startAnimation(drawerAnimation);
        refreshView.setVisibility(View.VISIBLE);
    }

    private void    hideRefreshButton() {
        if (refreshView == null || refreshView.getVisibility() == View.GONE) return;
        Log.d("ARTICLE_FETCH " + categoryId, "HIDE REFRESH");

        Animation drawerAnimation = AnimationUtils.loadAnimation(getContext(), R.anim.out_to_top);

        refreshView.startAnimation(drawerAnimation);
        refreshView.setVisibility(View.GONE);
    }

    protected void retrieveCategoryId() {
        Bundle args = getArguments();
        if (args == null) return;

        categoryId = args.getInt(KEY_CATEGORY_ID, -1);
    }

    @Override
    public void onDestroy() {
        isLive = false;
        if (call != null) call.cancel();

        super.onDestroy();
    }
}
