package requestlab.com.latribune.fragments;

import android.content.Context;
import android.os.Bundle;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import java.util.List;

import requestlab.com.latribune.R;
import requestlab.com.latribune.adapters.LiveAdapter;
import requestlab.com.latribune.api.TribuneAPI;
import requestlab.com.latribune.api.listeners.APICallbackListener;
import requestlab.com.latribune.cache.TribuneCache;
import requestlab.com.latribune.data.Live;
import requestlab.com.latribune.data.Update;
import requestlab.com.latribune.views.WrapContentLinearLayoutManager;
import retrofit2.Call;

/**
 * Created by lchazal on 01/04/16.
 */
public class LiveFragment extends Fragment {

    private LiveAdapter                     adapter;
    private WrapContentLinearLayoutManager  linearLayoutManager;
    private SwipeRefreshLayout swipeRefreshLayout;
    protected View    refreshView;
    private View    loadingView;

    private int categoryId = 0;

    protected Update currentUpdate;
    protected Update newUpdate;

    private Call call;

    private static final int MAX_RETRY_COUNT = 3;

    private int updateCountdown = 0;
    private int retrieveCountdown = 0;

    private boolean isLive = false;
    private boolean firstLoad = false;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        firstLoad = true;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_article, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        setupAdapter();
        setupLayoutManager();
        setupRecyclerView(view);
        setupSwipeRefreshLayout(view);
        setupRefreshView(view);
        loadingView = view.findViewById(R.id.empty_view);
    }

    @Override
    public void onStart() {
        super.onStart();

        if (!firstLoad) return;

        Log.d("ARTICLE_FETCH LIVE", "Resuming LIVE");

        isLive = true;
        updateCountdown = 0;
        retrieveCountdown = 0;

        retrieveArticlesFromCache();
        retrieveUpdateStatusFromCache();
        retrieveUpdateStatusFromAPI();
    }

    private void retrieveUpdateStatusFromAPI() {
        Log.d("ARTICLE_FETCH LIVE", "Fetching update from api");

        TribuneAPI.getInstance().getLastLiveUpdate(new APICallbackListener<Update>() {
            @Override
            public void onResponseSuccess(Update response) {
                firstLoad = false;
                onUpdateReceivedFromAPI(response);
            }

            @Override
            public void onResponseFailure(String message) {
                onUpdateCallFailure();
            }

            @Override
            public void onFailure(String message) {
                onUpdateCallFailure();
            }
        });
    }

    private void onUpdateReceivedFromAPI(Update update) {
        Log.d("ARTICLE_FETCH LIVE", "Current update : " + (currentUpdate == null ? "0" : currentUpdate.getTimestamp()) + " New update : " + update.getTimestamp());

        if (currentUpdate == null || update.getTimestamp() != currentUpdate.getTimestamp()) {
            Log.d("ARTICLE_FETCH LIVE", "First Update retrieval");
            newUpdate = update;
            retrieveArticlesFromAPI();
//        } else if (update.getTimestamp() != currentUpdate.getTimestamp()) {
//            Log.d("ARTICLE_FETCH LIVE", "API update doesn't match ours");
//            newUpdate = update;
//            showRefreshButton();
        } else {
            Log.d("ARTICLE_FETCH LIVE", "API update is the same as ours");
            swipeRefreshLayout.setRefreshing(false);
        }
    }

    private void onUpdateCallFailure() {
        Log.e("ARTICLE_FETCH " + categoryId, "Update call failed");
        if (!isLive) return;
        if (updateCountdown < MAX_RETRY_COUNT) {
            updateCountdown++;
            Log.e("ARTICLE_FETCH " + categoryId, "Update retry n° " + updateCountdown);
            retrieveUpdateStatusFromAPI();
        } else {
            Log.e("ARTICLE_FETCH " + categoryId, "failure call Update");
            swipeRefreshLayout.setRefreshing(false);
        }
    }

    private void retrieveArticlesFromCache() {
        List<Live> articles = TribuneCache.getInstance().getObjectList(Live.class);

        if (articles != null) {
            adapter.addItemList(articles);
            linearLayoutManager.scrollToPosition(0);
            updateLoadingViewVisibility();
        }
    }

    private void retrieveUpdateStatusFromCache() {
        Log.d("ARTICLE_FETCH LIVE", "Fetching update from cache");

        currentUpdate = TribuneCache.getInstance().getUpdate(categoryId);
    }

    private void retrieveArticlesFromAPI() {
        if (call != null) call.cancel();
        call = TribuneAPI.getInstance().getLive(new APICallbackListener<List<Live>>() {
            @Override
            public void onResponseSuccess(List<Live> response) {
                Log.d("CATEGORY_FRAGMENT", "Success call for live");
                swipeRefreshLayout.setRefreshing(false);
                onArticlesReceivedFromAPI(response);
            }

            @Override
            public void onResponseFailure(String message) {
                Log.d("CATEGORY_FRAGMENT", "failure response for live");
                onRetrieveCallFailed();
            }

            @Override
            public void onFailure(String message) {
                Log.d("CATEGORY_FRAGMENT", "failure call for live");
                onRetrieveCallFailed();
            }
        });
    }

    private void onArticlesReceivedFromAPI(List<Live> lives) {
        Log.d("LIVE", "Handling result of " + lives.size() + "live(s)");
        TribuneCache.getInstance().addOrUpdateObjectList(lives);
        swipeRefreshLayout.setRefreshing(false);
        retrieveArticlesFromCache();
        currentUpdate = newUpdate;
        if (currentUpdate != null) {
            TribuneCache.getInstance().setUpdate(categoryId, currentUpdate);
        }
    }

    private void onRetrieveCallFailed() {
        if (!isLive) return;
        if (retrieveCountdown < MAX_RETRY_COUNT) {
            retrieveCountdown++;
            Log.e("ARTICLE_FETCH " + categoryId, "Retrieve retry n° " + retrieveCountdown);
            retrieveArticlesFromAPI();
        } else {
            Log.e("ARTICLE_FETCH " + categoryId, "failure call Retrieve");
            swipeRefreshLayout.setRefreshing(false);
        }
    }

    private void updateLoadingViewVisibility() {
        loadingView.setVisibility(adapter.getItemCount() > 0 ? View.GONE : View.VISIBLE);
    }

    private void setupRefreshView(View view) {
        refreshView = view.findViewById(R.id.refresh_view);
        refreshView.setVisibility(View.GONE);
        refreshView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                hideRefreshButton();
                retrieveArticlesFromAPI();
            }
        });
    }

    protected void setupAdapter() {
        adapter = new LiveAdapter(getContext());
    }

    protected void setupLayoutManager() {
        linearLayoutManager = new WrapContentLinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
    }

    protected void setupRecyclerView(View view) {
        RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(adapter);
    }

    private void setupSwipeRefreshLayout(View view) {
        swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipeRefreshLayout);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
//                hideRefreshButton();
                retrieveUpdateStatusFromAPI();
                retrieveCountdown = 0;
                updateCountdown = 0;
            }
        });
    }

    private void    showRefreshButton() {
        if (refreshView == null || refreshView.getVisibility() == View.VISIBLE) return;
        Log.d("ARTICLE_FETCH LIVE", "SHOW REFRESH");

        Context context = getContext();
        if (context == null) return;

        Animation drawerAnimation = AnimationUtils.loadAnimation(context, R.anim.in_from_top);

        refreshView.startAnimation(drawerAnimation);
        refreshView.setVisibility(View.VISIBLE);
    }

    private void    hideRefreshButton() {
        if (refreshView == null || refreshView.getVisibility() == View.GONE) return;
        Log.d("ARTICLE_FETCH LIVE", "HIDE REFRESH");

        Context context = getContext();
        if (context == null) return;

        Animation drawerAnimation = AnimationUtils.loadAnimation(context, R.anim.out_to_top);

        refreshView.startAnimation(drawerAnimation);
        refreshView.setVisibility(View.GONE);
    }

    @Override
    public void onDestroy() {
        isLive = false;
        if (call != null) call.cancel();

        super.onDestroy();
    }
}
