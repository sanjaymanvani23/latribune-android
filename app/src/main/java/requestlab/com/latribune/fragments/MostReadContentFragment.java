package requestlab.com.latribune.fragments;

import android.os.Bundle;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import java.util.List;

import requestlab.com.latribune.C.Constants;
import requestlab.com.latribune.R;
import requestlab.com.latribune.adapters.MostReadAdapter;
import requestlab.com.latribune.api.TribuneAPI;
import requestlab.com.latribune.api.listeners.APICallbackListener;
import requestlab.com.latribune.cache.TribuneCache;
import requestlab.com.latribune.data.Article;
import requestlab.com.latribune.decorators.CategoryDecorator;
import requestlab.com.latribune.tracking.TrackerManager;
import requestlab.com.latribune.views.WrapContentLinearLayoutManager;
import retrofit2.Call;

/**
 * Created by lchazal on 19/04/16.
 */
public class MostReadContentFragment extends AContentFragment {

    protected MostReadAdapter               adapter;
    private WrapContentLinearLayoutManager  linearLayoutManager;
    private SwipeRefreshLayout swipeRefreshLayout;

    private int categoryId = Constants.CATEGORY_ID_MOST_READ;

    private Call call;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.content_most_read, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        setupAdapter();
        setupLayoutManager();
        setupRecyclerView(view);
        setupSwipeRefreshLayout(view);
    }

    @Override
    public void onStart() {
        super.onStart();

        TrackerManager.getInstance().tagScreen(getString(R.string.tracking_most_read_chapter),
                getString(R.string.tracking_title_main));
        retrieveArticles();
    }

    protected void setupAdapter() {
        adapter = new MostReadAdapter(getContext());
    }

    protected void setupLayoutManager() {
        linearLayoutManager = new WrapContentLinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
    }

    protected void setupRecyclerView(View view) {
        RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setHasFixedSize(true);
        recyclerView.addItemDecoration(new CategoryDecorator(adapter.getStyleSet(), (int) getContext().getResources().getDimension(R.dimen.article_margin_default)));
//        recyclerView.setRecycledViewPool(viewPool);
        recyclerView.setAdapter(adapter);
    }

    private void setupSwipeRefreshLayout(View view) {
        swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipeRefreshLayout);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                retrieveArticles();
            }
        });
    }

    protected void retrieveArticles() {
        retrieveArticlesOffline();
        retrieveArticlesOnline();
    }

    private void retrieveArticlesOffline() {
        List<Article> articles = TribuneCache.getInstance().getArticlesByCategory(categoryId);

        if (articles == null) return;

        adapter.addItemList(articles);
        linearLayoutManager.scrollToPosition(0);
    }

    private void retrieveArticlesOnline() {
        if (call != null) call.cancel();
        call = TribuneAPI.getInstance().getMostRead(new APICallbackListener<List<Article>>() {
            @Override
            public void onResponseSuccess(List<Article> response) {
                Log.d("CATEGORY_FRAGMENT", "Success call for mostRead");
                for (Article a :
                        response) {
                    TribuneCache.getInstance().setArticleExtraData(a, categoryId);
                }
                TribuneCache.getInstance().removeArticlesNotIn(response, categoryId);
                TribuneCache.getInstance().addOrUpdateObjectList(response);
                swipeRefreshLayout.setRefreshing(false);
                retrieveArticlesOffline();
            }

            @Override
            public void onResponseFailure(String message) {
                Log.d("CATEGORY_FRAGMENT", "failure response for mostRead");
                swipeRefreshLayout.setRefreshing(false);
            }

            @Override
            public void onFailure(String message) {
                Log.d("CATEGORY_FRAGMENT", "failure call for mostRead");
                swipeRefreshLayout.setRefreshing(false);
            }
        });
    }

    @Override
    public void onDrawerItemSelected(int position) {

    }

    @Override
    public void onDestroy() {
        if (call != null) call.cancel();

        super.onDestroy();
    }
}
