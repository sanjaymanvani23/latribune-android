package requestlab.com.latribune.fragments;


import androidx.fragment.app.Fragment;

/**
 * Created by lchazal on 19/04/16.
 *
 * Base class for fragments display in the main activity
 */
public abstract class AContentFragment extends Fragment {

    public abstract void onDrawerItemSelected(int position);


}
