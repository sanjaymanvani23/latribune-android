package requestlab.com.latribune.fragments;

import android.animation.ObjectAnimator;
import android.graphics.Bitmap;
import android.os.Bundle;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.squareup.picasso.Target;

import org.w3c.dom.Text;

import requestlab.com.latribune.R;
import requestlab.com.latribune.utils.ImageManager;
import requestlab.com.latribune.views.ExpandableTextView;
import requestlab.com.latribune.views.imageviewtouch.ImageViewTouch;
import requestlab.com.latribune.views.imageviewtouch.ImageViewTouchBase;

/**
 * Created by lchazal on 25/04/16.
 */
public class GalleryFragment extends Fragment {

    private String  url;
    private String  legend;
//    private ImageView       imageView;
    private ImageViewTouch  imageViewTouch;
//    private ExpandableTextView legendTextView;
    private TextView legendTextView;
    private Target      mTarget;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_gallery, container, false);

        imageViewTouch = (ImageViewTouch) v.findViewById(R.id.image);
        imageViewTouch.setDisplayType(ImageViewTouchBase.DisplayType.FIT_TO_SCREEN);
        legendTextView = (TextView) v.findViewById(R.id.legend_text);
//        legendTextView = (ExpandableTextView) v.findViewById(R.id.legend_text);

        return v;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle payload = getArguments();
        if (payload != null) {
            url = payload.getString("imageurl");
            legend = payload.getString("imagelegend");
        }
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        legendTextView.setText(legend);
        final int collapsedMaxLines = 3;
        legendTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ObjectAnimator animation = ObjectAnimator.ofInt(legendTextView, "maxLines",
                        legendTextView.getMaxLines() == collapsedMaxLines? legendTextView.getLineCount() : collapsedMaxLines);
                animation.setDuration(200).start();
            }
        });
        loadImageFromUrl();
    }

    private void loadImageFromUrl() {
//        Log.d("GALLERY", "Loading image from url");
//        if (imageView == null || url == null || url.isEmpty()) return;

        try {
            ImageManager.getInstance().loadImageIntoView(url, imageViewTouch);
        } catch (NullPointerException e) {
            Log.e("GALLERY", e.getMessage());
        } catch (IllegalArgumentException ee) {
            Log.e("GALLERY", ee.getMessage());
        }

//        mTarget = new Target() {
//            @Override
//            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
////                mProgressBar.setVisibility(View.GONE);
////                mImageViewTouch.setVisibility(View.VISIBLE);
//                setImageBitmap(bitmap);
//            }
//
//            @Override
//            public void onBitmapFailed(Drawable errorDrawable) {
//
//            }
//
//            @Override
//            public void onPrepareLoad(Drawable placeHolderDrawable) {
//
//            }
//        };
//        ImageManager.loadImageIntoTarget(this,
//                url,
//                mTarget,
//                mWidth > MAX_SIZE ? MAX_SIZE : mWidth,
//                mHeight > MAX_SIZE ? MAX_SIZE : mHeight);
    }

    private void setImageBitmap(Bitmap bmp) {
        imageViewTouch.setImageBitmap(bmp);
    }
}
