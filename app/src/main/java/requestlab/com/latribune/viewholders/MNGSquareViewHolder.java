package requestlab.com.latribune.viewholders;

import android.content.Context;
import android.view.View;
import android.widget.RelativeLayout;

import com.mngads.MNGNativeObject;
import com.mngads.util.MNGFrame;

import requestlab.com.latribune.R;
import requestlab.com.latribune.data.Article;
import requestlab.com.latribune.utils.AdManager;

/**
 * Created by lchazal on 20/04/16.
 */
public class MNGSquareViewHolder extends AViewHolder {

    private View            adTitle;
    private RelativeLayout  squareBanner;

    private boolean     isLoaded = false;
    public MNGSquareViewHolder(View itemView) {
        super(itemView);

        adTitle = itemView.findViewById(R.id.mng_ad_title);
        squareBanner = (RelativeLayout) itemView.findViewById(R.id.square_banner);
    }

    @Override
    public void bindViewHolder(Context context, Article article, int position) {
        if (isLoaded) return;

        if (squareBanner != null) {
            AdManager.buildMNGAdLoadingTask(context, createAdListener(), AdManager.AdType.SQUARE).execute();

            isLoaded = true;
        }
    }

    private AdManager.IAdLoadedListener createAdListener() {
        return new AdManager.IAdLoadedListener() {
            @Override
            public void onAdLoaded(View view, int i) {
                squareBanner.addView(view);
                adTitle.setVisibility(View.VISIBLE);
            }

            @Override
            public void onNativeAdLoaded(MNGNativeObject object) {}

            @Override
            public void onAdFailed(Exception e) {

            }

            @Override
            public void onAdChanged(MNGFrame mngFrame) {

            }
        };
    }
}
