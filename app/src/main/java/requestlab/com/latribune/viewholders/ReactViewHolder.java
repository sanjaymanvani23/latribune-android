package requestlab.com.latribune.viewholders;

import android.content.Context;
import android.content.Intent;

import android.view.View;
import android.widget.TextView;

import androidx.appcompat.widget.AppCompatButton;
import androidx.core.content.ContextCompat;

import org.ocpsoft.prettytime.PrettyTime;

import java.util.Calendar;
import java.util.Locale;

import requestlab.com.latribune.R;
import requestlab.com.latribune.activities.CommentsActivity;
import requestlab.com.latribune.activities.ReactActivity;
import requestlab.com.latribune.data.Article;
import requestlab.com.latribune.data.Comment;
import requestlab.com.latribune.utils.CompatibilityUtils;


/**
 * Created by lchazal on 21/04/16.
 */
public class ReactViewHolder extends AViewHolder {

    private View        view;

    private TextView    name;
    private TextView    time;
    private TextView    comment;

    private AppCompatButton reactBtn;
    private AppCompatButton viewCommentBtn;

    public ReactViewHolder(View itemView) {
        super(itemView);

        view = itemView.findViewById(R.id.view);

        name = (TextView) itemView.findViewById(R.id.name);
        time = (TextView) itemView.findViewById(R.id.time);
        comment = (TextView) itemView.findViewById(R.id.comment);
        reactBtn = (AppCompatButton) itemView.findViewById(R.id.reactBtn);
        viewCommentBtn = (AppCompatButton) itemView.findViewById(R.id.commentBtn);
    }

    public ReactViewHolder(View itemView, Context context) {
        this(itemView);

        if (context != null) CompatibilityUtils.setButtonTint(reactBtn, ContextCompat.getColor(context, R.color.send_button_background));
        if (context != null) CompatibilityUtils.setButtonTint(viewCommentBtn, ContextCompat.getColor(context, R.color.separator));
    }

    @Override
    public void bindViewHolder(Context context, Article article, int position) {
        if (article.getComments() == null || article.getComments().getLastComment() == null || article.getComments().getCount() == 0) {
            view.setVisibility(View.GONE);
            return;
        }
        view.setVisibility(View.VISIBLE);
        Comment lastComment = article.getComments().getLastComment();

        name.setText(lastComment.getPseudo());

        Calendar calendar = Calendar.getInstance();
        PrettyTime p = new PrettyTime(Locale.getDefault());
        calendar.setTime(lastComment.getDate());
        time.setText(p.format(calendar));

        comment.setText(lastComment.getContent());

        reactBtn.setOnClickListener(createReactClickListener(context, article.getId(), lastComment.getPseudo(), article.getTitle()));

        int commentCount = article.getComments().getCount();

        viewCommentBtn.setText(context.getResources().getQuantityString(R.plurals.comment_quantity, commentCount, commentCount));
        viewCommentBtn.setOnClickListener(createCommentClickListener(context, article.getId(), article.getComments().getCount()));
    }

    private View.OnClickListener createReactClickListener(final Context context, final int id, final String pseudo, final String title) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, ReactActivity.class);
                intent.putExtra(ReactActivity.REACT_ARTICLE_ID, id);
                intent.putExtra(ReactActivity.REACT_ARTICLE_TITLE, title);
                intent.putExtra(ReactActivity.REACT_CALLER, ReactViewHolder.class.getSimpleName());
                context.startActivity(intent);
            }
        };
    }

    private View.OnClickListener createCommentClickListener(final Context context, final int id, final int count) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, CommentsActivity.class);

                intent.putExtra("articleid", id);
                intent.putExtra("articlecount", count);
                context.startActivity(intent);
            }
        };
    }
}
