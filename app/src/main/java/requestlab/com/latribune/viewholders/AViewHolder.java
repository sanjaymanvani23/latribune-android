package requestlab.com.latribune.viewholders;

import android.content.Context;

import android.view.View;

import androidx.recyclerview.widget.RecyclerView;

import requestlab.com.latribune.cache.TribuneCache;
import requestlab.com.latribune.data.Article;

/**
 * Created by lchazal on 20/04/16.
 */
public abstract class AViewHolder extends RecyclerView.ViewHolder {

    public AViewHolder(View itemView) {
        super(itemView);
    }

    public abstract void bindViewHolder(Context context, Article article, int position);

    public void bindViewHolder(Context context, Article ... article) {
        bindViewHolder(context, article == null ? null : article[0], 0);
    }
//
    public void bindViewHolder(Context context, int articleId) {
        Article article = TribuneCache.getInstance().getObject(Article.class, articleId);

        bindViewHolder(context, article, 0);
    }
}
