package requestlab.com.latribune.viewholders;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;

import android.text.Html;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.core.content.ContextCompat;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import requestlab.com.latribune.R;
import requestlab.com.latribune.activities.BetterArticleDetailsActivity;
import requestlab.com.latribune.activities.GalleryActivity;
import requestlab.com.latribune.api.listeners.APICallbackListener;
import requestlab.com.latribune.cache.TribuneCache;
import requestlab.com.latribune.data.Article;
import requestlab.com.latribune.data.Media;
import requestlab.com.latribune.utils.CSSManager;
import requestlab.com.latribune.utils.CompatibilityUtils;
import requestlab.com.latribune.utils.FontPreferencesManager;
import requestlab.com.latribune.utils.ImageManager;
import requestlab.com.latribune.utils.NetworkUtils;
import requestlab.com.latribune.views.fontviews.FontTextView;

/**
 * Created by lchazal on 14/04/16.
 */
public class ArticleDetailsViewHolder extends AViewHolder {

    private Context context;

    private ImageView       image;
    private ImageView       videoIcon;
    private ImageView       diaporamaIcon;
    private FontTextView    title;
    private TextView        hat;
    private TextView        date;
    private Button          diaporamaButton;
    private View            separator;
    private WebView         content;

    private View            authorBloc;
    private ImageView       authorImage;
    private TextView        authorTwitter;
    private FontTextView    authorName;

    private View relatedBox;
    private View articleOne;
    private ImageView articleOneImage;
    private FontTextView articleOneText;
    private View articleTwo;
    private ImageView articleTwoImage;
    private FontTextView articleTwoText;
    private View articleThree;
    private ImageView articleThreeImage;
    private FontTextView articleThreeText;

    public ArticleDetailsViewHolder(View itemView) {
        super(itemView);

        image = (ImageView) itemView.findViewById(R.id.article_image);
        videoIcon = (ImageView) itemView.findViewById(R.id.article_video);
        diaporamaIcon = (ImageView) itemView.findViewById(R.id.article_diapo);
        title = (FontTextView) itemView.findViewById(R.id.article_title);
        hat = (TextView) itemView.findViewById(R.id.hat);
        date = (TextView) itemView.findViewById(R.id.article_date);
        diaporamaButton = (Button) itemView.findViewById(R.id.diaporama_button);
        separator = itemView.findViewById(R.id.separator);
        content = (WebView) itemView.findViewById(R.id.article_content);

        authorBloc = itemView.findViewById(R.id.author_bloc);
        authorImage = (ImageView) authorBloc.findViewById(R.id.author_image);
        authorName = (FontTextView) authorBloc.findViewById(R.id.author_name);
        authorTwitter = (TextView) authorBloc.findViewById(R.id.author_twitter);

        relatedBox = itemView.findViewById(R.id.related_box);
        if (relatedBox != null) {
            articleOne = relatedBox.findViewById(R.id.article_one);
            articleOneImage = (ImageView) articleOne.findViewById(R.id.image);
            articleOneText = (FontTextView) articleOne.findViewById(R.id.text);
            articleTwo = relatedBox.findViewById(R.id.article_two);
            articleTwoImage = (ImageView) articleTwo.findViewById(R.id.image);
            articleTwoText = (FontTextView) articleTwo.findViewById(R.id.text);
            articleThree = relatedBox.findViewById(R.id.article_three);
            articleThreeImage = (ImageView) articleThree.findViewById(R.id.image);
            articleThreeText = (FontTextView) articleThree.findViewById(R.id.text);
        }
    }

    public void onResume() {
        Log.d("THREADPAUSE", "Resuming VH");
        if (content != null) {
            content.onResume();
        }
    }

    public void onPause() {
        Log.d("THREADPAUSE", "Pausing VH");
        if (content != null) {
            content.onPause();
        }
    }

    public void onDestroy() {
        Log.d("THREADPAUSE", "destorying VH");
        if (content != null) {
            content.loadUrl("");
            content.destroy();
        }
    }

    @Override
    public void bindViewHolder(final Context context, final Article article, int position) {

        this.context = context;

        try {
            if (image != null) {
                String url = getBestUrl(article.getMedia(), true);
                if (url != null && !url.isEmpty()) {
                    ImageManager.getInstance().fitImageIntoView(url, image);
                }
                image.setOnClickListener(createImageClickListener(article));
                videoIcon.setVisibility(article.getMedia().getMediaType().equals("video") ? View.VISIBLE : View.GONE);
                diaporamaIcon.setVisibility(isDiaporama(article) ? View.VISIBLE : View.GONE);
            }

            hat.setText(article.getHeader());
            hat.setTextSize(TypedValue.COMPLEX_UNIT_SP, FontPreferencesManager.getInstance().getpSize());
            title.setText(article.getTitle());


            Calendar c = Calendar.getInstance();
            c.setTime(article.getPublicationDate());

            SimpleDateFormat format = new SimpleDateFormat("dd'/'MM'/'yyyy 'à' kk'h'mm", Locale.getDefault());

            if (isDiaporama(article)) {
                date.setText(Html.fromHtml(context.getString(R.string.article_date,
                        format.format(c.getTime()))));
                loadHTMLContentIntoWebView(null, false);
                loadAuthorBlocData(null, false);
                diaporamaButton.setVisibility(View.VISIBLE);
                CompatibilityUtils.setButtonTint(diaporamaButton, ContextCompat.getColor(context, R.color.send_button_background));
                diaporamaButton.setOnClickListener(createImageClickListener(article));
                separator.setVisibility(View.GONE);
            } else {
                date.setText(Html.fromHtml(context.getString(R.string.article_date_time,
                        format.format(c.getTime()),
                        article.getReadingTime())));
                loadHTMLContentIntoWebView(article, true);
                loadAuthorBlocData(article, true);
                separator.setVisibility(View.VISIBLE);
                diaporamaButton.setVisibility(View.GONE);
            }

        } catch (NullPointerException e) {
            Log.d("DETAIL_VIEW_HOLDER", "Error binding :" + e.getMessage());
        }
    }

    private void    loadHTMLContentIntoWebView(Article article, boolean isVisible) {
        content.setVisibility(isVisible ? View.VISIBLE : View.GONE);
        if (!isVisible) return;

        content.setWebViewClient(new WebViewClient(){ // Ensures that the url is started outside the webview
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url){
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                context.startActivity(intent);
                return true;
            }
        });

        String bodyHTML = article.getBodyHTML();

        if (bodyHTML != null && !bodyHTML.isEmpty()) {
            content.getSettings().setJavaScriptEnabled(true);
            String htmlData = CSSManager.getInstance().formatHTMLString(article.getBodyHTML());
            content.loadDataWithBaseURL("file:///android_asset/", htmlData, "text/html", "utf-8", null);
        }
    }

    private void    loadAuthorBlocData(Article article, boolean isVisible) {
        authorBloc.setVisibility(isVisible ? View.VISIBLE : View.GONE);
        if (!isVisible) return;

        authorName.setText(article.getSignature());
        authorTwitter.setText(article.getTwitter());
        Log.d("TWITTEROO", "name is " + article.getTwitter());
        authorTwitter.setOnClickListener(createTwitterClickListener(article.getTwitter()));
        if (article.getAuthorMedia() != null && !article.getAuthorMedia().getUrl().isEmpty()) {
            Log.d("TWITTEROO", "Setting author media : " + article.getAuthorMedia().getUrl());
            authorImage.setVisibility(View.VISIBLE);
            ImageManager.getInstance().loadRoundImageIntoView(article.getAuthorMedia().getUrl(), authorImage);
        } else {
            Log.d("TWITTEROO", "No author media or empty URL");
            authorImage.setVisibility(View.GONE);
        }
    }

    private String  getBestUrl(Media m, boolean isLarge) {
        if (m == null) return null;
        String url;
        if (m.getFormats() != null) {
            if (!isLarge) {
                url = m.getFormats().getSmall();
                if (url != null && !url.isEmpty()) return url;
            }
            url = m.getFormats().getLarge();
            if (url != null && !url.isEmpty()) return url;
            url = m.getFormats().getOriginal();
            if (url != null && !url.isEmpty()) return url;
            return m.getUrl();
        } else return m.getUrl();
    }

    private boolean isDiaporama(Article a) {
        return a.getMedias() != null && a.getMedias().size() > 0;
    }

    private boolean isVideo(Article a) {
        return a.getSubHeading() != null && (a.getSubHeading().equals("Vidéo") || a.getSubHeading().equals("video"));
    }

    public void bindRelatedViews(List<Article> relatedList) {

        if (relatedList != null && relatedList.size() > 0) {
            Log.d("RELATING", "We got " + relatedList.size() + " related");
            relatedBox.setVisibility(View.VISIBLE);

            switch (relatedList.size()) {
                case 1:
                    Log.d("RELATING", "Doing the 1 case");
                    articleOne.setVisibility(View.VISIBLE);
                    articleTwo.setVisibility(View.GONE);
                    articleThree.setVisibility(View.GONE);
                    bindArticle(relatedList.get(0), articleOne, articleOneImage, articleOneText);
                    break;
                case 2:
                    Log.d("RELATING", "Doing the 2 case");
                    articleOne.setVisibility(View.VISIBLE);
                    articleTwo.setVisibility(View.VISIBLE);
                    articleThree.setVisibility(View.GONE);
                    bindArticle(relatedList.get(0), articleOne, articleOneImage, articleOneText);
                    bindArticle(relatedList.get(1), articleTwo, articleTwoImage, articleTwoText);
                    break;
                case 3:
                default:
                    Log.d("RELATING", "Doing the 3 case");
                    articleOne.setVisibility(View.VISIBLE);
                    articleTwo.setVisibility(View.VISIBLE);
                    articleThree.setVisibility(View.VISIBLE);
                    bindArticle(relatedList.get(0), articleOne, articleOneImage, articleOneText);
                    bindArticle(relatedList.get(1), articleTwo, articleTwoImage, articleTwoText);
                    bindArticle(relatedList.get(2), articleThree, articleThreeImage, articleThreeText);
                    break;
            }
        } else {
            Log.d("RELATING", "We got nothing.");
            relatedBox.setVisibility(View.GONE);
        }
    }

//    private void retrieveArticleData(final Related related, final View view, final ImageView image, FontTextView text) {
//        view.setVisibility(View.VISIBLE);
//
//        final Article a = TribuneCache.getInstance().getObject(Article.class, related.getArticleId());
//
//        if (a != null) bindArticle(a, view, image, text);
//
//        TribuneAPI.getInstance().getArticle(related.getArticleId(), createArticleAPICallback(view, image, text));
//    }

    private void bindArticle(final Article article, final View view, final ImageView image, FontTextView text) {
        if (article == null) return;
        if (article.getMedia() != null && image != null)
            ImageManager.getInstance().fitImageIntoView(ImageManager.getBestUrl(article.getMedia(), false), image);
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TribuneCache.getInstance().setArticleReadStatus(article, true);

                Intent intent = new Intent(context, BetterArticleDetailsActivity.class);
                intent.putExtra(BetterArticleDetailsActivity.ARTICLE_DETAIL_ID, article.getId());
//                intent.putExtra(BetterArticleDetailsActivity.ARTICLE_DETAIL_CATEGORY, article.getId());
                context.startActivity(intent);
            }
        });
        text.setText(article.getTitle());
    }

    private APICallbackListener<Article>    createArticleAPICallback(final View view, final ImageView image, final FontTextView text) {
        return new APICallbackListener<Article>() {
            @Override
            public void onResponseSuccess(Article response) {
                Log.d("DETAIL_VIEW_HOLDER", "Response for related ARticle");
                bindArticle(response, view, image, text);
            }

            @Override
            public void onResponseFailure(String message) {

            }

            @Override
            public void onFailure(String message) {

            }
        };
    }

    private View.OnClickListener    createImageClickListener(final Article article) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isVideo(article)) {
                    if (article.getMedia() == null || article.getMedia().getUrl() == null || article.getMedia().getUrl().isEmpty()) return;
                    NetworkUtils.openUrlInCustomTab((Activity) context, Uri.parse(article.getMedia().getUrl()));
                    return;
                }


                ArrayList<String> urls = new ArrayList<>();

                urls.add(article.getMedia().getUrl());

                if (article.getMedias() != null) {
                    for (Media m :
                            article.getMedias()) {
                        urls.add(m.getUrl());
                    }
                }

                Intent intent = new Intent(context, GalleryActivity.class);
                intent.putExtra(GalleryActivity.KEY_ARTICLE_KEY, article.getCompositeKey());
                context.startActivity(intent);
            }
        };
    }

    private View.OnClickListener    createTwitterClickListener(final String name) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    context.getPackageManager().getPackageInfo("com.twitter.android", 0);
                    context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("twitter://user?screen_name=" + name)));
                } catch (Exception e) {
                    Log.d("TWITTEROO", "Can't reach twitter app : " + e.getMessage());
                    context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://twitter.com/" + name)));
                }
            }
        };
    }
}
