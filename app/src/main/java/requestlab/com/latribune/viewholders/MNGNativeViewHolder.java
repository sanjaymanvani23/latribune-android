package requestlab.com.latribune.viewholders;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.mngads.MNGNativeObject;
import com.mngads.util.MNGFrame;

import requestlab.com.latribune.R;
import requestlab.com.latribune.data.Article;
import requestlab.com.latribune.utils.AdManager;
import requestlab.com.latribune.utils.ImageManager;
import requestlab.com.latribune.views.fontviews.FontTextView;

/**
 * Created by lchazal on 20/04/16.
 */
public class MNGNativeViewHolder extends AViewHolder {

    private boolean     isLoaded = false;

//    private View            adTitle;
//    private View            view;
    private View            mngView;
    private ImageView       icon;
    private TextView        title;
    private FontTextView    body;
    private TextView        target;
    private ImageView       networkLogo;

    public MNGNativeViewHolder(View itemView) {
        super(itemView);

//        view = itemView;
//        adTitle = itemView.findViewById(R.id.ad_title);
        mngView = itemView.findViewById(R.id.ad_native_view);
        icon = (ImageView) itemView.findViewById(R.id.ad_native_icon);
        title = (TextView) itemView.findViewById(R.id.ad_native_title);
        body = (FontTextView) itemView.findViewById(R.id.ad_native_body);
        target = (TextView) itemView.findViewById(R.id.ad_native_target);
        networkLogo = (ImageView) itemView.findViewById(R.id.network_logo);
    }

    @Override
    public void bindViewHolder(Context context, Article article, int position) {
        if (isLoaded) return;

        AdManager.buildMNGAdLoadingTask(context, createAdListener(), AdManager.AdType.NATIVE).execute();
        isLoaded = true;
    }

    private void setVisible(boolean visibile) {
//        view.setVisibility(visibile ? View.VISIBLE : View.GONE);
//        adTitle.setVisibility(visibile ? View.VISIBLE : View.GONE);
        mngView.setVisibility(visibile ? View.VISIBLE : View.GONE);
    }

    private AdManager.IAdLoadedListener createAdListener() {
        return new AdManager.IAdLoadedListener() {
            @Override
            public void onAdLoaded(View view, int i) {}

            @Override
            public void onAdChanged(MNGFrame mngFrame) {}

            @Override
            public void onNativeAdLoaded(MNGNativeObject object) {
                setVisible(true);
                buildNativeAd(object);
            }

            @Override
            public void onAdFailed(Exception e) {
                setVisible(false);
            }
        };
    }

    private void    buildNativeAd(MNGNativeObject object) {
        if (icon != null) {
            String url = object.getAdCoverImageUrl();
            if (url != null && !url.isEmpty()) ImageManager.getInstance().fitImageIntoView(url, icon);
        }
        if (title != null) title.setText(object.getCallToAction());
        if (body != null) body.setText(String.format("%s : %s", object.getTitle(), object.getBody()));
      //  if (target != null) object.registerViewForInteraction(target,null,null,title);
        if (networkLogo != null && object.getBadge() != null) networkLogo.setImageBitmap(object.getBadge());
    }
}
