package requestlab.com.latribune.viewholders;

import android.content.Context;
import android.view.View;
import android.widget.RelativeLayout;

import com.mngads.MNGNativeObject;
import com.mngads.util.MNGFrame;

import requestlab.com.latribune.R;
import requestlab.com.latribune.data.Article;
import requestlab.com.latribune.utils.AdManager;

/**
 * Created by lchazal on 19/05/16.
 */
public class MNGBannerViewHolder extends AViewHolder {

    private RelativeLayout adView;

    private boolean     isLoaded = false;

    public MNGBannerViewHolder(View itemView) {
        super(itemView);

        adView = (RelativeLayout) itemView.findViewById(R.id.mng_view);
    }

    @Override
    public void bindViewHolder(Context context, Article article, int position) {
        if (isLoaded) return;

        if (adView != null) {
            AdManager.buildMNGAdLoadingTask(context, createAdListener(), AdManager.AdType.BANNER).execute();

            isLoaded = true;
        }
    }

    private AdManager.IAdLoadedListener createAdListener() {
        return new AdManager.IAdLoadedListener() {
            @Override
            public void onAdLoaded(View view, int i) {
                adView.setVisibility(View.VISIBLE);
                adView.addView(view);
            }

            @Override
            public void onNativeAdLoaded(MNGNativeObject object) {}

            @Override
            public void onAdFailed(Exception e) {

            }

            @Override
            public void onAdChanged(MNGFrame mngFrame) {

            }
        };
    }
}
