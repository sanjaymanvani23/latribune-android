package requestlab.com.latribune.viewholders;

import android.content.Context;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import requestlab.com.latribune.R;
import requestlab.com.latribune.data.Article;
import requestlab.com.latribune.utils.ImageManager;
import requestlab.com.latribune.views.fontviews.FontTextView;

/**
 * Created by lchazal on 20/04/16.
 */
public class RelatedArticlesViewHolder extends AViewHolder {

    private View articleOne;
    private ImageView articleOneImage;
    private FontTextView articleOneText;
    private View articleTwo;
    private ImageView articleTwoImage;
    private FontTextView articleTwoText;
    private View articleThree;
    private ImageView articleThreeImage;
    private FontTextView articleThreeText;


    public RelatedArticlesViewHolder(View itemView) {
        super(itemView);

        articleOne = itemView.findViewById(R.id.article_one);
        articleOneImage = (ImageView) articleOne.findViewById(R.id.image);
        articleOneText = (FontTextView) articleOne.findViewById(R.id.text);
        articleTwo = itemView.findViewById(R.id.article_two);
        articleTwoImage = (ImageView) articleTwo.findViewById(R.id.image);
        articleTwoText = (FontTextView) articleTwo.findViewById(R.id.text);
        articleThree = itemView.findViewById(R.id.article_three);
        articleThreeImage = (ImageView) articleThree.findViewById(R.id.image);
        articleThreeText = (FontTextView) articleThree.findViewById(R.id.text);
    }

    @Override
    public void bindViewHolder(Context context, Article... articles) {

        articleTwo.setVisibility(View.GONE);
        articleThree.setVisibility(View.GONE);

        switch (articles.length) {
            case 3:
                Log.d("VIOU", "Doing the 3 case");
                bindArticle(articles[2], articleThree, articleThreeImage, articleThreeText);
            case 2:
                Log.d("VIOU", "Doing the 2 case");
                bindArticle(articles[1], articleTwo, articleTwoImage, articleTwoText);
            case 1:
                Log.d("VIOU", "Doing the 1 case");
                bindArticle(articles[0], articleThree, articleThreeImage, articleThreeText);
                break;
        }
    }

    private void bindArticle(Article article, View view, ImageView image, FontTextView text) {
        view.setVisibility(View.VISIBLE);
        if (article.getMedia() != null && image != null)
            ImageManager.getInstance().fitImageIntoView(article.getMedia().getUrl(), image);
        text.setText(article.getTitle());
    }

    @Override
    public void bindViewHolder(Context context, Article article, int position) {

    }
}
