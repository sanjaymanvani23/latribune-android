package requestlab.com.latribune.viewholders;


import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;


import com.mngads.MNGNativeObject;
import com.mngads.util.MNGFrame;
import com.mngads.views.MAdvertiseNativeContainer;

import requestlab.com.latribune.R;
import requestlab.com.latribune.data.Article;
import requestlab.com.latribune.utils.AdManager;


public class NativeHolder extends AViewHolder {

    private boolean     isLoaded = false;

    private final TextView nativeAdTitle;
    private final TextView nativeAdBody;
    public final RelativeLayout bodyLayout;
    private final TextView nativeAdContext;
    private final ImageView nativeBadgeImage;
    private final Button nativeAdCallToAction;
    public final MAdvertiseNativeContainer nativeAdContainer;
    private final ImageView nativeAdIcon;
    private final ViewGroup mediaContainer;

    public NativeHolder(View view) {
        super(view);
        nativeAdContainer = view.findViewById(R.id.adUnit);
        bodyLayout = view.findViewById(R.id.bodyLayout);
        nativeAdTitle = view.findViewById(R.id.nativeAdTitle);
        nativeAdBody = view.findViewById(R.id.nativeAdBody);
        nativeAdContext = view.findViewById(R.id.nativeAdContext);
        nativeBadgeImage = view.findViewById(R.id.badgeView);
        nativeAdCallToAction = view.findViewById(R.id.nativeAdCallToAction);
        nativeAdIcon = view.findViewById(R.id.nativeAdIcon);
        mediaContainer = view.findViewById(R.id.mediaContainer);
    }

    @Override
    public void bindViewHolder(Context context, Article article, int position) {
        if (isLoaded) return;

        AdManager.buildMNGAdLoadingTask(context, createAdListener(), AdManager.AdType.NATIVE).execute();
        isLoaded = true;
    }

    private AdManager.IAdLoadedListener createAdListener() {
        return new AdManager.IAdLoadedListener() {
            @Override
            public void onAdLoaded(View view, int i) {}

            @Override
            public void onAdChanged(MNGFrame mngFrame) {}

            @Override
            public void onNativeAdLoaded(MNGNativeObject object) {
                setVisible(true);
                displayNativeAd(object,false);
            }

            @Override
            public void onAdFailed(Exception e) {
                setVisible(false);
            }
        };
    }

    private void setVisible(boolean visibile) {
//        view.setVisibility(visibile ? View.VISIBLE : View.GONE);
//        adTitle.setVisibility(visibile ? View.VISIBLE : View.GONE);
        nativeAdContainer.setVisibility(visibile ? View.VISIBLE : View.GONE);
    }


    public void displayNativeAd(MNGNativeObject nativeObject, boolean setCover) {
        if (nativeObject != null) {
            bodyLayout.setVisibility(View.VISIBLE);
            nativeAdContainer.setVisibility(View.VISIBLE);
            nativeAdContext.setText(nativeObject.getSocialContext());
            nativeAdTitle.setText(nativeObject.getTitle());
            nativeAdBody.setText(nativeObject.getBody());
            nativeAdCallToAction.setText(nativeObject.getCallToAction());
            nativeBadgeImage.setImageBitmap(nativeObject.getBadge(getContext(), "Publicité"));
            if (setCover) {
                nativeObject.registerViewForInteraction(nativeAdContainer, mediaContainer, nativeAdIcon, nativeAdCallToAction);
            } else {
                nativeObject.registerViewForInteraction(nativeAdContainer, null, nativeAdIcon, nativeAdCallToAction);
            }
        } else {
            bodyLayout.setVisibility(View.GONE);
            nativeAdContainer.setVisibility(View.GONE);
        }
    }

    Context getContext() {
        return nativeAdContainer.getContext();
    }
}

