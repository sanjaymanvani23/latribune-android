package requestlab.com.latribune.viewholders;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.outbrain.OBSDK.Entities.OBRecommendation;
import com.outbrain.OBSDK.Entities.OBRecommendationsResponse;
import com.outbrain.OBSDK.FetchRecommendations.RecommendationsListener;

import java.util.List;

import requestlab.com.latribune.R;
import requestlab.com.latribune.data.Article;
import requestlab.com.latribune.utils.ImageManager;
import requestlab.com.latribune.utils.OutbrainManager;
import requestlab.com.latribune.views.fontviews.FontTextView;


/**
 * Created by lchazal on 20/04/16.
 */
public class OutbrainViewHolder extends AViewHolder implements RecommendationsListener {

    private static final String OUTBRAIN_URL = "http://www.outbrain.com/what-is/default/en-mobile";

    private boolean     isLoaded = false;
    private Context     context;

    private View            header;
    private ImageButton     outbrainLogo;
    private FontTextView    title;

    private View        view_one;
    private ImageView   image_one;
    private ImageView   video_one;
    private TextView    title_one;
    private TextView    sponsor_one;
    private View        view_two;
    private ImageView   image_two;
    private ImageView   video_two;
    private TextView    title_two;
    private TextView    sponsor_two;
    private View        view_three;
    private ImageView   image_three;
    private ImageView   video_three;
    private TextView    title_three;
    private TextView    sponsor_three;
    private View        view_four;
    private ImageView   image_four;
    private ImageView   video_four;
    private TextView    title_four;
    private TextView    sponsor_four;

    public OutbrainViewHolder(View itemView) {
        super(itemView);

        Log.d("OUTBRAIN", "Creating outbrain vh : view is " + (itemView == null ? "ko": "ok"));

        header = itemView.findViewById(R.id.header);
        outbrainLogo = (ImageButton) itemView.findViewById(R.id.outbrain_logo);
        title = (FontTextView) itemView.findViewById(R.id.title);

        Log.d("OUTBRAIN", "header is " + (header == null ? "ko" : "ok"));
        Log.d("OUTBRAIN", "outbrainLogo is " + (outbrainLogo == null ? "ko" : "ok"));
        Log.d("OUTBRAIN", "title is " + (title == null ? "ko" : "ok"));

        view_one = itemView.findViewById(R.id.article_one);
        if (view_one != null) {
            image_one = (ImageView) view_one.findViewById(R.id.image);
            video_one = (ImageView) view_one.findViewById(R.id.picto_overlay);
            title_one = (TextView) view_one.findViewById(R.id.text);
            sponsor_one = (TextView) view_one.findViewById(R.id.sponsor);
        }
        view_two = itemView.findViewById(R.id.article_two);
        if (view_two != null) {
            image_two = (ImageView) view_two.findViewById(R.id.image);
            video_two = (ImageView) view_two.findViewById(R.id.picto_overlay);
            title_two = (TextView) view_two.findViewById(R.id.text);
            sponsor_two = (TextView) view_two.findViewById(R.id.sponsor);
        }
        view_three = itemView.findViewById(R.id.article_three);
        if (view_three != null) {
            image_three = (ImageView) view_three.findViewById(R.id.image);
            video_three = (ImageView) view_three.findViewById(R.id.picto_overlay);
            title_three = (TextView) view_three.findViewById(R.id.text);
            sponsor_three = (TextView) view_three.findViewById(R.id.sponsor);
        }
        view_four = itemView.findViewById(R.id.article_four);
        if (view_four != null) {
            image_four = (ImageView) view_four.findViewById(R.id.image);
            video_four = (ImageView) view_four.findViewById(R.id.picto_overlay);
            title_four = (TextView) view_four.findViewById(R.id.text);
            sponsor_four = (TextView) view_four.findViewById(R.id.sponsor);
        }
    }

    @Override
    public void bindViewHolder(Context context, Article article, int position) {
        if (isLoaded) return;

        this.context = context;

        OutbrainManager.getInstance().getRecommendations(article.getUrl(), this);

        isLoaded = true;
    }

    @Override
    public void onOutbrainRecommendationsSuccess(OBRecommendationsResponse recommendations) {
        Log.d("OUTBRAIN", "Received recommendations");
        for (OBRecommendation r :
                recommendations.getAll()) {
            Log.d("OUTBRAIN", "content : " + r.getContent());
            Log.d("OUTBRAIN", "img : " + r.getThumbnail().getUrl());
            Log.d("OUTBRAIN", "source : " + r.getSourceName());
            Log.d("OUTBRAIN", "isPaid : " + r.isPaid());
        }
        List<OBRecommendation> list = recommendations.getAll();

        if (list.size() <= 0) {
            header.setVisibility(View.GONE);
            view_one.setVisibility(View.GONE);
            view_two.setVisibility(View.GONE);
            view_three.setVisibility(View.GONE);
            return;
        }
        if (list.size() > 0) {
            header.setVisibility(View.VISIBLE);
            outbrainLogo.setOnClickListener(createOutbrainClickListener());
            view_one.setVisibility(View.VISIBLE);
            bindView(list.get(0), view_one, image_one, video_one, title_one, sponsor_one);
        }
        if (list.size() > 1) {
            view_two.setVisibility(View.VISIBLE);
            bindView(list.get(1), view_two, image_two, video_two, title_two, sponsor_two);
        }
        if (list.size() > 2) {
            view_three.setVisibility(View.VISIBLE);
            bindView(list.get(2), view_three, image_three, video_three, title_three, sponsor_three);
        }
        if (list.size() > 3) {
            view_four.setVisibility(View.VISIBLE);
            bindView(list.get(3), view_four, image_four, video_four, title_four, sponsor_four);
        }
    }

    private void bindView(OBRecommendation obRecommendation, View view, ImageView image, ImageView video, TextView title, TextView sponsor) {
        if (obRecommendation.getThumbnail() != null && image != null) {
            String url = obRecommendation.getThumbnail().getUrl();
            if (url != null && !url.isEmpty()) ImageManager.getInstance().fitImageIntoView(url, image);
        }
        if (video != null) video.setVisibility(obRecommendation.isVideo() ? View.VISIBLE : View.GONE);
        if (title != null) title.setText(obRecommendation.getContent());
        if (sponsor != null) {
            sponsor.setText(obRecommendation.getSourceName());
            //TODO change field ...
        }
        if (view != null) {
            view.setOnClickListener(createRecommendationClickListener(obRecommendation));
        }
    }

    private View.OnClickListener createOutbrainClickListener() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(OUTBRAIN_URL));
                context.startActivity(browserIntent);
            }
        };
    }

    private View.OnClickListener createRecommendationClickListener(final OBRecommendation rec) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String url = OutbrainManager.getInstance().getRecommendationUrl(rec);

                Log.d("OUTBRAIN", "Clicked : " + url);
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                context.startActivity(browserIntent);
            }
        };
    }

    @Override
    public void onOutbrainRecommendationsFailure(Exception ex) {
        Log.e("OUTBRAIN", "Failed recommendations : " + ex.getMessage());
    }
}
