package requestlab.com.latribune;

import android.content.Context;


import androidx.multidex.MultiDex;

import com.ad4screen.sdk.A4SApplication;
import com.batch.android.BatchActivityLifecycleHelper;
import com.batch.android.*;
import com.crashlytics.android.Crashlytics;
import com.mngads.MNGAdsFactory;

import io.fabric.sdk.android.Fabric;
import requestlab.com.latribune.cache.TribuneCache;
import requestlab.com.latribune.tracking.TrackerManager;
import requestlab.com.latribune.utils.CSSManager;
import requestlab.com.latribune.utils.DataManager;
import requestlab.com.latribune.utils.FontPreferencesManager;
import requestlab.com.latribune.utils.ImageManager;
import requestlab.com.latribune.utils.OutbrainManager;

/**
 * Created by lchazal on 02/03/16.
 */
public class TribuneApplication extends A4SApplication {

    @Override
    public void onApplicationCreate() {
        super.onApplicationCreate();

        initialize();
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    private void initialize() {
        Fabric.with(this, new Crashlytics());

        String userid = BuildConfig.DEBUG ? "Débug" : "Production";
        Crashlytics.setUserIdentifier(userid);

        OutbrainManager.getInstance().initialize(this);
        MNGAdsFactory.initialize(this, BuildConfig.MNGADS_APP_ID);
        MNGAdsFactory.setDebugModeEnabled(BuildConfig.DEBUG);

        TrackerManager.getInstance().init(this);
        registerActivityLifecycleCallbacks(TrackerManager.getAdjustLifecycleCallback());

        FontPreferencesManager.getInstance().fetchValues(this);
        CSSManager.getInstance().init(this);
        DataManager.getInstance().init(this);

        Batch.setConfig(new Config(BuildConfig.BATCH_KEY));
        registerActivityLifecycleCallbacks(new BatchActivityLifecycleHelper());

        setupCache();
        setupImageLoader();
    }

    /**
     * Initialize Cache
     */
    private void setupCache() {
        TribuneCache.getInstance().initialize(this);
    }

    private void setupImageLoader() {
        ImageManager.getInstance().init(this);
    }
}
