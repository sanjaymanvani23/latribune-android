package requestlab.com.latribune.activities;

import android.os.Bundle;
import android.util.Log;
import android.util.TypedValue;
import android.widget.SeekBar;
import android.widget.TextView;

import requestlab.com.latribune.R;
import requestlab.com.latribune.tracking.TrackerManager;
import requestlab.com.latribune.utils.AnimationUtils;
import requestlab.com.latribune.utils.CSSManager;
import requestlab.com.latribune.utils.FontPreferencesManager;

/**
 * Created by lchazal on 17/03/16.
 */
public class TextSizeActivity extends AToolbarCompatActivity implements SeekBar.OnSeekBarChangeListener {

    private static final int MIN_TEXT_SIZE = 12;
    private static final int MAX_TEXT_SIZE = 20;

    private TextView    previewText;

    private int         currentFontSize = 17;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_text_size);

        setupToolbar(R.drawable.ic_arrow_back_white_24dp, getString(R.string.text_size_toolbar));


        previewText = (TextView) findViewById(R.id.preview);
        int savedFontSize = FontPreferencesManager.getInstance().getpSize();
        updateTextSize(savedFontSize);

        setSeekBarListener();
    }

    private void setSeekBarListener() {
        SeekBar seekBar = (SeekBar) findViewById(R.id.seek_bar);
        seekBar.setOnSeekBarChangeListener(this);
        seekBar.setMax((MAX_TEXT_SIZE - MIN_TEXT_SIZE) * 10);
        seekBar.setProgress((currentFontSize - MIN_TEXT_SIZE) * 10);
    }

    private void saveValues() {
        FontPreferencesManager fontManager = FontPreferencesManager.getInstance();

        Log.d("FONT_MANAGER", "method save values");

        if (fontManager.getpSize() != currentFontSize) {
            fontManager.setpSize(currentFontSize);
            fontManager.setHeaderSize(currentFontSize + getHeaderSizeDifference());
            fontManager.setSubHeaderSize(currentFontSize + getSubHeaderSizeDifference());

            Log.d("FONT_MANAGER", "Calling save values");
            fontManager.saveValues(this);

            CSSManager.getInstance().formatCssString(this);
        }
    }

    private int getHeaderSizeDifference() {
        int textSize = getResources().getInteger(R.integer.css_p_default_size);
        int headerSize = getResources().getInteger(R.integer.css_h1_default_size);

        return headerSize - textSize;
    }

    private int getSubHeaderSizeDifference() {
        int textSize = getResources().getInteger(R.integer.css_p_default_size);
        int subheadSize = getResources().getInteger(R.integer.css_h2_default_size);

        return subheadSize - textSize;
    }

    @Override
    protected void onHomeButtonClicked() {
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        Log.d("FONT_MANAGER", "onBackPressed");
        saveValues();
        super.onBackPressed();
        AnimationUtils.animateOut(this);
    }

    @Override
    protected void onResume() {
        super.onResume();

        TrackerManager.getInstance().tagScreen(getString(R.string.tracking_settings_chapter),
                getString(R.string.tracking_settings_title_text_size));
    }

    private void updateTextSize(int fontSize) {
        if (fontSize != currentFontSize) {
            previewText.setTextSize(TypedValue.COMPLEX_UNIT_SP, fontSize);
            currentFontSize = fontSize;
        }
    }

    /* Text size SeekBar callbacks */

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        Log.d("SEEK_BAR_TEXT", "Change to value " + progress + " from " + (fromUser ? "user" : "auto"));
        int newFontSize = progress / 10 + MIN_TEXT_SIZE;
        updateTextSize(newFontSize);
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {

    }
}
