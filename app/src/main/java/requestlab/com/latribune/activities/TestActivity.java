package requestlab.com.latribune.activities;

import android.os.Bundle;

import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.annotation.Nullable;

import com.mngads.MNGAdsFactory;
import com.mngads.listener.MNGAdsSDKFactoryListener;
import com.mngads.listener.MNGBannerListener;
import com.mngads.util.MNGFrame;

import requestlab.com.latribune.R;
import requestlab.com.latribune.accengage.A4SAppCompatActivity;


/**
 * Created by lchazal on 19/04/16.
 */
public class TestActivity extends A4SAppCompatActivity implements MNGAdsSDKFactoryListener, MNGBannerListener {

    private MNGAdsFactory mngAdsBannerAdsFactory;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.test_layout);


//        LigAdView view = (LigAdView) findViewById(R.id.view1);
//        view.fillLigAdView(this, 81839);
//        LigAdView vview = (LigAdView) findViewById(R.id.view2);
//        vview.fillLigAdView(this, 81839);

        MNGAdsFactory.initialize(this,"8177862");

        if(MNGAdsFactory.isInitialized()){

            //The SDK is initialized
            Toast.makeText(this, "MNGAdsFactory is initialized", Toast.LENGTH_SHORT).show();

        }else {

            //The SDK is initializing
            //set up a callback that will be called when is fully initiamized
            MNGAdsFactory.setMNGAdsSDKFactoryListener(this);

            Toast.makeText(this, "MNGAdsFactory is not initialized", Toast.LENGTH_SHORT).show();

        }

        mngAdsBannerAdsFactory = new MNGAdsFactory(this);
// set banner listener
        mngAdsBannerAdsFactory.setBannerListener(this);
        mngAdsBannerAdsFactory.setPlacementId("/8177862/banner");

        mngAdsBannerAdsFactory.loadBanner(new MNGFrame(320, 50));

       /* if(mngAdsBannerAdsFactory.createBanner(new MNGFrame(320, 50))){
            //Wait callBack from listener
            Log.d("BANNA", "Can create banner");
        }else{
            //adsFactory can not handle your request
            Log.d("BANNA", "Can't create banner");
        }*/
    }

    @Override
    public void onMNGAdsSDKFactoryDidFinishInitializing() {
        Toast.makeText(this, "MNGAds SDK Factory Did Finish Initializing", Toast.LENGTH_SHORT).show();

    }

    @Override
    public void onMNGAdsSDKFactoryDidFailInitialization(Exception e) {

    }

    @Override
    public void onMNGAdsSDKFactoryDidResetConfig() {

    }

    @Override
    public void bannerDidLoad(View view, int i) {
        Log.d("BANNA", "Banner loaded");

        ((LinearLayout) findViewById(R.id.layout)).addView(view);
    }

    @Override
    public void bannerDidFail(Exception e) {
        Log.d("BANNA", "Banner failed");

    }

    @Override
    public void bannerResize(MNGFrame mngFrame) {
        Log.d("BANNA", "Banner resized");

    }
}
