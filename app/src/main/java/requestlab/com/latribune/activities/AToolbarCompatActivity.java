package requestlab.com.latribune.activities;


import android.util.Log;
import android.view.MenuItem;
import android.widget.TextView;

import androidx.appcompat.widget.Toolbar;

import requestlab.com.latribune.R;
import requestlab.com.latribune.accengage.A4SAppCompatActivity;

/**
 * Created by lchazal on 19/01/16.
 *
 * Base activity for activities using toolbar. Any child of this should include a @layout/toolbar in it's own layout.
 */
//public abstract class AToolbarCompatActivity extends AppCompatActivity {
public abstract class AToolbarCompatActivity extends A4SAppCompatActivity {

    private static final String TAG = "TOOLBAR_COMPAT_ACTIVITY";

    private TextView    mToolbarTitle;

    protected void      setupToolbar(int homeIconId, String title) {
        setupToolbar(homeIconId);
        setToolbarTitle(title);
    }

    protected void    setupToolbar(int homeIconId) {
        Toolbar mToolbar = (Toolbar) findViewById(R.id.toolbar);
        if (mToolbar != null) {
            mToolbarTitle = (TextView) mToolbar.findViewById(R.id.toolbar_title);
            setSupportActionBar(mToolbar);
        }

        try {
            getSupportActionBar().setHomeAsUpIndicator(homeIconId);
        } catch (NullPointerException e) {
            Log.d(TAG, "Error setting home indicator : " + e.getMessage());
        }

        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    public void setToolbarTitle(String title) {
        if (mToolbarTitle != null) mToolbarTitle.setText(title);
    }

    protected void onHomeButtonClicked() {
        onBackPressed();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.onHomeButtonClicked();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
