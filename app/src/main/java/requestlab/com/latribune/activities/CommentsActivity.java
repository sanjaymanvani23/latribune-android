package requestlab.com.latribune.activities;

import android.os.Bundle;

import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import requestlab.com.latribune.R;
import requestlab.com.latribune.adapters.CommentsAdapter;
import requestlab.com.latribune.api.TribuneAPI;
import requestlab.com.latribune.api.listeners.APICallbackListener;
import requestlab.com.latribune.cache.TribuneCache;
import requestlab.com.latribune.data.Comment;
import requestlab.com.latribune.data.Comments;
import requestlab.com.latribune.tracking.TrackerManager;

/**
 * Created by lchazal on 17/03/16.
 */
public class CommentsActivity extends AToolbarCompatActivity {

    private LinearLayout    commentBox;
    private CommentsAdapter adapter;

    private int articleId;
    private int count;

    private Comments        mainComment;
    private List<Comment>   commentList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comments);

        articleId = getIntent().getIntExtra("articleid", 0);
        count = getIntent().getIntExtra("articlecount", 0);

        setupToolbar(R.drawable.ic_arrow_back_white_24dp, getResources().getQuantityString(R.plurals.comment_toolbar_title, count, count));

        setupAdapter();
        setupRecyclerView();

//        createDummyComment();
    }

    @Override
    protected void onResume() {
        super.onResume();

        TrackerManager.getInstance().tagScreen(getString(R.string.tracking_comments_chapter),
                getString(R.string.tracking_comments_title_list));


        if (articleId == 0) Toast.makeText(this, "Invalid article id", Toast.LENGTH_LONG).show();
        else {
            retrieveCacheData();
            retrieveApiData();
        }
    }

    private void retrieveCacheData() {
        mainComment = TribuneCache.getInstance().getObject(Comments.class, articleId);

        if (mainComment != null && mainComment.getLastComment() != null) {
            adapter.clear();
            adapter.addComment(mainComment.getLastComment());
        }
    }

    private void    retrieveApiData() {
        //DO STUFF
        TribuneAPI.getInstance().getCommentList(articleId, createAPICallbackListener());
    }

    private void setupAdapter() {
        adapter = new CommentsAdapter(this, articleId);
    }

    private void setupRecyclerView() {
        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recycler_view);

        if (recyclerView == null) return;

        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
    }

    private APICallbackListener<List<Comment>>  createAPICallbackListener() {
        return new APICallbackListener<List<Comment>>() {
            @Override
            public void onResponseSuccess(List<Comment> response) {
                adapter.clear();
                for (Comment c :
                        response) {
                    adapter.addComment(c);
                }
            }

            @Override
            public void onResponseFailure(String message) {

            }

            @Override
            public void onFailure(String message) {

            }
        };
    }
}
