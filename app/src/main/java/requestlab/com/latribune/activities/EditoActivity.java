package requestlab.com.latribune.activities;

import android.os.Bundle;

import android.view.View;
import android.webkit.WebView;
import android.widget.ImageButton;

import androidx.annotation.Nullable;

import requestlab.com.latribune.R;
import requestlab.com.latribune.accengage.A4SAppCompatActivity;
import requestlab.com.latribune.tracking.TrackerManager;
import requestlab.com.latribune.utils.CSSManager;

/**
 * Created by lchazal on 02/06/16.
 */
public class EditoActivity extends A4SAppCompatActivity {

    public static final String  EXTRA_HTML_CONTENT = "exhtmlco";

    private WebView     content;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edito);

        TrackerManager.getInstance().tagScreen(getString(R.string.tracking_infos_chapter),
                getString(R.string.tracking_infos_title_edito));

        content = (WebView) findViewById(R.id.webview);

        loadWebViewData(getStringExtra());
        setBackButton();
    }

    private void setBackButton() {
        ImageButton button  = (ImageButton) findViewById(R.id.back);
        if (button == null) return;
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void loadWebViewData(String htmlString) {
        if (content == null || htmlString.isEmpty()) return;

        content.getSettings().setJavaScriptEnabled(true);
        String htmlData = CSSManager.getInstance().formatHTMLString(htmlString);
        content.loadDataWithBaseURL("file:///android_asset/", htmlData, "text/html", "utf-8", null);
    }

    private String getStringExtra() {
        return getIntent().getStringExtra(EXTRA_HTML_CONTENT);
    }
}
