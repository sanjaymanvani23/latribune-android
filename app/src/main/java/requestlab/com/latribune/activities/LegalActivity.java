package requestlab.com.latribune.activities;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.TextView;

import requestlab.com.latribune.R;
import requestlab.com.latribune.tracking.TrackerManager;
import requestlab.com.latribune.utils.AnimationUtils;

/**
 * Created by lchazal on 17/03/16.
 */
public class LegalActivity extends AToolbarCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_legal);

        setupCGU();

        setupToolbar(R.drawable.ic_arrow_back_white_24dp, getString(R.string.legal_toolbar));
    }

    @Override
    protected void onHomeButtonClicked() {
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        AnimationUtils.animateOut(this);
    }

    private void setupCGU() {
        TextView text = (TextView) findViewById(R.id.cgu_text);
        if (text != null) {
            text.setText(Html.fromHtml(getString(R.string.legal_law)));
            text.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onCGUTextClicked();
                }
            });
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        TrackerManager.getInstance().tagScreen(getString(R.string.tracking_settings_chapter),
                getString(R.string.tracking_settings_title_legal));
    }

    private void onCGUTextClicked() {
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://static.latribune.fr/514789/cgu-non-pro.pdf"));
        startActivity(browserIntent);
    }
}
