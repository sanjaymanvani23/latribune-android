package requestlab.com.latribune.activities;

import android.graphics.Color;
import android.os.Bundle;

import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.core.content.ContextCompat;

import com.crashlytics.android.Crashlytics;

import requestlab.com.latribune.R;
import requestlab.com.latribune.api.TribuneAPI;
import requestlab.com.latribune.api.listeners.APICallbackListener;
import requestlab.com.latribune.tracking.TrackerManager;
import requestlab.com.latribune.utils.AnimationUtils;
import requestlab.com.latribune.utils.CompatibilityUtils;
import requestlab.com.latribune.utils.ErrorLogger;

/**
 * Created by lchazal on 17/03/16.
 */
public class ReactActivity extends AToolbarCompatActivity{

    public static final String REACT_CALLER = "articlecaller";
    public static final String REACT_ARTICLE_ID = "articleid";

    public static final String REACT_ARTICLE_TITLE = "articletitle";
    public static final String REACT_PSEUDO = "pseudo";

    private TextView    title;
    private TextView    target;

    private int       articleId;
    private String callerTag;

    private EditText    pseudoField;
    private EditText    emailField;
    private EditText    contentField;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_react);

        fetchArticleId();
        fetchCallingActivityData();

        setupCancelButton();
        setupSendButton();

        setupToolbar(R.drawable.ic_arrow_back_white_24dp, getString(R.string.react_title));

        title = (TextView) findViewById(R.id.react_title);
        target = (TextView) findViewById(R.id.pseudo);

        pseudoField = (EditText) findViewById(R.id.react_field_pseudo);
        emailField = (EditText) findViewById(R.id.react_field_email);
        contentField = (EditText) findViewById(R.id.react_field_content);

        setTitle();
    }

    private void    fetchArticleId() {
        articleId = getIntent().getIntExtra(REACT_ARTICLE_ID, -1);
        if (articleId == -1) {
            Log.e("ReactActivity", "Invalid article id : " + articleId);
            Toast.makeText(this, "Invalid article id : " + articleId, Toast.LENGTH_LONG).show();
            ErrorLogger.logBadArticleToCrashlytics(articleId, callerTag);
            finish();
        }
    }

    private void    fetchCallingActivityData() {
        callerTag = getIntent().getStringExtra(REACT_CALLER);
        if (callerTag == null) {
            Log.e("ReactActivity", "Calling activity didn't specify it's name !");
        }
    }

    private void    setTitle() {
        String pseudo = getIntent().getStringExtra(REACT_PSEUDO);

        if (pseudo == null || pseudo.isEmpty()) {
            title.setText(R.string.react_title);
            target.setText(getIntent().getStringExtra(REACT_ARTICLE_TITLE));
        } else {
            title.setText(R.string.respond_title);
            target.setText(getString(R.string.react_to, pseudo));
        }
    }

    @Override
    protected void onHomeButtonClicked() {
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        AnimationUtils.animateOut(this);
    }

    @Override
    protected void onResume() {
        super.onResume();

        TrackerManager.getInstance().tagScreen(getString(R.string.tracking_comments_chapter),
                getString(R.string.tracking_comments_title_post));
    }

    private void setupCancelButton() {
        Button btn = (Button) findViewById(R.id.btn_cancel);
        if (btn == null) return;
        CompatibilityUtils.setButtonTint(btn, Color.WHITE);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                terminate();
            }
        });
    }

    private void setupSendButton() {
        Button btn = (Button) findViewById(R.id.btn_send);
        if (btn == null) return;
        CompatibilityUtils.setButtonTint(btn, ContextCompat.getColor(this, R.color.send_button_background));
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onSendButtonClicked();
            }
        });
    }

    private void onSendButtonClicked() {
        if (pseudoField.getText().length() < 1 ||
                emailField.getText().length() < 1 ||
                contentField.getText().length() < 1) return;
        TribuneAPI.getInstance().sendComment(new APICallbackListener<Void>() {
            @Override
            public void onResponseSuccess(Void response) {
                terminate();
            }

            @Override
            public void onResponseFailure(String message) {
                Toast.makeText(ReactActivity.this, "Erreur lors de l'envoi du commentaire", Toast.LENGTH_LONG).show();
            }

            @Override
            public void onFailure(String message) {
                Toast.makeText(ReactActivity.this, "Impossible d'envoyer le commentaire", Toast.LENGTH_LONG).show();
            }
        }, articleId, contentField.getText().toString(), emailField.getText().toString(), pseudoField.getText().toString());
    }

    public void terminate() {
        onBackPressed();
    }
}
