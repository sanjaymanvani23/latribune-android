package requestlab.com.latribune.activities;

import android.graphics.Color;
import android.os.Bundle;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.LinearLayout;

import androidx.appcompat.widget.SwitchCompat;

import requestlab.com.latribune.C.Constants;
import requestlab.com.latribune.R;
import requestlab.com.latribune.cache.TribuneCache;
import requestlab.com.latribune.data.Thematics;
import requestlab.com.latribune.tracking.TrackerManager;
import requestlab.com.latribune.utils.AnimationUtils;
import requestlab.com.latribune.utils.Triple;

/**
 * Created by lchazal on 17/03/16.
 */
public class FavoritesActivity extends AToolbarCompatActivity {

    private boolean[] values = new boolean[Constants.NOTIFICATION_KEYS.size()];

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_favorites);

        addFavoritesButtons();

        setupToolbar(R.drawable.ic_arrow_back_white_24dp, getString(R.string.favorite_toolbar));
    }

    @Override
    protected void onHomeButtonClicked() {
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        saveFavorites();
        super.onBackPressed();
        AnimationUtils.animateOut(this);
    }

    @Override
    protected void onResume() {
        super.onResume();

        TrackerManager.getInstance().tagScreen(getString(R.string.tracking_settings_chapter),
                getString(R.string.tracking_settings_title_categories));
    }

    private void addFavoritesButtons() {
        LinearLayout ll = (LinearLayout) findViewById(R.id.favorites);

        ViewGroup.LayoutParams params = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 144);

        boolean odd = false;

        Thematics th = TribuneCache.getInstance().getThematics();
        if (th == null) th = new Thematics();

        for (int i = 0; i < values.length; i++) {
            Triple<String, String, Integer> t = Constants.NOTIFICATION_KEYS.get(i);

            SwitchCompat switchCompat = (SwitchCompat) LayoutInflater.from(this).inflate(R.layout.switch_favourite, null);
            switchCompat.setChecked(getValue(th, t.first()));
            switchCompat.setText(t.second());
            switchCompat.setLayoutParams(params);
            switchCompat.setBackgroundColor(odd ? Color.TRANSPARENT : Color.WHITE);
            values[i] = switchCompat.isChecked();
            switchCompat.setOnCheckedChangeListener(createCheckedChangeListener(i));
            odd = !odd;
            ll.addView(switchCompat);
        }
    }

    public static boolean             getValue(Thematics t, String key) {
        Log.d("THEME", "Getting value : " + key);
        switch (key) {
            case "optin_economie_france":
                return t.isOptin_economie_france();
            case "optin_economie_europe_inter":
                return t.isOptin_economie_europe_inter();
            case "optin_bourse_marches":
                return t.isOptin_bourse_marches();
            case "optin_banque-assurance":
                return t.isOptin_banque_assurance();
            case "optin_transport":
                return t.isOptin_transport();
            case "optin_grande_distrib":
                return t.isOptin_grande_distrib();
            case "optin_aero_defense":
                return t.isOptin_aero_defense();
            case "optin_techno_medias_telecoms":
                return t.isOptin_techno_medias_telecoms();
            case "optin_finances_immo":
                return t.isOptin_finances_immo();
            case "optin_energie_green":
                return t.isOptin_energie_green();
            default:
                Log.d("THEME", "Unknown value :/");
                return false;
        }
    }

    public static void             setValue(Thematics t, String key, boolean value) {
        Log.d("THEME", "Setting value : " + key + " to " + value);
        switch (key) {
            case "optin_economie_france":
                t.setOptin_economie_france(value);
                break;
            case "optin_economie_europe_inter":
                t.setOptin_economie_europe_inter(value);
                break;
            case "optin_bourse_marches":
                t.setOptin_bourse_marches(value);
                break;
            case "optin_banque-assurance":
                t.setOptin_banque_assurance(value);
                break;
            case "optin_transport":
                t.setOptin_transport(value);
                break;
            case "optin_grande_distrib":
                t.setOptin_grande_distrib(value);
                break;
            case "optin_aero_defense":
                t.setOptin_aero_defense(value);
                break;
            case "optin_techno_medias_telecoms":
                t.setOptin_techno_medias_telecoms(value);
                break;
            case "optin_finances_immo":
                t.setOptin_finances_immo(value);
                break;
            case "optin_energie_green":
                t.setOptin_energie_green(value);
                break;
            default:
                Log.d("THEME", "UNKNOWN VALUE");
                break;
        }
    }

    private CompoundButton.OnCheckedChangeListener createCheckedChangeListener(final int position) {
        return new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                values[position] = isChecked;
            }
        };
    }

    private void saveFavorites() {
        Thematics th = new Thematics();
        th.setId(1);

        for (int i = 0; i < values.length; i++) {
            Triple<String, String, Integer> t = Constants.NOTIFICATION_KEYS.get(i);
            setValue(th, t.first(), values[i]);
        }
        TribuneCache.getInstance().addOrUpdateObject(th);
        TrackerManager.sendAccengageThematics(getA4S(), values);
    }
}
