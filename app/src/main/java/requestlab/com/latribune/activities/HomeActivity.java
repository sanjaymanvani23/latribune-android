package requestlab.com.latribune.activities;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.FragmentTransaction;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import requestlab.com.latribune.BuildConfig;
import requestlab.com.latribune.C.Constants;
import requestlab.com.latribune.R;
import requestlab.com.latribune.adapters.HomeDrawerAdapter;
import requestlab.com.latribune.api.TribuneAPI;
import requestlab.com.latribune.api.listeners.APICallbackListener;
import requestlab.com.latribune.cache.TribuneCache;
import requestlab.com.latribune.data.Category;
import requestlab.com.latribune.data.Hebdo;
import requestlab.com.latribune.fragments.AContentFragment;
import requestlab.com.latribune.fragments.FlowContentFragment;
import requestlab.com.latribune.fragments.MostReadContentFragment;
import requestlab.com.latribune.fragments.SettingsContentFragment;
import requestlab.com.latribune.tracking.TrackerManager;
import requestlab.com.latribune.utils.AdManager;
import requestlab.com.latribune.utils.AnimationUtils;
import requestlab.com.latribune.utils.NetworkUtils;
import requestlab.com.latribune.views.fontviews.FontTextView;


/**
 * Created by lchazal on 12/04/16.
 */
public class HomeActivity extends AToolbarCompatActivity
        implements AdapterView.OnItemClickListener, DrawerLayout.DrawerListener {

    private HomeDrawerAdapter drawerAdapter;
    protected DrawerLayout drawerLayout;

    private String[]        categoryNames;
    private List<Category>  drawerCategories;

    private Map<String, AContentFragment>   fragments;

    public HomeActivity() {
        fragments = new HashMap<>();
    }

    private ImageView       imageTitle;
    private FontTextView    toolbarTitle;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        getA4S().setPushNotificationLocked(false);
        getA4S().setInAppDisplayLocked(false);

        AdManager.displayInterstitial();

        retrieveCategoryData();
        retrieveHebdoData();

        setupDrawerList();
        fillDrawerList();

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        imageTitle = (ImageView) toolbar.findViewById(R.id.toolbar_logo);
        toolbarTitle = (FontTextView) toolbar.findViewById(R.id.toolbar_title);
        toolbar.setTitle("");
        setSupportActionBar(toolbar);

        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawerLayout.addDrawerListener(toggle);
        drawerLayout.addDrawerListener(this);
        toggle.syncState();

        showHomeView(1);
    }

    private void retrieveHebdoData() {
        TribuneAPI.getInstance().getDailyEdition(new APICallbackListener<Hebdo>() {
            @Override
            public void onResponseSuccess(Hebdo response) {
                TribuneCache.getInstance().addOrUpdateHebdo(response);
                drawerAdapter.refreshHebdoImage();
            }

            @Override
            public void onResponseFailure(String message) {
                //DO NOTHING
            }

            @Override
            public void onFailure(String message) {
                //DO NOTHING
            }
        });
    }

    private void setToolbarTitle(boolean imageVisible, int titleId) {
        imageTitle.setVisibility(imageVisible ? View.VISIBLE : View.GONE);
        toolbarTitle.setText(titleId == 0 ? "" : getString(titleId));
    }

    public static final int MENU_TOP = 1;
    public static final int MENU_LIVE = 2;

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Log.d("MENU", "Clicked : " + position);
        Log.d("MENU", "id : " + id);

        if (view == null) return;
        if (position == 0) return; // This is handled by the top fragment itself

        Log.d("MENU", "View id : " + view.getId());

        switch (view.getId()) {
            case MENU_ID_SETTINGS:
                showSettingsView();
                break;
            case MENU_ID_LIVE:
                showHomeView(0);
                break;
            case MENU_ID_TOP:
                showHomeView(1);
                break;
            case MENU_ID_MOSTREAD:
                showMostReadView();
                break;
            default:
                showHomeView(position - 3);
                break;
        }
        closeDrawer();
    }

    @Override
    public void onBackPressed() {
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START);
        } else {
            drawerLayout.openDrawer(GravityCompat.START);
        }
    }

    private void setupDrawerList() {
        ListView drawerList = (ListView) findViewById(R.id.drawer_list);
        drawerAdapter = new HomeDrawerAdapter(this, new ArrayList<HomeDrawerAdapter.MenuElement>());

        drawerList.setAdapter(drawerAdapter);
        drawerList.setOnItemClickListener(this);
        drawerList.setVerticalScrollBarEnabled(false);
    }

    public static final int MENU_ID_SUB = -1;
    public static final int MENU_ID_LIVE = 1;
    public static final int MENU_ID_TOP = 2;
    public static final int MENU_ID_MOSTREAD = 3;
    public static final int MENU_ID_SETTINGS = 9;

    private void fillDrawerList() {
        if (drawerAdapter == null) return;
        drawerAdapter.clear();
        drawerAdapter.addMenuHeader();
        drawerAdapter.addMenuElement(getString(R.string.menu_live), MENU_ID_LIVE);
        drawerAdapter.addMenuElement(getString(R.string.menu_top), MENU_ID_TOP);
        drawerAdapter.addMenuElement(getString(R.string.menu_most_read), MENU_ID_MOSTREAD);
        addCategories();

        drawerAdapter.addMenuElement(getString(R.string.menu_settings), R.drawable.ic_menu_settings, MENU_ID_SETTINGS);
    }

    private void addCategories() {
        if (drawerCategories != null && !drawerCategories.isEmpty()) {
            drawerAdapter.addMenuSection(getString(R.string.menu_category), categoryNames);
        }
    }

    private void retrieveCategoryData() {
        addFrontCategory();
        drawerCategories = TribuneCache.getInstance().getCategoryByLevel(1);
        Log.d("CATEGORIES", "We found : " + drawerCategories.size() + " categories in DB");
        for (Category c :
                drawerCategories) {
            Log.d("CATEGORIES", "n°" + c.getId() + " lvl" + c.getLevel() + " pos " + c.getPosition() + " : " + c.getName());
        }
        getCategoryNames();
    }

    /**
     * "A la une" is a "fake" category, we got to add it manually if it doesn't exist
     */
    private void addFrontCategory() {

        Category front = new Category();
        front.setId(1);
        front.setLevel(1);
        front.setPosition(-1);
        front.setHidden(false);
        front.setName(getString(R.string.category_front));

        Category live = new Category();
        live.setId(-1);
        live.setLevel(1);
        live.setPosition(-2);
        live.setHidden(false);
        live.setName(getString(R.string.category_live));


        TribuneCache.getInstance().addOrUpdateObject(front);
        TribuneCache.getInstance().addOrUpdateObject(live);
    }

    private final int CATEGORY_SUBSCRIBERS = 89;

    private void getCategoryNames() {
        List<String> names = new ArrayList<>();
        names.add(getString(R.string.category_live));
        for (Category c :
                drawerCategories) {
            if (c.getId() != CATEGORY_SUBSCRIBERS) {
                if (Constants.CATEGORY_NAMES.containsKey(c.getName())) names.add(getString(Constants.CATEGORY_NAMES.get(c.getName())));
                else names.add(c.getName());
            }
        }

        categoryNames = names.toArray(new String[names.size()]);
    }

    protected void closeDrawer() {
        drawerLayout.closeDrawer(GravityCompat.START);
    }

    /**
     * Switch to settings screen
     */
    protected void onSettingsSelected() {
        showSettingsView();
//        startActivity(new Intent(this, SettingsActivity.class));
//        AnimationUtils.animateIn(this);
    }

    public void onHebdoFragmentClicked(View v) {
        NetworkUtils.openAppOrUrl(this, BuildConfig.SUBSCRIBERS_APP_PACKAGE);
//        NetworkUtils.openUrlInCustomTab(this, Uri.parse(BuildConfig.SUBSCRIBERS_URL));
    }

    private AContentFragment currentFragment;

    protected void showSettingsView() {
        switchContentView(ContentType.CONTENT_SETTINGS);
        setToolbarTitle(false, ContentType.CONTENT_SETTINGS.titleId);
    }

    protected void showHomeView(int position) {
        setToolbarTitle(true, ContentType.CONTENT_MAIN.titleId);
        switchContentView(ContentType.CONTENT_MAIN);
        currentFragment.onDrawerItemSelected(position);
    }

    protected void showMostReadView() {
        setToolbarTitle(false, ContentType.CONTENT_MOSTREAD.titleId);
        switchContentView(ContentType.CONTENT_MOSTREAD);
    }

    @Override
    public void onDrawerSlide(View drawerView, float slideOffset) {

    }

    @Override
    public void onDrawerOpened(View drawerView) {
        TrackerManager.getInstance().tagScreen(getString(R.string.tracking_menu));
    }

    @Override
    public void onDrawerClosed(View drawerView) {

    }

    @Override
    public void onDrawerStateChanged(int newState) {

    }

    /**
     * Define possible choices for switchContentView, providing retrieval/instantiation infos
     */
    public enum ContentType {
        CONTENT_MAIN("main", FlowContentFragment.class, 0),
        CONTENT_SETTINGS("settings", SettingsContentFragment.class, R.string.menu_settings),
        CONTENT_MOSTREAD("mostread", MostReadContentFragment.class, R.string.menu_most_read);

        public final int    titleId;
        public final String tag;
        public final Class<? extends AContentFragment> contentFragmentClass;

        ContentType(String tag, Class<? extends AContentFragment> contentFragmentClass, int titleStringREssourceId) {
            this.titleId = titleStringREssourceId;
            this.tag = tag;
            this.contentFragmentClass = contentFragmentClass;
        }
    }

    /**
     * Relay content info for fragment retrieval/instantiation
     *
     * @param type content to display
     */
    private void switchContentView(ContentType type) {
        try {
            switchContentFragment(type.tag, type.contentFragmentClass);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        }
    }

    /**
     * Change the frame layout's content.
     * Retrieve the already existing fragment if available, or create a new one
     *
     * @param tag fragment's tag for the map
     * @param contentFragmentClass fragment type if instantiation is required
     * @throws IllegalAccessException
     * @throws InstantiationException
     */
    private void switchContentFragment(String tag, Class<? extends AContentFragment> contentFragmentClass) throws IllegalAccessException, InstantiationException {

        if (currentFragment != null && currentFragment == fragments.get(tag)) {
            Log.d("HOME_DRAWER_A", "Fragment " + tag + " already on, do nothing");
            return; // The desired view is already displayed
        }

        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();

        if (fragments.containsKey(tag)) {
            Log.d("HOME_DRAWER_A", "Retrieving fragment by tag " + tag);
            AContentFragment homeFragment = fragments.get(tag);
            if (homeFragment != currentFragment && currentFragment != null) {
                Log.d("HOME_DRAWER_A", "Hid previous frag");
                ft.hide(currentFragment);
            }
            ft.show(homeFragment);
            currentFragment = homeFragment;
        } else {
            Log.d("HOME_DRAWER_A", "Creating fragment with tag " + tag);
            AContentFragment homeFragment = contentFragmentClass.newInstance();
            if (homeFragment != currentFragment && currentFragment != null) {
                Log.d("HOME_DRAWER_A", "Hid previous frag");
                ft.hide(currentFragment);
            }
            ft.add(R.id.content_frame, homeFragment);
            currentFragment = homeFragment;
            fragments.put(tag, homeFragment);
        }

        ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        ft.addToBackStack(null);
        ft.commit();
    }
}
