package requestlab.com.latribune.activities;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.android.billingclient.api.BillingClient;
import com.android.billingclient.api.BillingResult;
import com.android.billingclient.api.Purchase;
import com.android.billingclient.api.SkuDetails;
import com.android.billingclient.api.SkuDetailsResponseListener;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.mngads.MNGNativeObject;
import com.mngads.util.MNGFrame;

import java.util.ArrayList;
import java.util.List;

import requestlab.com.latribune.C.Constants;
import requestlab.com.latribune.R;
import requestlab.com.latribune.api.TribuneAPI;
import requestlab.com.latribune.api.listeners.APICallbackListener;
import requestlab.com.latribune.billing.BillingManager;
import requestlab.com.latribune.billing.PurchaseItem;
import requestlab.com.latribune.cache.TribuneCache;
import requestlab.com.latribune.data.Article;
import requestlab.com.latribune.data.Category;
import requestlab.com.latribune.data.Related;
import requestlab.com.latribune.tracking.TrackerManager;
import requestlab.com.latribune.utils.AdManager;
import requestlab.com.latribune.viewholders.ArticleDetailsViewHolder;
import requestlab.com.latribune.viewholders.OutbrainViewHolder;
import requestlab.com.latribune.viewholders.ReactViewHolder;

/**
 * Created by lchazal on 13/04/16.
 */
@SuppressLint("LongLogTag")
public class BetterArticleDetailsActivity extends AToolbarCompatActivity implements View.OnClickListener, APICallbackListener<Article>, BillingManager.BillingUpdatesListener {

    public static final String ARTICLE_DETAIL_ID = "articledetailid";
    public static final String ARTICLE_DETAIL_KEY = "articledetailkey";
    public static final String ARTICLE_DETAIL_CATEGORY = "articlecategory";

    private String articleKey;
    private int articleId = -1;
    private Article article;
    ArrayList<String> skuIDsSubscription = new ArrayList();

    private FloatingActionButton shareFAB;

    private View loadingView;
    List<SkuDetails> mSkuDetailsListSubscription = new ArrayList();
    String SKU_ITEM_PURCHASED = "";
    private String category = "";
    private View mngAdTitle;
    private RelativeLayout mngSquareView;
    private View outbrainView;
    private LinearLayout mLinearLayoutSubscription;
    private Button mButtonSubscription;
    private View mCgu;
    private List<Article> relatedList = new ArrayList<>();
    private BillingManager mBillingManager;
    private OutbrainViewHolder vh;

    private boolean isCalledBilling = false;
    private boolean isSubscribed = false;

    @Override
    protected void onPause() {
        super.onPause();

        if (articleViewHolder != null) articleViewHolder.onPause();
    }

    @Override
    public void onBackPressed() {
        if (articleViewHolder != null) articleViewHolder.onDestroy();
        super.onBackPressed();
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_better_article_detail);

        isCalledBilling = false;
        isSubscribed = false;

        loadingView = findViewById(R.id.empty_view);
        shareFAB = findViewById(R.id.share_fab);
        mLinearLayoutSubscription = findViewById(R.id.linearSubscription);
        mButtonSubscription = findViewById(R.id.button_subscription);
        mCgu = findViewById(R.id.subs_condition);

        if (shareFAB != null) {
            shareFAB.setOnClickListener(this);
        }

        SKU_ITEM_PURCHASED = PurchaseItem.IAB_SUBSCRIPTION.toString();

        setupBilling();
        setupMngSquare();
        setupOutbrain();
        setupArticleView();
        setupCommentView();

        retrieveArticle();

        setupToolbar(R.drawable.ic_arrow_back_white_24dp, category);

        mButtonSubscription.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                purchasesubscription();
            }
        });

        mCgu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://abonnement.latribune.fr/conditions-generales-d-utilisation"));
                startActivity(browserIntent);
            }
        });
    }

    private View articleView;

    private void setupMngSquare() {
        mngSquareView = (RelativeLayout) findViewById(R.id.square_banner);
        mngAdTitle = findViewById(R.id.mng_ad_title);
        if (mngSquareView == null) return;
        AdManager.buildMNGAdLoadingTask(this, createSquareAdListener(), AdManager.AdType.SQUARE).execute();
    }

    private ArticleDetailsViewHolder articleViewHolder;

    private void setupArticleView() {
        articleView = findViewById(R.id.view_holder_article);
        if (articleView == null) return;
        articleViewHolder = new ArticleDetailsViewHolder(articleView);
    }

    private View commentView;
    private ReactViewHolder reactViewHolder;

    private void bindArticleView() {
        if (articleViewHolder == null || article == null) {
            articleView.setVisibility(View.GONE);
            return;
        }
        articleViewHolder.bindViewHolder(this, article, 0);
    }

    private void setupCommentView() {
        commentView = findViewById(R.id.vh_react);
        if (commentView == null) return;
        reactViewHolder = new ReactViewHolder(commentView, this);
    }

    private void bindCommentView() {
        if (reactViewHolder == null || article == null) {
            commentView.setVisibility(View.GONE);
            return;
        }
        reactViewHolder.bindViewHolder(this, article, 0);
    }

    private void setupBilling() {
        mBillingManager = new BillingManager(this, this, PurchaseItem.BASE64_KEY.toString());
    }

    private void setupOutbrain() {
        outbrainView = findViewById(R.id.outbrain_view_holder);
        if (outbrainView == null) Log.d("OUTBRAIN", "damn, the outbrain view is null");
        if (outbrainView == null) return;
        vh = new OutbrainViewHolder(outbrainView);
    }

    private void bindOutbrainView() {
        if (vh == null || article == null) {
            outbrainView.setVisibility(View.GONE);
            return;
        }
        vh.bindViewHolder(this, article, 0);
    }

    private void bindArticle() {
        if (article == null) return;

        bindArticleView();
        bindOutbrainView();
        bindRelatedViews();
        bindCommentView();


        if (article.isProtectedArticle()) {
            Log.e("called ",isCalledBilling+"");
            Log.e("called ",isSubscribed+"");
            if(!isSubscribed) {
                mLinearLayoutSubscription.setVisibility(View.VISIBLE);
                shareFAB.setVisibility(View.GONE);
                mLinearLayoutSubscription.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                    }
                });
            }
        } else {
            mLinearLayoutSubscription.setVisibility(View.GONE);
            shareFAB.setVisibility(View.VISIBLE);

        }
    }

    private void loadAdView() {
        getA4S().setPushNotificationLocked(true);
        AdManager.buildMNGAdLoadingTask(this, createAdListener(), AdManager.AdType.INTERSTITIAL_OVERLAY).execute();
    }

    private void updateVisibility(boolean hasArticle) {
        if (loadingView == null) {
            return;
        }
        loadingView.setVisibility(hasArticle ? View.GONE : View.VISIBLE);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.article_detail, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_comment:
                startCommentListActivity();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void startCommentListActivity() {
        try {
            Intent intent = new Intent(this, ReactActivity.class);
            intent.putExtra(ReactActivity.REACT_ARTICLE_ID, article.getId());
            intent.putExtra(ReactActivity.REACT_CALLER, BetterArticleDetailsActivity.class.getSimpleName());
            intent.putExtra(ReactActivity.REACT_ARTICLE_TITLE, article.getTitle());
            startActivity(intent);
        } catch (NullPointerException e) {
            if (article == null) {
                Log.e("ARTICLE_DETAIL_A", "Null article object : " + e.getMessage());
            } else {
                Log.e("ARTICLE_DETAIL_A", "Probably null article comments object : " + e.getMessage());
            }
        }
    }

    private void retrieveArticle() {

        articleId = getIntent().getIntExtra(ARTICLE_DETAIL_ID, -1);
        articleKey = getIntent().getStringExtra(ARTICLE_DETAIL_KEY);

        if (articleKey == null || articleKey.isEmpty()) {
            fetchArticle(articleId);
        } else {
            fetchArticle(articleKey);
        }
    }

    @SuppressLint("RestrictedApi")
    private void fetchArticle(String key) {
        article = TribuneCache.getInstance().getArticle(key);
        if (article != null) {
            articleId = article.getId();
            TribuneCache.getInstance().setArticleReadStatus(article, true);
            fetchCategory(article.getParentCategory());
            bindArticle();
            updateVisibility(true);

            if (article.getRelated() != null) {
                int i = 0;
                for (Related a :
                        article.getRelated()) {
                    TribuneAPI.getInstance().getArticle(a.getArticleId(), createRelatedArticlesAPICallbackListener());
                    i++;
                    if (i >= 3) return;
                }
            }
        }
    }

    @SuppressLint("RestrictedApi")
    private void fetchArticle(int id) {
        article = TribuneCache.getInstance().getArticle(id + ":0");
        if (article != null) {
            articleKey = article.getCompositeKey();
            TribuneCache.getInstance().setArticleReadStatus(article, true);
            fetchCategory(article.getParentCategory());
            updateVisibility(true);
        }
        TribuneAPI.getInstance().getArticle(id, this);
    }

    private void fetchCategory(int id) {
        Category c = TribuneCache.getInstance().getObject(Category.class, id);
        if (c != null) {
            if (Constants.CATEGORY_NAMES.containsKey(c.getName()))
                category = getString(Constants.CATEGORY_NAMES.get(c.getName()));
            else category = c.getName();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (articleViewHolder != null) articleViewHolder.onResume();

        loadAdView();

        if (article != null)
            TrackerManager.getInstance().tagScreen(category, getString(R.string.tracking_sub_articles), article.getTitle());
    }

    private void onFABClicked() {
        if (article == null) return;
        Intent intent = new Intent();

        intent.setAction(Intent.ACTION_SEND);
        intent.putExtra(Intent.EXTRA_TEXT, article.getUrl());
        intent.setType("text/plain");
        startActivity(intent);
    }

    @SuppressLint("RestrictedApi")
    @Override
    public void onResponseSuccess(Article response) {
        Log.e("NEW_ARTICLE", "reponse cool");
        if (article != null) {
            response.setCompositeKey(article.getCompositeKey());
            response.setParentCategory(article.getParentCategory());
        } else {
            article = response;
            TribuneCache.getInstance().setArticleExtraData(response, 0);
        }
        TribuneCache.getInstance().addOrUpdateObject(response);
        TribuneCache.getInstance().setArticleReadStatus(response, true);

//        Log.e("VANISH " + response.getCompositeKey(), "Article null");
        bindArticle();

        updateVisibility(true);

        if (response.getRelated() != null) {
            int i = 0;
            for (Related a :
                    response.getRelated()) {
                TribuneAPI.getInstance().getArticle(a.getArticleId(), createRelatedArticlesAPICallbackListener());
                i++;
                if (i >= 3) return;
            }
        }
    }

    @Override
    public void onResponseFailure(String message) {

    }

    @Override
    public void onFailure(String message) {

    }

    @Override
    public void onClick(View v) {
        onFABClicked();
    }

    private APICallbackListener<Article> createRelatedArticlesAPICallbackListener() {
        return new APICallbackListener<Article>() {
            @Override
            public void onResponseSuccess(Article response) {
                relatedList.add(relatedList.size(), response);
                bindRelatedViews();
                Log.d("RELATED", "Got related article");
            }

            @Override
            public void onResponseFailure(String message) {

            }

            @Override
            public void onFailure(String message) {

            }
        };
    }

    private AdManager.IAdLoadedListener createAdListener() {
        return new AdManager.IAdLoadedListener() {
            @Override
            public void onAdLoaded(View view, int i) {
                Log.d("SPLASH", "Ad overlay Loaded");
            }

            @Override
            public void onNativeAdLoaded(MNGNativeObject object) {
            }

            @Override
            public void onAdFailed(Exception e) {
                Log.d("SPLASH", "Ad overlay failed to load");
            }

            @Override
            public void onAdChanged(MNGFrame mngFrame) {
                Log.d("SPLASH", "Ad overlay dismissed");
                getA4S().setPushNotificationLocked(false);
            }
        };
    }

    private AdManager.IAdLoadedListener createSquareAdListener() {
        return new AdManager.IAdLoadedListener() {
            @Override
            public void onAdLoaded(View view, int i) {
                mngAdTitle.setVisibility(View.VISIBLE);
                mngSquareView.addView(view);
            }

            @Override
            public void onNativeAdLoaded(MNGNativeObject object) {
            }

            @Override
            public void onAdFailed(Exception e) {

            }

            @Override
            public void onAdChanged(MNGFrame mngFrame) {

            }
        };
    }

    public void bindRelatedViews() {
        if (articleViewHolder != null)
            articleViewHolder.bindRelatedViews(relatedList);
    }


    private void purchasesubscription() {
        SKU_ITEM_PURCHASED = PurchaseItem.IAB_SUBSCRIPTION.toString();
        for (SkuDetails mSkuDetails : mSkuDetailsListSubscription) {
            if (SKU_ITEM_PURCHASED.equals(mSkuDetails.getSku())) {
                Log.e("BetterArticleDetailsActivity", "Selected SKU: " + mSkuDetails);
                mBillingManager.initiatePurchaseFlow(mSkuDetails);
                break;
            }
        }
    }

    @Override
    public void onBillingClientSetupFinished() {

        skuIDsSubscription.add(PurchaseItem.IAB_SUBSCRIPTION.toString());
        mBillingManager.querySkuDetailsAsync(BillingClient.SkuType.SUBS, skuIDsSubscription, new SkuDetailsResponseListener() {
            @Override
            public void onSkuDetailsResponse(@NonNull BillingResult billingResult, @Nullable List<SkuDetails> skuDetailsList) {
                if (skuDetailsList != null) {
                    isCalledBilling = true;
                    mSkuDetailsListSubscription = skuDetailsList;
                    Log.i("BetterArticleDetailsActivity", "querySkuDetailsAsync skuDetailsList Size: " + skuDetailsList);
                    for (SkuDetails sku : skuDetailsList) {

                        Log.e(
                                "BetterArticleDetailsActivity",
                                "querySkuDetailsAsync skuDetailsList: " + (sku.getTitle() + "\n Price " + sku.getPrice() + "\n").replace("(InAppSubscription)", " Sub ")
                        );
                    }
                }else{
                    Log.e("purchase","not");
                }
            }
        });


    }

    @Override
    public void onPurchasesFinished(List<Purchase> purchases) {
        Log.e("BetterArticleDetailsActivity", "onPurchasesFinished HAS BEEN CALLED");
        updatePurchase(purchases);
    }

    @Override
    public void onPurchasesUpdated(List<Purchase> purchases) {
        updatePurchase(purchases);
    }

    @Override
    public void onConsumeFinished(String token, int result) {
        Log.e("BetterArticleDetailsActivity", "onConsumeFinished Token :$token");
    }

    private void updatePurchase(List<Purchase> purchases) {
        for (Purchase purchase : purchases) {
//            if (!!purchase.isAcknowledged) {
//                mBillingManager!!.acknowledgePurchase(purchase)
//            }
            if (purchase.getSku().equalsIgnoreCase(SKU_ITEM_PURCHASED)) {

                if (SKU_ITEM_PURCHASED.equalsIgnoreCase(PurchaseItem.IAB_SUBSCRIPTION.toString())) {
                    isSubscribed=true;
                    Log.e("BetterArticleDetailsActivity", "Premium purchased");
                    mLinearLayoutSubscription.setVisibility(View.GONE);
                    shareFAB.setVisibility(View.VISIBLE);
                    break;
                }
            }


        }
    }


}
