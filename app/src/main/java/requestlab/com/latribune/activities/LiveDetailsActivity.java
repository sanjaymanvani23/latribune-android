package requestlab.com.latribune.activities;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;

import android.text.Html;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;


import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.mngads.MNGNativeObject;
import com.mngads.util.MNGFrame;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

import requestlab.com.latribune.R;
import requestlab.com.latribune.api.TribuneAPI;
import requestlab.com.latribune.api.listeners.APICallbackListener;
import requestlab.com.latribune.cache.TribuneCache;
import requestlab.com.latribune.data.Depeche;
import requestlab.com.latribune.tracking.TrackerManager;
import requestlab.com.latribune.utils.AdManager;
import requestlab.com.latribune.utils.CSSManager;
import requestlab.com.latribune.utils.ErrorLogger;
import requestlab.com.latribune.utils.ImageManager;
import requestlab.com.latribune.utils.OutbrainManager;
import requestlab.com.latribune.viewholders.OutbrainViewHolder;
import requestlab.com.latribune.views.TransparentLigAdView;
import requestlab.com.latribune.views.fontviews.FontTextView;

/**
 * Created by lchazal on 13/04/16.
 *
 */
public class LiveDetailsActivity extends AToolbarCompatActivity implements View.OnClickListener {

    private int articleId = -1;
    private Depeche depeche;

    private FloatingActionButton shareFAB;

    private View loadingView;

    private View    mngAdTitle;
    private RelativeLayout  mngSquareView;
    private View            outbrainView;
    private TransparentLigAdView    ligAdView;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_live_detail);

        loadingView = findViewById(R.id.empty_view);

        shareFAB = (FloatingActionButton) findViewById(R.id.share_fab);
        if (shareFAB != null) {
            shareFAB.setOnClickListener(this);
        }

        image = (ImageView) findViewById(R.id.article_image);
        title = (FontTextView) findViewById(R.id.article_title);
        date = (TextView) findViewById(R.id.article_date);
        content = (WebView) findViewById(R.id.article_content);
        authorName = (FontTextView) findViewById(R.id.author_name);

        retrieveArticle();

        setupToolbar(R.drawable.ic_arrow_back_white_24dp, "Live");

        setupMngSquare();
        setupOutbrain();
    }

    @Override
    protected void onPause() {
        super.onPause();

        if (content != null) content.onPause();
    }

    @Override
    public void onBackPressed() {
        if (content != null) {
            content.loadUrl("");
            content.destroy();
        }

        super.onBackPressed();
    }

    private void    setupMngSquare() {
        mngAdTitle = findViewById(R.id.mng_ad_title);
        mngSquareView = (RelativeLayout) findViewById(R.id.square_banner);
        if (mngSquareView == null) return;
        AdManager.buildMNGAdLoadingTask(this, createSquareAdListener(), AdManager.AdType.SQUARE).execute();
    }

    private void    setupOutbrain() {
        outbrainView = findViewById(R.id.related_box);
        if (outbrainView == null) return;
        vh = new OutbrainViewHolder(outbrainView);
    }

    private OutbrainViewHolder  vh;

    private void    bindOutbrainView(Depeche article) {
        if (vh == null || article == null) return;
        OutbrainManager.getInstance().getRecommendations(article.getUrl(), vh);
    }

    private class LoadAdTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void ... params) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    ligAdView.fillLigAdView(LiveDetailsActivity.this, 81839);
                }
            });
            return null;
        }

        @Override
        protected void onPostExecute(Void o) {
            super.onPostExecute(o);
            ligAdView.setVisibility(View.VISIBLE);
        }
    }

    private ImageView       image;
    private FontTextView    title;
    private FontTextView    authorName;
    private TextView        date;
    private WebView         content;

    private void bindArticle() {
        if (image != null) {
            if (depeche.getMedia() != null) {
                ImageManager.getInstance().fitImageIntoView(depeche.getMedia().getFormats().getOriginal(), image);
                image.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ArrayList<String> urls = new ArrayList<>();

                        urls.add(depeche.getMedia().getUrl());
                        urls.add(depeche.getMedia().getUrl());
                        urls.add(depeche.getMedia().getUrl());

                        Intent intent = new Intent(LiveDetailsActivity.this, GalleryActivity.class);
                        intent.putExtra(GalleryActivity.KEY_IS_DEPECHE, true);
                        intent.putExtra(GalleryActivity.KEY_DEPECHE_ID, depeche.getId());
                        startActivity(intent);
                    }
                });
            } else {
                image.setVisibility(View.GONE);
            }
        }

        title.setText(depeche.getTitle());
        authorName.setText(depeche.getSignature());

        Calendar c = Calendar.getInstance();
        c.setTime(depeche.getPublicationDate());

        SimpleDateFormat format = new SimpleDateFormat("dd'/'MM'/'yyyy 'à' kk'h'mm", Locale.getDefault());

        date.setText(Html.fromHtml(getString(R.string.article_date,
                format.format(c.getTime()))));

        content.getSettings().setJavaScriptEnabled(true);

        String htmlData = CSSManager.getInstance().formatHTMLString(depeche.getBodyHtml());

        content.loadDataWithBaseURL("file:///android_asset/", htmlData, "text/html", "utf-8", null);

        shareFAB.setVisibility(View.VISIBLE);

        bindOutbrainView(depeche);
    }


    private void loadAdView() {
        getA4S().setPushNotificationLocked(true);
        AdManager.buildMNGAdLoadingTask(this, createInterstitialListener(), AdManager.AdType.INTERSTITIAL_OVERLAY).execute();
    }

    private void retrieveArticle() {
        articleId = getIntent().getIntExtra(BetterArticleDetailsActivity.ARTICLE_DETAIL_ID, -1);

        if (articleId == -1) {
            ErrorLogger.logBadArticleToCrashlytics(articleId, this.getClass().getName());
            Toast.makeText(this, "Couldn't display article : invalid id " + articleId, Toast.LENGTH_LONG).show();
            finish();
        }

        fetchCacheData(articleId);
        fetchApiData(articleId);
    }

    private void fetchCacheData(int id) {
        Depeche depeche = TribuneCache.getInstance().getObject(Depeche.class, id);
        if (depeche != null) {
            Log.d("DEPECHE_FETCH", "We got a cache result : " + depeche.getTitle());
//            adapter.addArticle(depeche);
            this.depeche = depeche;
            bindArticle();
        }
    }

    private void fetchApiData(final int id) {
        TribuneAPI.getInstance().getDepeche(id, new APICallbackListener<Depeche>() {
            @Override
            public void onResponseSuccess(Depeche response) {
                Log.d("DEPECHE_FETCH", "We got an answer : " + response.getTitle());
                TribuneCache.getInstance().addOrUpdateObject(response);
                fetchCacheData(id);
            }

            @Override
            public void onResponseFailure(String message) {
                Log.d("DEPECHE_FETCH", "failure");
            }

            @Override
            public void onFailure(String message) {
                Log.d("DEPECHE_FETCH", "failure");

            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();

        loadAdView();

        if (depeche != null) TrackerManager.getInstance().tagScreen(getString(R.string.tracking_live_chapter),
                getString(R.string.tracking_sub_articles),
                depeche.getTitle());
    }

    private void onFABClicked() {
        Log.d("FAB_CLICK", "Live FAB clicked");
        Intent intent = new Intent();

        intent.setAction(Intent.ACTION_SEND);
        intent.putExtra(Intent.EXTRA_TEXT, depeche.getUrl());
        intent.setType("text/plain");
        startActivity(intent);
    }

    @Override
    public void onClick(View v) {
        onFABClicked();
    }

    private AdManager.IAdLoadedListener createInterstitialListener() {
        return new AdManager.IAdLoadedListener() {
            @Override
            public void onAdLoaded(View view, int i) {
                Log.d("SPLASH", "Ad overlay Loaded");
            }

            @Override
            public void onNativeAdLoaded(MNGNativeObject object) {}

            @Override
            public void onAdFailed(Exception e) {
                Log.d("SPLASH", "Ad overlay failed to load");
            }

            @Override
            public void onAdChanged(MNGFrame mngFrame) {
                Log.d("SPLASH", "Ad overlay dismissed");
                getA4S().setPushNotificationLocked(false);
            }
        };
    }

    private AdManager.IAdLoadedListener createSquareAdListener() {
        return new AdManager.IAdLoadedListener() {
            @Override
            public void onAdLoaded(View view, int i) {
                mngSquareView.addView(view);
                mngAdTitle.setVisibility(View.VISIBLE);
            }

            @Override
            public void onNativeAdLoaded(MNGNativeObject object) {}

            @Override
            public void onAdFailed(Exception e) {

            }

            @Override
            public void onAdChanged(MNGFrame mngFrame) {

            }
        };
    }
}
