package requestlab.com.latribune.activities;

import android.os.Bundle;

import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import androidx.core.content.ContextCompat;

import requestlab.com.latribune.BuildConfig;
import requestlab.com.latribune.R;
import requestlab.com.latribune.tracking.TrackerManager;
import requestlab.com.latribune.utils.AnimationUtils;
import requestlab.com.latribune.utils.CompatibilityUtils;
import requestlab.com.latribune.utils.NetworkUtils;

/**
 * Created by lchazal on 17/03/16.
 */
public class SendReportActivity extends AToolbarCompatActivity {

    private boolean suggestion = false;

    private EditText    nameField;
    private EditText    mailField;
    private EditText    contentField;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_send_report);

        suggestion = getIntent().getBooleanExtra("title", false);
        setupNameField();
        setupMailField();
        setupContentField();
        setupSendButton();
        setupToolbar(R.drawable.ic_arrow_back_white_24dp, getString(suggestion ? R.string.suggestion_title : R.string.technical_title));
    }

    @Override
    protected void onHomeButtonClicked() {
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        AnimationUtils.animateOut(this);
    }

    @Override
    protected void onResume() {
        super.onResume();

        TrackerManager.getInstance().tagScreen(getString(R.string.tracking_settings_chapter),
                getString(suggestion ? R.string.suggestion_title : R.string.technical_title));
    }

    public void setupNameField() {
        nameField = (EditText) findViewById(R.id.edit_name);
    }

    public void setupMailField() {
        mailField = (EditText) findViewById(R.id.edit_mail);
    }

    public void setupContentField() {
        contentField = (EditText) findViewById(R.id.edit_content);
    }

    private void setupSendButton() {
        Button btn = (Button) findViewById(R.id.send_button);
        if (btn == null) return;
        CompatibilityUtils.setButtonTint(btn, ContextCompat.getColor(this, R.color.send_button_background));
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onSendButtonClicked();
            }
        });
    }

    public void onSendButtonClicked() {
        NetworkUtils.sendEmail(this,
                BuildConfig.TECHNICAL_MAIL,
                getString(suggestion ? R.string.suggestion_title : R.string.technical_title),
                contentField.getText().toString()
        );
    }
}
