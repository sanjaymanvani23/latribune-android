package requestlab.com.latribune.activities;

import android.os.Bundle;

import android.util.Log;
import android.view.View;

import androidx.viewpager.widget.ViewPager;

import java.util.ArrayList;
import java.util.List;

import requestlab.com.latribune.R;
import requestlab.com.latribune.accengage.A4SAppCompatActivity;
import requestlab.com.latribune.adapters.GalleryAdapter;
import requestlab.com.latribune.cache.TribuneCache;
import requestlab.com.latribune.data.Article;
import requestlab.com.latribune.data.Depeche;
import requestlab.com.latribune.data.Media;

/**
 * Created by lchazal on 25/04/16.
 */
//public class GalleryActivity extends AppCompatActivity {
public class GalleryActivity extends A4SAppCompatActivity {

    public static final String  KEY_URLS = "urls";
    public static final String  KEY_LEGEND = "legend";

    public static final String  KEY_IS_DEPECHE = "isdepeche";
    public static final String  KEY_DEPECHE_ID = "depecheid";
    public static final String  KEY_ARTICLE_KEY = "articlekey";

    private GalleryAdapter  adapter;
    private ViewPager viewPager;

    private List<GalleryAdapter.MediaData> medias = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gallery);

        Log.d("GALLERY_MODE", "Starting Gallery");
        boolean bobolean = getIntent().getBooleanExtra(KEY_IS_DEPECHE, false);
        Log.d("GALLERY_MODE", "is depeche ? " + bobolean);
        Log.d("GALLERY_MODE", "id : " + (bobolean ? getIntent().getIntExtra(KEY_DEPECHE_ID, -1) : getIntent().getStringExtra(KEY_ARTICLE_KEY)));
        retrieveArticle(getIntent().getBooleanExtra(KEY_IS_DEPECHE, false));

        setupAdapter();
        setupViewPager();
    }

    private void retrieveArticle(boolean isDepeche) {
        if (isDepeche) retrieveDepeche(getIntent().getIntExtra(KEY_DEPECHE_ID, -1));
        else retrieveArticle(getIntent().getStringExtra(KEY_ARTICLE_KEY));
    }

    private void    retrieveDepeche(int id) {
        Log.d("GALLERY_MODE", "Getting a depeche");
        if (id == -1) return;
        Log.d("GALLERY_MODE", "Getting a valid depeche");
        Depeche depeche = TribuneCache.getInstance().getObject(Depeche.class, id);
        if (depeche != null)
            addImagesToGallery(depeche);
    }

    private void retrieveArticle(String key) {
        Log.d("GALLERY_MODE", "Getting an article");
        if (key == null || key.isEmpty()) return;
        Log.d("GALLERY_MODE", "Getting a valid article");
        Article article = TribuneCache.getInstance().getArticle(key);
        if (article != null)
            addImagesToGallery(article);
    }

    private void    addImagesToGallery(Article article) {
        Log.d("GALLERY_MODE", "Getting article medias");
        if (article.getMedia() != null)
            addMediaToGallery(article.getMedia().getUrl(), article.getMediaLegend());
        if (article.getMedias() != null)
            for (Media m :
                    article.getMedias()) {
                addMediaToGallery(m.getUrl(), m.getTitle());
            }
    }

    private void    addImagesToGallery(Depeche depeche) {
        Log.d("GALLERY_MODE", "Getting depeche medias");
        if (depeche.getMedia() != null)
            addMediaToGallery(depeche.getMedia().getUrl(), depeche.getMediaLegend());
    }

    private void setupAdapter() {
        adapter = new GalleryAdapter(getSupportFragmentManager(), this, medias);
    }

    private void addMediaToGallery(String url, String legend) {
        if (url == null || url.isEmpty()) return;

        GalleryAdapter.MediaData mediaData = new GalleryAdapter.MediaData();

        mediaData.mediaUrl = url;
        mediaData.mediaLegend = legend;

        medias.add(mediaData);
    }

    private void setupViewPager() {
        viewPager = (ViewPager) findViewById(R.id.pager);

        if (viewPager == null) return;

        viewPager.setAdapter(adapter);
        viewPager.setOffscreenPageLimit(2);
//        viewPager.addOnPageChangeListener(this);
    }

    public void onCloseButtonClicked(View view) {
        finish();
    }
}
