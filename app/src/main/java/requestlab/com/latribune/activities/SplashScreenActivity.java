package requestlab.com.latribune.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;

import android.util.Log;
import android.view.View;

import com.mngads.MNGNativeObject;
import com.mngads.util.MNGFrame;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.Days;
import org.joda.time.LocalDate;

import java.util.Date;
import java.util.List;

import requestlab.com.latribune.R;
import requestlab.com.latribune.accengage.A4SAppCompatActivity;
import requestlab.com.latribune.api.TribuneAPI;
import requestlab.com.latribune.api.listeners.APICallbackListener;
import requestlab.com.latribune.data.Comment;
import requestlab.com.latribune.utils.AdManager;
import requestlab.com.latribune.utils.DataManager;

public class SplashScreenActivity extends A4SAppCompatActivity {

    public static final int SPLASH_STOP_MESSAGE = 0;
    public static final int SPLASH_EXTRA_MESSAGE = 1;
    public static final int SPLASH_TIME = 1000;
    public static final int EXTRA_TIME = 4000;
    public static final int MIN_DAY_BEFORE_THEMES = 10;

    private Handler mSplashHandler;

    private boolean initialCountDownFinished = false;
    private boolean adViewLoaded = false;
    private boolean startUpDone = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_splashscreen);

        getA4S().setPushNotificationLocked(true);
        getA4S().setInAppDisplayLocked(true);

        setSplashScreenCountdown();
        startInitialCountDown();
        loadAdView();
        retrieveCategoryData();
    }

    private void loadAdView() {
        AdManager.buildMNGAdLoadingTask(this, createAdListener(), AdManager.AdType.INTERSTITIAL).execute();
    }

    /**
     * Retrieve Category list to build the main screen
     */
    private void retrieveCategoryData() {
        DataManager.getInstance().loadCategoryData();
    }

    /***
     * Setup countdown callback
     */
    private void setSplashScreenCountdown() {
        mSplashHandler = new Handler(new Handler.Callback() {
            @Override
            public boolean handleMessage(Message msg) {
                switch (msg.what) {
                    case SPLASH_STOP_MESSAGE:
                        Log.d("SPLASH", "Initial countdown finished");
                        onInitialCountDownEnd();
                        break;
                    case SPLASH_EXTRA_MESSAGE:
                        Log.d("SPLASH", "Extra countdown finished");
                        onExtraCountDownEnd();
                }
                return true;
            }
        });
    }

    /**
     * Start the countdown
     */
    private void startInitialCountDown() {
        Message msg = new Message();
        msg.what = SPLASH_STOP_MESSAGE;
        mSplashHandler.sendMessageDelayed(msg, SPLASH_TIME);
    }

    /**
     * Start the extra time countdown for ad loading
     */
    private void startExtraCountdown() {
        Message msg = new Message();
        msg.what = SPLASH_EXTRA_MESSAGE;
        mSplashHandler.sendMessageDelayed(msg, EXTRA_TIME);
    }

    private void onInitialCountDownEnd() {
        initialCountDownFinished = true;
        if (adViewLoaded) startNextActivity();
        else startExtraCountdown();
    }

    private void onExtraCountDownEnd() {
        if (!adViewLoaded) {
            AdManager.killCurrentFactory();
            startNextActivity();
        }
    }

    private void startNextActivity() {
        startUpDone = true;
        SharedPreferences prefs = getSharedPreferences(getString(R.string.shared_pref_session_file), Context.MODE_PRIVATE);

        int launchCount = prefs.getInt(getString(R.string.shared_pref_launch_count), 0);

        launchCount++;

        prefs.edit().putInt(getString(R.string.shared_pref_launch_count), launchCount).apply();

        int daysSinceLastPick = getDaysCountSinceLastPick();

        Log.d("FIRST_LAUNCH", "Not first start, skipping tutorial");
        startActivity(new Intent(this, HomeActivity.class));
        finish();
    }

    private int getDaysCountSinceLastPick() {
        SharedPreferences prefs = getSharedPreferences(getString(R.string.shared_pref_session_file), MODE_PRIVATE);
        String lastTime = prefs.getString(getString(R.string.shared_pref_last_pick_date), "");

        DateTime now = new DateTime((DateTimeZone.UTC));

        if (lastTime.isEmpty()) {
            prefs.edit().putString(getString(R.string.shared_pref_last_pick_date), now.toString()).apply();
            return 0;
        }

        DateTime then = new DateTime(lastTime);

        int dayCount = Days.daysBetween(then, now).getDays();
        if (dayCount >= MIN_DAY_BEFORE_THEMES) {
            prefs.edit().putString(getString(R.string.shared_pref_last_pick_date), now.toString()).apply();
        }

        return dayCount;
    }

    private AdManager.IAdLoadedListener createAdListener() {
        return new AdManager.IAdLoadedListener() {
            @Override
            public void onAdLoaded(View view, int i) {
                Log.d("SPLASH", "Ad Loaded");
                if (startUpDone) return;
                adViewLoaded = true;
                if (initialCountDownFinished) startNextActivity();
            }

            @Override
            public void onNativeAdLoaded(MNGNativeObject object) {}

            @Override
            public void onAdFailed(Exception e) {
                if (startUpDone) return;
                Log.d("SPLASH", "Ad failed to load");
                if (initialCountDownFinished) startNextActivity();
            }

            @Override
            public void onAdChanged(MNGFrame mngFrame) {
                Log.d("SPLASH", "Ad dismissed");
            }
        };
    }
}
